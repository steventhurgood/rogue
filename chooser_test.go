package rogue

import (
	"math/rand"
	"testing"
)

var (
	r = rand.New(rand.NewSource(1))
)

func TestChooser(t *testing.T) {
	tests := [][]WeightedChoice{
		[]WeightedChoice{
			WeightedChoice{1.0, "one"},
			WeightedChoice{0.0, "two"},
			WeightedChoice{0.0, "three"},
		},
		[]WeightedChoice{
			WeightedChoice{0.0, "one"},
			WeightedChoice{1.0, "two"},
			WeightedChoice{0.0, "three"},
		},
		[]WeightedChoice{
			WeightedChoice{0.0, "one"},
			WeightedChoice{0.0, "two"},
			WeightedChoice{1.0, "three"},
		},
	}
	want := []string{"one", "two", "three"}

	for i, test := range tests {
		got, err := Choose(test, r)
		if err != nil {
			t.Errorf("Choose(%v) -> Error: %v", test, err)
		}
		t.Logf("Choose(%v) -> %v", test, got)
		if got != want[i] {
			t.Errorf("Choose(%v) -> %v. Want: %v", test, got, want[i])
		}
	}
}
