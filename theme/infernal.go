package theme

import (
	"bitbucket.org/steventhurgood/rogue"
	"github.com/golang/glog"
)

// The infernal theme is like the dungeon theme, but with more fire and demonic things

func init() {
	infernal := &rogue.Theme{
		Name: "infernal",
		ZoneAttributes: rogue.ZoneAttributes{
			NumRooms: &rogue.Random{
				Limits: &rogue.Range{20, 30},
			},
			RoomSize: &rogue.Random{
				LogNormal: &rogue.DistParams{10, 5},
			},
			Rooms: []rogue.WeightedChoice{
				rogue.WeightedChoice{1.0, "dungeon"},
				rogue.WeightedChoice{2.0, "cave"},
			},
		},
	}
	if err := rogue.RegisterTheme(infernal); err != nil {
		glog.Fatal(err)
	}
}
