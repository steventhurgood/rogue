package theme

import (
	"bitbucket.org/steventhurgood/rogue"
	"github.com/golang/glog"
)

// The dungeon theme contains rooms and corridors and medieval-like things

func init() {
	dungeon := &rogue.Theme{
		Name: "dungeon",
		ZoneAttributes: rogue.ZoneAttributes{
			NumRooms: &rogue.Random{
				Limits: &rogue.Range{10, 20},
			},
			RoomSize: &rogue.Random{
				Limits: &rogue.Range{5, 15},
			},
			Rooms: []rogue.WeightedChoice{
				rogue.WeightedChoice{2.0, "dungeon"},
				rogue.WeightedChoice{1.0, "cave"},
			},
		},
	}
	if err := rogue.RegisterTheme(dungeon); err != nil {
		glog.Fatal(err)
	}
}
