package rogue

import (
	"fmt"
	"math/rand"
)

// random number methods

type Random struct {
	Limits    *Range
	LogNormal *DistParams
}

func RandomNumber(rnd *Random, r *rand.Rand) (int, error) {
	if rnd == nil {
		return 0, fmt.Errorf("No random number method specified")
	}
	switch {
	case rnd.Limits != nil:
		{
			return RandomRange(rnd.Limits, r), nil
		}
	case rnd.LogNormal != nil:
		{
			return RandomLogNormal(rnd.LogNormal, r), nil
		}
	default:
		{
			return 0, fmt.Errorf("No random number generators specified")
		}
	}
}
