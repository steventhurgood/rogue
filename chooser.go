package rogue

import (
	"fmt"
	"math/rand"
)

// WeightedChoice represents a number of different options with different probabilistic weights
type WeightedChoice struct {
	Weight float64
	Choice string
}

// Choose - selects randomly from a list of weighted choices
func Choose(w []WeightedChoice, r *rand.Rand) (string, error) {
	var sum float64 = 0
	for _, c := range w {
		sum += c.Weight
	}

	selection := r.Float64() * sum
	for _, c := range w {
		if selection < c.Weight {
			return c.Choice, nil
		}
		selection -= c.Weight
	}
	return "", fmt.Errorf("No choices available") // should never happen
}
