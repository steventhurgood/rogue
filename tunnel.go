package rogue

type Tunnel struct {
	From, To Room
	Span     bool
}

func (t Tunnel) Len() int {
	return 2
}

func (t Tunnel) XY(i int) (float64, float64) {
	var b Room
	if i == 0 {
		b = t.From
	} else {
		b = t.To
	}
	x := float64(b.X() + b.Width()/2.0)
	y := float64(b.Y() + b.Height()/2.0)
	return x, y
}
