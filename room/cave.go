package room

import (
	"fmt"
	"math/rand"

	"github.com/golang/glog"

	"bitbucket.org/steventhurgood/rogue"
)

func init() {
	c := &caveRoomFactory{}
	if err := rogue.RegisterRoom(c); err != nil {
		glog.Fatal(err)
	}
}

type caveRoom struct {
	attributes rogue.RoomAttributes
	rogue.Box
}

func (c *caveRoom) GetAttributes() *rogue.RoomAttributes {
	return &c.attributes
}

func (c *caveRoom) Render(m [][]rogue.Tile, z *rogue.Zone, r *rand.Rand) error {
	// carve out open space:
	for x := c.X(); x < c.X()+c.Width(); x++ {
		for y := c.Y(); y < c.Y()+c.Height(); y++ {
			rm := m[x][y]
			if !rm.Attributes.Blocked {
				return fmt.Errorf("Error creating room. Tile (%v, %v) already occupied: %v", x, y, rm)
			}
			glog.Infof("Carving: (%v, %v)", x, y)
			rm.Zone = z
			rm.Type = rogue.RoomTile
			rm.Attributes.Blocked = false
		}
	}
	return nil
}

type caveRoomFactory struct{}

func (c *caveRoomFactory) Name() string {
	return "cave"
}

func (c *caveRoomFactory) New(z *rogue.Zone, r *rand.Rand) rogue.Room {
	room := &caveRoom{
		attributes: rogue.RoomAttributes{
			Type: "cave",
		},
	}
	return room
}
