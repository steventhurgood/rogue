package room

import (
	"fmt"
	"math/rand"

	"github.com/golang/glog"

	"bitbucket.org/steventhurgood/rogue"
)

func init() {
	d := &dungeonRoomFactory{}
	if err := rogue.RegisterRoom(d); err != nil {
		glog.Fatal(err)
	}
}

type dungeonRoom struct {
	attributes rogue.RoomAttributes
	rogue.Box
}

func (d *dungeonRoom) GetAttributes() *rogue.RoomAttributes {
	return &d.attributes
}

func (d *dungeonRoom) Render(m [][]rogue.Tile, z *rogue.Zone, r *rand.Rand) error {
	// carve out open space:
	for x := d.X(); x < d.X()+d.Width(); x++ {
		for y := d.Y(); y < d.Y()+d.Height(); y++ {
			rm := &m[x][y]
			if !rm.Attributes.Blocked {
				return fmt.Errorf("Error creating room. Tile (%v, %v) already occupied: %v", x, y, rm)
			}
			glog.Infof("Carving: (%v, %v)", x, y)
			rm.Zone = z
			rm.Type = rogue.RoomTile
			rm.Attributes.Blocked = false
			rm.Attributes.Hardness = 0.2
		}
	}
	// render walls
	for x := d.X() - 1; x < d.X()+d.Width()+1; x++ {
		edges := []int{d.Y() - 1, d.Y() + d.Height() + 1}
		for _, y := range edges {
			rm := &m[x][y]
			if !rm.Attributes.Blocked {
				return fmt.Errorf("Error creating room wall. Tile (%v, %v) already occupied: %v", x, y, rm)
			}
			glog.Infof("Building Wall: (%v, %v)", x, y)
			rm.Zone = z
			rm.Type = rogue.WallTile
			rm.Attributes.Blocked = true
			rm.Attributes.Hardness = 20.0
		}
	}
	return nil
}

type dungeonRoomFactory struct{}

func (d *dungeonRoomFactory) Name() string {
	return "dungeon"
}

func (d *dungeonRoomFactory) New(z *rogue.Zone, r *rand.Rand) rogue.Room {
	room := &dungeonRoom{
		attributes: rogue.RoomAttributes{
			Type: "dungeon",
		},
	}
	return room
}
