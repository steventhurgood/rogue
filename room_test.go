package rogue

import (
	"math/rand"
	"testing"
)

type TestRoom struct {
	Box
	a *RoomAttributes
}

func (t *TestRoom) GetAttributes() *RoomAttributes {
	return t.a
}

func (t *TestRoom) Render([][]Tile, *Zone, *rand.Rand) error {
	return nil
}

func TestRoomToBox(t *testing.T) {
	r := []Room{
		&TestRoom{
			Box: Box{
				x:      0,
				y:      0,
				width:  10,
				height: 10,
			},
		},
	}
	b := RoomToBoxLike(r)
	b[0].SetX(10)

	if r[0].X() != 10 {
		t.Errorf("RoomToBoxLike(%v) -> %v", r, b)
	}
}
