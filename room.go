package rogue

import (
	"fmt"
	"math/rand"

	"github.com/golang/glog"
)

// Package room defines interfaces and implementation details for creating rooms in a map

var (
	roomRegistry = make(map[string]RoomFactory)
)

func RegisterRoom(r RoomFactory) error {
	name := r.Name()
	if _, ok := roomRegistry[name]; ok {
		return fmt.Errorf("Room %v already registered", name)
	}
	glog.Infof("Registering room: %v", name)
	roomRegistry[name] = r
	return nil
}

type RoomAttributes struct {
	Type string
}

type Room interface {
	GetAttributes() *RoomAttributes
	Render([][]Tile, *Zone, *rand.Rand) error // cave out a map of tiles, at a specific density - must support 1 + 3
	BoxLike
}

type RoomFactory interface {
	Name() string
	New(z *Zone, r *rand.Rand) Room
}

func GetRoom(roomType string) (RoomFactory, error) {
	if r, ok := roomRegistry[roomType]; ok {
		return r, nil
	}
	return nil, fmt.Errorf("Room %v not registered (%v)", roomType, roomRegistry)
}

func SetRoomSize(room BoxLike, z *Zone, r *rand.Rand) error {
	if w := room.Width(); w == 0 {
		w, err := RandomNumber(z.Config.RoomSize, r)
		if err != nil {
			return err
		}
		room.SetWidth(w)
	}
	if h := room.Height(); h == 0 {
		h, err := RandomNumber(z.Config.RoomSize, r)
		if err != nil {
			return err
		}
		room.SetHeight(h)
	}
	return nil
}

func RoomToBoxLike(r []Room) []BoxLike {
	b := make([]BoxLike, len(r))
	for i := range r {
		b[i] = r[i]
	}
	return b
}
