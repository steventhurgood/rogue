package rogue

import (
	"bytes"
	"fmt"
)

type ZoneAttributes struct {
	NumRooms       *Random
	RoomSize       *Random
	RoomSeparation *Random
	Rooms          []WeightedChoice
	Layout         []WeightedChoice
	LayoutOptions  *LayoutOptions

	// FeatureDist
	// ItemDist
	// RoomLayout
	// ZoneLayout
}

func (z *ZoneAttributes) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "Num Rooms: %v\n", z.NumRooms)
	fmt.Fprintf(&b, "Room Size: %v\n", z.RoomSize)
	fmt.Fprintf(&b, "Rooms: %v\n", z.Rooms)
	fmt.Fprintf(&b, "Layout: %v\n", z.Layout)
	fmt.Fprintf(&b, "Layout Options: %v\n", z.LayoutOptions)
	return b.String()
}

// CopyCascading propogates copies attributes to dst
// if an attribute is undefined in dst, look for it in srcs[0], srcs[1], ...
func CopyZoneAttributes(dst *ZoneAttributes, srcs ...*ZoneAttributes) error {
	if dst.NumRooms == nil {
		for _, src := range srcs {
			if src.NumRooms != nil {
				dst.NumRooms = src.NumRooms
				break
			}
		}
	}
	if dst.RoomSize == nil {
		for _, src := range srcs {
			if src.RoomSize != nil {
				dst.RoomSize = src.RoomSize
				break
			}
		}
	}
	if dst.Rooms == nil {
		for _, src := range srcs {
			if src.Rooms != nil {
				dst.Rooms = src.Rooms
				break
			}
		}
	}
	if dst.RoomSeparation == nil {
		for _, src := range srcs {
			if src.RoomSeparation != nil {
				dst.RoomSeparation = src.RoomSeparation
				break
			}
		}
	}
	if dst.Layout == nil {
		for _, src := range srcs {
			if src.Layout != nil {
				dst.Layout = src.Layout
				break
			}
		}
	}
	if dst.LayoutOptions == nil {
		for _, src := range srcs {
			if src.LayoutOptions != nil {
				dst.LayoutOptions = src.LayoutOptions
				break
			}
		}
	}
	return nil
}
