package rogue

import (
	"fmt"

	"bitbucket.org/steventhurgood/rogue/mesh"
)

func ZoneToMesh(z *Zone) (*mesh.Mesh, error) {
	m := &mesh.Mesh{}
	for i, r := range z.Rooms {
		m.P = append(m.P, mesh.Vertex{
			X:     float64(r.X()) + 0.5*float64(r.Width()),
			Y:     float64(r.Y()) + 0.5*float64(r.Height()),
			Index: i,
		})
	}
	if err := m.Triangulate(); err != nil {
		fmt.Println("Points:\n")
		fmt.Printf("%v\n", mesh.TestCase(m))
		return nil, err
	}
	return m, nil
}

func LevelToMesh(l *Level) (*mesh.Mesh, error) {
	m := &mesh.Mesh{}
	for i, z := range l.Zones {
		m.P = append(m.P, mesh.Vertex{
			X:     float64(z.X()) + 0.5*float64(z.Width()),
			Y:     float64(z.Y()) + 0.5*float64(z.Height()),
			Index: i,
		})
	}
	if err := m.Triangulate(); err != nil {
		return nil, err
	}
	return m, nil
}
