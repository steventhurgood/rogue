package rogue

import "math/rand"

type Range struct {
	Min, Max int
}

// RandRange generates a random integer number between Min and Max, inclusive.
func RandomRange(rng *Range, r *rand.Rand) int {
	width := rng.Max - rng.Min
	n := r.Intn(width + 1)
	return rng.Min + n
}
