package layout

import (
	"fmt"
	"math"
	"math/rand"

	"bitbucket.org/steventhurgood/rogue"
	"github.com/golang/glog"
)

var (
	cloud = &CloudLayout{}
)

type CloudLayout struct {
	numRooms int
	roomSize int
	scale    float64
}

func (c *CloudLayout) Name() string { return "cloud" }

func (c *CloudLayout) Position(b rogue.BoxLike, z *rogue.Zone, r *rand.Rand) error {
	density := z.Config.LayoutOptions.Density
	if c.numRooms == 0 {
		c.numRooms = len(z.Rooms)
		roomSize, err := rogue.RandomNumber(z.Config.RoomSize, r)
		if err != nil {
			return fmt.Errorf("Error getting room size: %v", err)
		}
		c.roomSize = roomSize
		c.scale = 0.3 * float64(c.roomSize) * math.Sqrt(float64(c.numRooms)) / density
		glog.Infof("Cloud layout: %v rooms of size %v; %v density. x + y sd: %v", c.numRooms, c.roomSize, density, c.scale)
	}
	x := int(r.NormFloat64() * c.scale)
	y := int(r.NormFloat64() * c.scale)
	b.SetX(x)
	b.SetY(y)
	return nil
}

func init() {
	if err := rogue.RegisterLayout(cloud); err != nil {
		glog.Fatal(err)
	}
}
