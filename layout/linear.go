package layout

import (
	"fmt"
	"math/rand"

	"bitbucket.org/steventhurgood/rogue"
	"github.com/golang/glog"
)

var (
	horizontal = &LinearLayout{
		horizontal: true,
	}
	vertical = &LinearLayout{
		horizontal: false,
	}
)

type LinearLayout struct {
	horizontal bool
	numRooms   int
	roomSize   int
	scale      float64
}

func (c *LinearLayout) Name() string {
	if c.horizontal {
		return "horizontal"
	}
	return "vertical"
}

func (l *LinearLayout) Position(b rogue.BoxLike, z *rogue.Zone, r *rand.Rand) error {
	density := z.Config.LayoutOptions.Density
	jitter := z.Config.LayoutOptions.Jitter
	if l.numRooms == 0 {
		l.numRooms = len(z.Rooms)
		roomSize, err := rogue.RandomNumber(z.Config.RoomSize, r)
		if err != nil {
			return fmt.Errorf("Error getting room size: %v", err)
		}
		l.roomSize = roomSize
		l.scale = 0.3 * float64(l.roomSize*l.numRooms) / density
	}
	position := int(r.NormFloat64() * l.scale)
	j := int(r.NormFloat64() * jitter * float64(l.roomSize))

	if l.horizontal {
		b.SetX(position)
		b.SetY(j - b.Height()/2)
	} else {
		b.SetY(position)
		b.SetX(j - b.Width()/2)
	}
	return nil
}

func init() {
	if err := rogue.RegisterLayout(horizontal); err != nil {
		glog.Fatal(err)
	}
	if err := rogue.RegisterLayout(vertical); err != nil {
		glog.Fatal(err)
	}
}
