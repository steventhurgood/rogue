Level -
  overall container for a single map.
  comprises several zones - this partitions the level into several sections, which can be linear "zone 1 - zone 2 - zone 3 ..." or not.
  has an entrance and exit - may be from above/below like stairs/ladder/trapdoor, or sideways like a door

zones -
  organized to be distinct - perhaps use the same box + flocking approach to group zones together.
  has a theme. eg., dungeon, prison, sewer, castle, cave, town, canyon, ... ?
  each zone contains rooms
  TODO: think how to partition zones using eg., crevace, river, cliff, ...
  special zones: used primarily to hold a special "feature" room - a bridge over a chasm, or a large staircase, or ... something.

rooms -
  chosen according to a theme. Basic room is a box with walls and doors. Other rooms could be custom according to the theme. eg., throne room, cathedral, cell, 
  each room has a way of choosing doors, and a maximum number of doors. For regular rooms, doors could be randomly assigned around the room, for pre-fab rooms like dungons, doors might have to be shared. 
  rooms may also have a theme. eg., a dungeon might contain cells, mess halls, torture chambers, fighting pits, ...

doors - 
  are assigned to rooms, and could vary according to theme (eg, wooden door, cave entrance, collapsed wall, ...)
  have a size - small or large, which may depend upon room type (eg., the main door in a throne room would be large). A room might specify that a given door is "large", "small", or "don't care". Doors may be hidden, as with items, or they may require an item to unlock.

corridors -
  join doors. Corridors also vary according to theme (tunnel, cave) and room type at either end, randomly.
  corridors between zones wth different themes would take the theme of either end. eg., if one end were a castle and the other a dungeon, then the tunnel might be a walled passage that emerges in a cave; if the tunnel were a cave, then the castle-side "door" could be a hole in a wall, or a hidden entrance.
  corridors are small or large according to the sizes at either end. A large door - large door to a large door would result in a large corridor. Likewise small - small. Small - large or vice versa TBD (perhaps only have door options "large" and "don't care" - where Large - ? is a large tunnel, and ? - ? is a small tunnel.

features -
  rooms contain features. These vary according to the theme of the level and room. A dungeon might contain torture instruments or a statue, windows or corpses. Some features may conceal items (eg., a corpse may have a key on it, or a statue may be holding a shield; Some features, eg., a window will not have anything).
  Features may affect other aspects, like making a room lighter for windows or torches, or introducing water to the level like a crack in a cave wall, or a sewer pipe outlet.
  TBD - can features be hidden. eg., traps. Can features have behaviour when walked over - eg., cobwebs or vines.

items -
  rooms also contain items. These may vary according to the theme, or may be generic (eg., a key). These can be picked up. items may be concealed in features, or simply on the floor.


attributes:

zone - num rooms distribution
zone - room size distribution
level - theme distribution
zone - feature distribution
zone - item distribution
zone - room layout
level - zone layout

a config defines options and distributions for these attributes. A level or zone has specific values.

eg., a zone config will specify the room size in terms of mean and variance. A zone will have specific rooms with specific sizes.

a zone will have a specific theme, a level config will offer multiple themes for zones.


overriding:
  zone > theme > level

This is, a level may provide a room size. If the theme specifies a room size, then that will be used over the level. If the zone config specifies a room size, then that will be used over the theme.
