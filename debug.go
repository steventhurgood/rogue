package rogue

import (
	// "bitbucket.org/steventhurgood/rogue/mesh"

	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg"
)

type boxEdges struct {
	b      BoxLike
	offset [2]int
}

func (e boxEdges) Len() int {
	return 5
}

func SaveBoxes(b []BoxLike, filename string) error {
	p, err := plot.New()
	if err != nil {
		return err
	}
	if err := GraphBoxes(b, [2]int{0, 0}, p); err != nil {
		return err
	}
	p.Save(vg.Length(10)*vg.Centimeter, vg.Length(10)*vg.Centimeter, filename)
	return nil
}

func SaveLevel(l *Level, filename string) error {
	p, err := plot.New()
	if err != nil {
		return err
	}
	if err := GraphBoxes(ZoneToBoxLike(l.Zones), [2]int{0, 0}, p); err != nil {
		return err
	}
	for _, z := range l.Zones {
		if err := GraphBoxes(RoomToBoxLike(z.Rooms), [2]int{0, 0}, p); err != nil {
			return err
		}
		if err := GraphTunnels(z.Tunnels, p); err != nil {
			return err
		}
	}
	if err := GraphTunnels(l.Tunnels, p); err != nil {
		return err
	}
	// if l.Mesh != nil {
	//	if err := mesh.PlotMesh(l.Mesh, p); err != nil {
	//		return err
	//	}
	//}
	// if l.Skeleton != nil {
	//	if err := mesh.PlotSkeleton(l.Skeleton, p); err != nil {
	//		return err
	//	}
	//}
	p.Save(vg.Length(10)*vg.Centimeter, vg.Length(10)*vg.Centimeter, filename)
	return nil
}

func (e boxEdges) XY(i int) (float64, float64) {
	b := e.b
	xy := [][2]int{
		[2]int{b.X(), b.Y()},                          // top left
		[2]int{b.X() + b.Width(), b.Y()},              // top right
		[2]int{b.X() + b.Width(), b.Y() + b.Height()}, // bottom right
		[2]int{b.X(), b.Y() + b.Height()},             // bottom left
		[2]int{b.X(), b.Y()},                          // top left
	}
	return float64(xy[i][0] + e.offset[0]), float64(xy[i][1] + e.offset[1])
}

func GraphBoxes(b []BoxLike, offset [2]int, p *plot.Plot) error {
	for _, b := range b {
		e, err := plotter.NewLine(boxEdges{b, offset})
		if err != nil {
			return err
		}
		p.Add(e)
	}
	return nil
}

func GraphTunnels(tunnels []Tunnel, p *plot.Plot) error {
	for _, t := range tunnels {
		e, err := plotter.NewLine(t)
		if err != nil {
			return err
		}
		p.Add(e)
	}
	return nil
}
