package rogue

import (
	"fmt"
	"math/rand"

	"github.com/golang/glog"

	"bytes"
)

// Zone fully specifies a collection of rooms, with features, items, etc.
type Zone struct {
	Config         *ZoneConfig
	Level          *Level
	Rooms          []Room
	Tunnels        []Tunnel
	Layout         Layout
	RoomSeparation int

	Box
}

func (z *Zone) Join(a, b int) error {
	glog.Infof("Joining rooms: %v - %v", a, b)
	z.Tunnels = append(z.Tunnels, Tunnel{
		From: z.Rooms[a],
		To:   z.Rooms[b],
	})
	return nil
}

func (z *Zone) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "Config: %v\n", z.Config)
	fmt.Fprintf(&b, "Number of rooms: %v\n", len(z.Rooms))
	fmt.Fprintf(&b, "Layout: %v\n", z.Layout.Name())
	for i, r := range z.Rooms {
		fmt.Fprintf(&b, "%v:\n%v\n", i, r)
	}
	return b.String()
}

// ZoneConfig specifies the necessary parameters to generate a zone
type ZoneConfig struct {
	Theme *Theme

	ZoneAttributes
}

func (z *ZoneConfig) String() string {
	var b bytes.Buffer
	fmt.Fprint(&b, "Zone Config\n")
	fmt.Fprintf(&b, "Theme: %v\n", z.Theme)
	fmt.Fprintf(&b, "Attributes: %v\n", z.ZoneAttributes)
	return b.String()
}

// NewZone creates and populates a zone
func NewZone(zc *ZoneConfig, l *Level, r *rand.Rand) (*Zone, error) {
	z := &Zone{
		Config: zc,
		Level:  l,
	}
	CopyZoneAttributes(&zc.ZoneAttributes, &zc.Theme.ZoneAttributes, &l.Config.ZoneAttributes)
	numRooms, err := RandomNumber(zc.NumRooms, r)
	if err != nil {
		return nil, fmt.Errorf("Error generating number of rooms: %v", err)
	}
	roomSeparation, err := RandomNumber(zc.RoomSeparation, r)
	if err != nil {
		return nil, fmt.Errorf("Error generating room separation: %v", err)
	}
	layoutName, err := Choose(zc.Layout, r)
	if err != nil {
		return nil, fmt.Errorf("Error choosing layout: %v", err)
	}
	layout, err := GetLayout(layoutName)
	if err != nil {
		glog.Warningf("%v", zc)
		return nil, fmt.Errorf("Error getting layout: %v", err)
	}
	z.Rooms = make([]Room, numRooms)
	z.RoomSeparation = roomSeparation
	z.Layout = layout
	glog.Infof("Creating zone with %v rooms", numRooms)
	for i := range z.Rooms {
		roomType, err := Choose(zc.Rooms, r)
		if err != nil {
			glog.Warningf("%v", zc)
			return nil, fmt.Errorf("Error choosing room: %v", err)
		}
		glog.Infof("Generating room: %v", roomType)
		factory, err := GetRoom(roomType)
		if err != nil {
			glog.Warningf("%v", zc)
			return nil, fmt.Errorf("Error getting room factory: %v", err)
		}
		room := factory.New(z, r)
		SetRoomSize(room, z, r)
		err = layout.Position(room, z, r)
		if err != nil {
			return nil, fmt.Errorf("Error positioning room: %v", err)
		}
		z.Rooms[i] = room
	}
	Avoid(RoomToBoxLike(z.Rooms), l, roomSeparation)
	roomsToRemove := FilterOverlapping(RoomToBoxLike(z.Rooms), l, 1)
	filteredRooms := make([]Room, 0, len(z.Rooms))
	for i := range z.Rooms {
		if !roomsToRemove[i] {
			filteredRooms = append(filteredRooms, z.Rooms[i])
		}
	}
	z.Rooms = filteredRooms
	extents, err := NormalizeBox(RoomToBoxLike(z.Rooms))
	z.Box = *extents
	if err != nil {
		return nil, err
	}
	return z, nil
}

func ConfigFromTheme(themeName string, r *rand.Rand) (*ZoneConfig, error) {
	theme, err := GetTheme(themeName)
	if err != nil {
		return nil, err
	}
	zc := &ZoneConfig{
		Theme: theme,
	}
	return zc, nil
}

func ZoneToBoxLike(z []*Zone) []BoxLike {
	b := make([]BoxLike, len(z))
	for i := range z {
		b[i] = z[i]
	}
	return b
}
