package rogue

// package map contains the types and functions for defining and creating a map

import (
	"fmt"
	"math/rand"

	"bitbucket.org/steventhurgood/rogue/mesh"
	"github.com/golang/glog"

	"bytes"
)

// Level fully defines a specific instance of a level, with rooms, corridors, items, etc.
type Level struct {
	Zones   []*Zone
	Tunnels []Tunnel
	Config  *LevelConfig
	Box

	zoneSeparation int
	Layout         Layout

	Mesh     *mesh.Mesh
	Skeleton *mesh.Skeleton
}

// Join two zones, A & B
// To do this, find the closest pair of rooms, and add a tunnel to the level that points at those two rooms.

func (l *Level) Join(a, b int) error {
	fmt.Printf("Joining %v -> %v\n", a, b)
	zoneA, zoneB := l.Zones[a], l.Zones[b]
	minDist := -1.0
	minI, minJ := 0, 0
	for i, iRoom := range zoneA.Rooms {
		for j, jRoom := range zoneB.Rooms {
			d := BoxDistance(iRoom, jRoom)
			if minDist < 0 || d < minDist {
				minDist = d
				minI, minJ = i, j
			}
		}
	}
	fmt.Printf("Joining intra-zone rooms: [%v]%v and [%v]%v\n", a, minI, b, minJ)
	l.Tunnels = append(l.Tunnels, Tunnel{
		From: zoneA.Rooms[minI],
		To:   zoneB.Rooms[minJ],
	})
	return nil
}

func (l *Level) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "Level Config: %+v\n", l.Config)
	fmt.Fprintf(&b, "Num Zones: %v\n", len(l.Zones))
	for i, z := range l.Zones {
		fmt.Fprintf(&b, "Zone %v:\n%v\n", i, z)
	}
	return b.String()
}

// LevelConfig specifies the parameters for generating new levels
type LevelConfig struct {
	NumZones         *Random
	FixedZoneConfigs []*ZoneConfig
	Themes           []WeightedChoice

	// box collision avoidance parameters
	BoxRepulsion  float64
	BoxDamping    float64
	MaxIterations int

	// Zone layout parameters
	ZoneSeparation *Random
	Layout         []WeightedChoice
	LayoutOptions  *LayoutOptions

	// inheritable overridable zone parameters
	ZoneAttributes
}

// NewLevel creates a new level from a LevelConfig
func NewLevel(lc *LevelConfig, r *rand.Rand) (*Level, error) {
	l := &Level{
		Config: lc,
	}
	// pick layout
	if l.Config.Layout != nil {
		layoutName, err := Choose(l.Config.Layout, r)
		if err != nil {
			return nil, err
		}
		layout, err := GetLayout(layoutName)
		if err != nil {
			return nil, err
		}
		l.Layout = layout
	}

	// pick number of zones
	numZones, err := RandomNumber(lc.NumZones, r)
	if err != nil {
		return nil, err
	}
	l.Zones = make([]*Zone, numZones)

	for i := 0; i < numZones; i++ {
		var zc *ZoneConfig
		if i < len(lc.FixedZoneConfigs) {
			zc = lc.FixedZoneConfigs[i]
			CopyZoneAttributes(&zc.ZoneAttributes, &lc.FixedZoneConfigs[i].ZoneAttributes)
		} else {
			// choose a theme, and create a new zone config based on that?
			var err error
			themeName, err := Choose(lc.Themes, r)
			if err != nil {
				return nil, err
			}
			zc, err = ConfigFromTheme(themeName, r)
			if err != nil {
				return nil, err
			}
		}
		z, err := NewZone(zc, l, r)
		if err != nil {
			return nil, err
		}
		if l.Layout != nil {
			l.Layout.Position(z, z, r)
		}
		l.Zones[i] = z
	}
	glog.Infof("Avoiding Zones")
	Avoid(ZoneToBoxLike(l.Zones), l, 1)
	for i := range l.Zones {
		glog.Infof("Zone box %v: %v", i, l.Zones[i].Box)
	}
	extents, err := NormalizeBox(ZoneToBoxLike(l.Zones))
	if err != nil {
		return nil, err
	}
	glog.Infof("Level extents: %v", extents)
	for _, z := range l.Zones {
		// offset := [2]int{extents.X() + z.X(), extents.Y() + z.Y()}
		glog.Infof("  Zone extents: %v", z.Box)
		for _, r := range z.Rooms {
			glog.Infof("    Room extents: (%v, %v, %v, %v)", r.X(), r.Y(), r.Width(), r.Height())
			r.SetX(r.X() + z.X())
			r.SetY(r.Y() + z.Y())
		}
		// z.SetX(offset[0])
		// z.SetY(offset[1])
	}
	extents.SetX(0) // all rooms and zones have been moved relative to 0,0
	extents.SetY(0)
	l.Box = *extents
	skeletons := make([]*mesh.Skeleton, len(l.Zones))
	for i, z := range l.Zones {
		m, err := ZoneToMesh(z)
		if err != nil {
			fmt.Printf("Error converting zone %v to mesh: %v\n", i, err)
			fmt.Printf("%v", z)
			SaveBoxes(RoomToBoxLike(z.Rooms), "broken.svg")
			return nil, err
		}
		s, err := mesh.BetaSkeleton(m, 1.0)
		if err != nil {
			fmt.Printf("Error converting zone %v to skeleton\n", i)
			return nil, err
		}
		fmt.Printf("Joining zone %v\n", i)
		if err := s.Join(z, 0.8, r); err != nil {
			fmt.Printf("Error joining zone %v to skeleton\n", i)
			return nil, err
		}
		skeletons[i] = s
	}
	m, err := LevelToMesh(l)
	if err != nil {
		fmt.Printf("Error converting level to mesh: %v\n", err)
		return nil, err
	}
	s, err := mesh.BetaSkeleton(m, 1.0)
	if err != nil {
		fmt.Printf("Error converting level to skeleton\n")
		return nil, err
	}
	fmt.Printf("Joining level\n")
	if err := s.Join(l, 0.0, r); err != nil {
		fmt.Printf("Error joining level to skeleton\n")
		return nil, err
	}
	l.Skeleton = s
	/*
		if m, err := LevelToMesh(l); err != nil {
			return nil, err
		} else {
			l.Mesh = m
		}
		if s, err := mesh.BetaSkeleton(l.Mesh, 1.0); err != nil {
			return nil, err
		} else {
			l.Skeleton = s
		}
	*/
	return l, nil
}
