package rogue

import (
	"reflect"
	"testing"
)

func TestRoundUpVelocities(t *testing.T) {
	tests := [][2]float64{
		[2]float64{0.1, 0.1},
		[2]float64{0.2, -0.2},
		[2]float64{0.3, 0.3},
		[2]float64{0.4, 0.4},
		[2]float64{-0.5, 0.5},
		[2]float64{1.6, -1.6},
	}
	want := [][2]int{
		[2]int{0, 0},
		[2]int{0, 0},
		[2]int{0, 0},
		[2]int{1, 1},
		[2]int{-1, 1},
		[2]int{2, -2},
	}

	got := roundUpVelocities(tests, 0.5)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("roundUpVelocities(%v) ->\n%v\nwant:\n%v", tests, got, want)
	}
}
