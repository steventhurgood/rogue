package main

import (
	"flag"
	"math/rand"

	"bitbucket.org/steventhurgood/rogue"
	_ "bitbucket.org/steventhurgood/rogue/layout"
	_ "bitbucket.org/steventhurgood/rogue/room"
	_ "bitbucket.org/steventhurgood/rogue/theme"

	"github.com/golang/glog"
)

// generate a map from a config file

var (
	seed = flag.Int64("seed", 0, "Seed used to drive randomness")
)

func main() {
	flag.Parse()
	lc := &rogue.LevelConfig{
		NumZones: &rogue.Random{Limits: &rogue.Range{10, 20}},
		FixedZoneConfigs: []*rogue.ZoneConfig{&rogue.ZoneConfig{
			Theme: rogue.GetThemeOrDie("dungeon"),
			ZoneAttributes: rogue.ZoneAttributes{
				NumRooms: &rogue.Random{Limits: &rogue.Range{10, 20}},
				// RoomSize: &rogue.Random{LogNormal: &rogue.DistParams{10, 3}},
				Layout: []rogue.WeightedChoice{
					rogue.WeightedChoice{1.0, "horizontal"},
				},
			},
		}},
		ZoneAttributes: rogue.ZoneAttributes{
			NumRooms: &rogue.Random{Limits: &rogue.Range{20, 100}},
			Layout: []rogue.WeightedChoice{
				rogue.WeightedChoice{10.0, "cloud"},
				rogue.WeightedChoice{1.0, "vertical"},
				rogue.WeightedChoice{1.0, "horizontal"},
			},
			RoomSeparation: &rogue.Random{
				Limits: &rogue.Range{3, 5},
			},
			LayoutOptions: &rogue.LayoutOptions{
				Density: 1.0,
				Jitter:  0.01,
			},
		},
		Themes: []rogue.WeightedChoice{
			rogue.WeightedChoice{1.0, "dungeon"},
			rogue.WeightedChoice{1.0, "infernal"},
		},
		BoxRepulsion:  1.0,
		BoxDamping:    0.9,
		MaxIterations: 1000,
	}
	r := rand.New(rand.NewSource(*seed))
	l, err := rogue.NewLevel(lc, r)
	if err != nil {
		glog.Fatal(err)
	}
	glog.Info(l)
	rogue.SaveLevel(l, "level.svg")
	tiles, err := rogue.Render(l, r)
	if err != nil {
		glog.Fatal(err)
	}
	rogue.PngTiles(tiles, "map.png")
}
