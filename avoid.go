package rogue

import (
	"fmt"
	"math"
	"sort"

	"github.com/golang/glog"
)

// Methods for causing box-like structures to avoid each other.

// FilterOverlapping returns a boolean slice of boxes to remove such that none are overlapping
func FilterOverlapping(b []BoxLike, l *Level, separation int) []bool {
	remove := make([]bool, len(b))
	for i := 0; i < len(b)-1; i++ {
		for j := i + 1; j < len(b); j++ {
			if Overlapping(b[i], b[j], separation) {
				remove[i] = true
			}
		}
	}
	return remove
}

// Avoid causes box-like structures to move apart to prevent overlapping
func Avoid(b []BoxLike, l *Level, separation int) {
	for i := range b {
		glog.Infof("Avoiding box: (%v, %v, %v, %v)", b[i].X(), b[i].Y(), b[i].Width(), b[i].Height())
	}
	repulsion := l.Config.BoxRepulsion
	damping := l.Config.BoxDamping
	boxes := asBoxes(b)
	// move boxes first of all, then apply to real rooms.
	forces := make([][2]float64, len(boxes))
	velocities := make([][2]float64, len(boxes))
	overlapping := true
	iteration := 0
	moving := make([]bool, len(boxes))
	for overlapping && iteration < l.Config.MaxIterations {
		glog.V(3).Infof("Overlapping iteration: %v", iteration)
		iteration++
		if glog.V(3) {
			SaveBoxes(BoxToBoxLike(boxes), fmt.Sprintf("avoid-%v.png", iteration))
		}
		overlapping = false
		for i := 0; i < len(boxes); i++ {
			moving[i] = false
			forces[i][0] = 0.0
			forces[i][1] = 0.0
		}
		for i := 0; i < len(boxes)-1; i++ {
			for j := i + 1; j < len(boxes); j++ {
				if Overlapping(boxes[i], boxes[j], separation) {
					overlapping = true
					moving[i] = true
					moving[j] = true
					diff := diffVector(boxes[i], boxes[j])
					glog.V(2).Infof("Diff %v - %v: %v", boxes[i], boxes[j], diff)
					forces[i][0] += diff[0] * repulsion
					forces[i][1] += diff[1] * repulsion

					forces[j][0] -= diff[0] * repulsion
					forces[j][1] -= diff[1] * repulsion
				}
			}
		}
		for i := range boxes {
			velocities[i][0] += forces[i][0]
			velocities[i][1] += forces[i][1]
		}
		// pick midpoint, update positions
		toMove := roundUpVelocities(velocities, 0.5)
		for i := range boxes {
			glog.V(2).Infof("%3v: %10v %10v %10v", i, forces[i], velocities[i], toMove[i])
			// movex, movey := roundUpVelocities(velocities, above)
			// boxes[i].SetX(boxes[i].X() + movex)
			// boxes[i].SetY(boxes[i].Y() + movey)
			if moving[i] {
				boxes[i].SetX(boxes[i].X() + toMove[i][0])
				boxes[i].SetY(boxes[i].Y() + toMove[i][1])
			}

			velocities[i][0] *= damping
			velocities[i][1] *= damping
		}
		if !overlapping {
			glog.Infof("Collision avoidance in %v iterations")
		}
	}
	for i := range boxes {
		b[i].SetX(boxes[i].X())
		b[i].SetY(boxes[i].Y())
	}
	for i := range b {
		glog.Infof("Avoided box: (%v, %v, %v, %v)", b[i].X(), b[i].Y(), b[i].Width(), b[i].Height())
	}
}

// roundUpVelocities takes a slice of vectors and rounds up those above some quantile threshold, and rounds down those below the threshold
func roundUpVelocities(v [][2]float64, quantile float64) [][2]int {
	above := aboveQuantile(v, quantile)
	r := make([][2]int, len(v))
	for i := range r {
		x := v[i][0]
		y := v[i][1]
		if above[i] {

			if x > 0 {
				r[i][0] = int(math.Ceil(x))
			} else {
				r[i][0] = int(math.Floor(x))
			}
			if y > 0 {
				r[i][1] = int(math.Ceil(y))
			} else {
				r[i][1] = int(math.Floor(y))
			}
		} else {
			r[i][0] = int(math.Floor(x) + 0.5)
			r[i][1] = int(math.Floor(y) + 0.5)
		}
	}
	return r
}

// median velocity gives the median absolute velocity scalar
func aboveQuantile(v [][2]float64, quantile float64) []bool {
	m := make([]float64, len(v))
	above := make([]bool, len(v))

	for i := range v {
		m[i] = vectorLength(v[i])
	}
	sort.Float64s(m)
	midPoint := int(quantile * float64(len(m)))
	midValue := m[midPoint]
	for i := range v {
		above[i] = vectorLength(v[i]) >= midValue
	}
	return above
}

func vectorLength(a [2]float64) float64 {
	return math.Sqrt(a[0]*a[0] + a[1]*a[1])
}

// the vector from b to a, normalised
func diffVector(a, b *Box) [2]float64 {
	aCentre := BoxCentre(a)
	bCentre := BoxCentre(b)

	v := [2]float64{
		float64(aCentre[0] - bCentre[0]),
		float64(aCentre[1] - bCentre[1]),
	}
	mag := math.Sqrt(v[0]*v[0] + v[1]*v[1])
	if mag == 0 {
		return [2]float64{0, 0}
	}
	v[0] /= (mag * mag * mag) // force normalized, then proportional to distance^-2
	v[1] /= (mag * mag * mag)
	return v
}
func asBoxes(b []BoxLike) []*Box {
	r := make([]*Box, len(b))
	for i := range b {
		r[i] = &Box{
			x:      b[i].X(),
			y:      b[i].Y(),
			width:  b[i].Width(),
			height: b[i].Height(),
		}
	}
	return r
}

// Overlapping determins whether two box-like structures overlap, with some amount of separation enforced.
func Overlapping(a, b BoxLike, sep int) bool {
	// check if any of the vertices of A are contained in B+sep.
	switch {
	case a.X()+a.Width()+sep <= b.X():
		{
			return false
		} // a completely to the left of b
	case a.Y()+a.Height()+sep <= b.Y():
		{
			return false
		} // a completely above b
	case a.X() >= b.X()+b.Width()+sep:
		{
			return false
		} // a completely to the right of b
	case a.Y() >= b.Y()+b.Height()+sep:
		{
			return false
		} // a completely below b
	}
	return true
}
