package main

import (
	"flag"
	"log"
	"net/http"

	gw "bitbucket.org/steventhurgood/rogueai"
	"google.golang.org/grpc"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"golang.org/x/net/context"
)

var (
	backend = flag.String("backend", "10.110.171.106:8080", "endpoint of dungeon service")
	listen = flag.String("listen", ":8081", "host:port to listen on")
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	if err := gw.RegisterDungeonHandlerFromEndpoint(ctx, mux, *backend, opts); err != nil {
		return err
	}
	return http.ListenAndServe(*listen, mux)
}

func main() {
	flag.Parse()

	if err := run(); err != nil {
		log.Fatal(err)
	}
}
