"use strict";
exports.__esModule = true;
var PIXI = require("pixi.js");
var viewport_1 = require("./viewport");
var MovingAverage = /** @class */ (function () {
    function MovingAverage(n) {
        this.n = n;
        this.points = new Array();
        this.total = 0;
        this.nextI = 0;
        for (var i = 0; i < n; i++) {
            this.points.push(0.0);
        }
    }
    MovingAverage.prototype.next = function (value) {
        this.total -= this.points[this.nextI];
        this.total += value;
        this.points[this.nextI] = value;
        this.nextI = (this.nextI + 1) % this.n;
    };
    MovingAverage.prototype.average = function () {
        return this.total / this.n;
    };
    return MovingAverage;
}());
var Keyboard = /** @class */ (function () {
    function Keyboard(code) {
        this.code = code;
        this.isDown = false;
        this.isUp = true;
    }
    ;
    Keyboard.prototype.onPress = function (handler) {
        this.press = handler;
        return this;
    };
    Keyboard.prototype.onRelease = function (handler) {
        this.release = handler;
        return this;
    };
    Keyboard.prototype.register = function () {
        var _this = this;
        this.downHandler = function (event) {
            if (event.keyCode == _this.code) {
                if (_this.isUp && _this.press) {
                    _this.press();
                }
                _this.isDown = true;
                _this.isUp = false;
                event.preventDefault();
            }
        };
        this.upHandler = function (event) {
            if (event.keyCode == _this.code) {
                if (_this.isDown && _this.release) {
                    _this.release();
                }
                _this.isDown = false;
                _this.isUp = true;
                event.preventDefault();
            }
        };
        window.addEventListener("keydown", function (event) { return _this.downHandler(event); });
        window.addEventListener("keyup", function (event) { return _this.upHandler(event); });
    };
    return Keyboard;
}());
var Box = /** @class */ (function () {
    function Box(left, right, top, bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }
    Box.prototype.register = function (container) {
        var line = new PIXI.Graphics();
        line.lineStyle(4, 0xFFFFFF, 1);
        line.moveTo(this.left, this.top);
        line.lineTo(this.right, this.top);
        line.lineTo(this.right, this.bottom);
        line.lineTo(this.left, this.bottom);
        line.lineTo(this.left, this.top);
        container.addChild(line);
    };
    return Box;
}());
var Actor = /** @class */ (function () {
    function Actor(sprite, width, height, border) {
        this.sprite = sprite;
        var filter = new PIXI.filters.ColorMatrixFilter();
        filter.hue(this.randomInt(0, 360), false);
        sprite.filters = [filter];
        var scale = Math.random() + 0.5;
        this.border = border;
        sprite.width *= scale;
        sprite.height *= scale;
        // need a number between 0+border and width-border-
        sprite.x = this.randomInt(this.border, width - sprite.width - border);
        sprite.y = this.randomInt(this.border, height - sprite.height - border);
        this.xv = this.randomInt(-3, 3);
        this.yv = this.randomInt(-3, 3);
        this.gravity = 0.1;
        this.damping = 0.9;
    }
    Actor.prototype.randomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    Actor.prototype.jiggle = function () {
        this.xv = this.randomInt(-3, 3);
        this.yv = this.randomInt(-3, 3);
    };
    Actor.prototype.register = function (stage) {
        stage.addChild(this.sprite);
    };
    Actor.prototype.move = function (delta) {
        this.sprite.x += this.xv * delta;
        this.sprite.y += this.yv * delta;
        this.yv += (delta * this.gravity);
        if (this.sprite.x > (800 - this.sprite.width - this.border)) {
            this.sprite.x = 2 * (800 - this.sprite.width - this.border) - this.sprite.x;
            this.xv *= -1;
        }
        /*
         * if x=3, border=5
         * x should be 7
         * (border) + (border-x)
         *
         * x - -1, border=5
         * x should be 11
         */
        if (this.sprite.x < this.border) {
            this.sprite.x = this.border + (this.border - this.sprite.x);
            this.xv *= -1;
        }
        if (this.sprite.y > (600 - this.sprite.height - this.border)) {
            /*
             * if height = 10
             * width = 600
             * border = 10 (590)
             * x = 585 (5 pixels over the border)
             * needs to move to
             * x - 575
             * x - (width-border-sprite.width)  = x - (600 - 10 - 10) = 580
             * new x = (width-border-sprite.width) - (x - (width-border-sprite.width)
             * new x = 2 * (width-border-sprite.width) - x
             *  eg., 2 * (600-10-10) = 2 * 580 = 1060
             *  newX = 1060 - x
             *  eg., 1160 - 585 = 585
             * 1200 - 595 = 605
             *
             */
            this.sprite.y = 2 * (600 - this.sprite.height - this.border) - this.sprite.y;
            this.yv *= -1 * this.damping;
        }
        if (this.sprite.y < this.border) {
            this.sprite.y = this.border + (this.border - this.sprite.y);
            this.yv *= -1;
        }
    };
    return Actor;
}());
function main() {
    var width = 800;
    var height = 600;
    var border = 10;
    var num_skulls = 20;
    var app = new PIXI.Application(width, height, {});
    var container = document.getElementById("main");
    container.appendChild(app.view);
    var actors = new Array();
    var dungeonMap = viewport_1.TileGrid.newRandomGrid(100, 100);
    var viewport = new viewport_1.Viewport(10.5, 10.5, 30, 28, 32 * 10 * 2, 32 * 7 * 2, dungeonMap);
    console.log("Starting: ", PIXI);
    var type = "WebGL";
    if (!PIXI.utils.isWebGLSupported()) {
        type = "canvas";
    }
    var deltaText = new PIXI.Text('pixi', { fill: 0xFFFFFF });
    PIXI.utils.sayHello(type);
    PIXI.loader.add([
        "images/skull.png",
        "images/wall.png",
        "images/blocked.png",
        "images/floor.png"
    ]).load(function () {
        var container = new PIXI.Container();
        container.x = 100;
        container.width = 32 * 10 * 2;
        container.y = 100;
        container.height * 32 * 7 * 2;
        // let viewport = new Viewport(10, 10, 10, 7, 32*10, 32*7, dungeonMap, container);
        viewport.register(container);
        app.stage.addChild(container);
        var box = new Box(border, width - border, border, height - border);
        var viewbox = new Box(100, 100 + 32 * 10 * 2, 100, 100 + 32 * 7 * 2);
        box.register(app.stage);
        viewbox.register(app.stage);
        var _loop_1 = function (i) {
            var sprite = new PIXI.Sprite(PIXI.loader.resources["images/skull.png"].texture);
            sprite.interactive = true;
            var actor = new Actor(sprite, width, height, border);
            sprite.on('mousedown', function (event) { return actor.jiggle(); });
            actor.register(app.stage);
            actors.push(actor);
        };
        for (var i = 0; i < num_skulls; i++) {
            _loop_1(i);
        }
        deltaText.x = 700;
        deltaText.y = 550;
        app.stage.addChild(deltaText);
    });
    app.stage.filters = [];
    var left = new Keyboard(37)
        .onPress(function () { viewportxv = -0.001; })
        .onRelease(function () { viewportxv = 0; })
        .register();
    var right = new Keyboard(39)
        .onPress(function () { viewportxv = 0.001; })
        .onRelease(function () { viewportxv = 0; })
        .register();
    var up = new Keyboard(38)
        .onPress(function () { viewportyv = -0.001; })
        .onRelease(function () { viewportyv = 0; })
        .register();
    var down = new Keyboard(40)
        .onPress(function () { viewportyv = 0.001; })
        .onRelease(function () { viewportyv = 0; })
        .register();
    app.stage.interactive = true;
    var fps = new MovingAverage(120);
    var viewportx = 1;
    var viewporty = 1;
    var viewportxv = 0.001;
    var viewportyv = 0.001;
    app.ticker.add(function (delta) {
        fps.next(app.ticker.FPS);
        deltaText.text = "" + fps.average();
        for (var _i = 0, actors_1 = actors; _i < actors_1.length; _i++) {
            var actor = actors_1[_i];
            actor.move(delta);
            viewport.move(viewportx, viewporty);
            viewportx = viewportx + delta * viewportxv;
            viewporty = viewporty + delta * viewportyv;
            if (viewportx < 2) {
                viewportxv = 0.001;
            }
            if (viewportx > dungeonMap.width - 2) {
                viewportxv = -0.001;
            }
            if (viewporty < 2) {
                viewportyv = 0.001;
            }
            if (viewporty > dungeonMap.height - 2) {
                viewportyv = -0.001;
            }
        }
    });
}
window.onload = main;
