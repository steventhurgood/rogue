
import * as PIXI from "pixi.js";
import { Viewport, TileGrid } from "./viewport";

class MovingAverage {
  private points : Array<number>;
  private total : number;
  private nextI : number;

  constructor(private n : number) {
    this.points = new Array();
    this.total = 0;
    this.nextI=0;
    for (let i =0; i < n; i++) {
      this.points.push(0.0);
    }
  }

  next(value : number) {
    this.total -= this.points[this.nextI];
    this.total += value;
    this.points[this.nextI] = value;
    this.nextI = (this.nextI + 1) % this.n;
  }

  average() {
    return this.total / this.n;
  }
}

class Keyboard {
  private press : () => void;
  private release : () => void;
  private downHandler : (any) => void;
  private upHandler : (any) => void;
  private isUp : boolean;
  private isDown : boolean;

  constructor(private code: number) {
    this.isDown = false;
    this.isUp = true;
  };

  onPress(handler: () => void) : Keyboard {
    this.press = handler;
    return this;
  }
  onRelease(handler: () => void) : Keyboard {
    this.release = handler;
    return this;
  }

  register() : void {
    this.downHandler = (event) => {
      if (event.keyCode == this.code) {
        if (this.isUp && this.press) {
          this.press();
        }
        this.isDown = true;
        this.isUp = false;
      event.preventDefault();
      }
    };
    this.upHandler = (event) => {
      if (event.keyCode == this.code) {
        if (this.isDown && this.release) {
          this.release();
        }
        this.isDown = false;
        this.isUp = true;
      event.preventDefault();
      }
    };
    window.addEventListener("keydown", (event) => this.downHandler(event));
    window.addEventListener("keyup", (event) => this.upHandler(event));
  }
}

class Box {
  private left : number;
  private right : number;
  private top : number;
  private bottom : number;
  private graphics : PIXI.Graphics;

  constructor(left : number, right : number , top : number, bottom : number) {
    this.left = left;
    this.right = right;
    this.top = top;
    this.bottom = bottom;
  }

  public register(container : PIXI.Container) {
    let line = new PIXI.Graphics();
    line.lineStyle(4, 0xFFFFFF, 1);
    line.moveTo(this.left, this.top);
    line.lineTo(this.right, this.top);
    line.lineTo(this.right, this.bottom);
    line.lineTo(this.left, this.bottom);
    line.lineTo(this.left, this.top);
    container.addChild(line);

  }
}

class Actor {
  private sprite: PIXI.Sprite;

  private xv : number;
  private yv: number;
  private gravity : number;
  private damping: number;
  private border: number;

  private randomInt(min : number, max : number) : number {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  public jiggle() : void {
    this.xv = this.randomInt(-3, 3);
    this.yv = this.randomInt(-3, 3);
  }

  constructor(sprite : PIXI.Sprite, width: number, height: number, border: number) {
    this.sprite = sprite;
    let filter = new PIXI.filters.ColorMatrixFilter();
    filter.hue(this.randomInt(0, 360), false);

    sprite.filters = [filter];
    let scale = Math.random() + 0.5;
    this.border = border;
    sprite.width *= scale;
    sprite.height *= scale;
    // need a number between 0+border and width-border-
    sprite.x = this.randomInt(this.border, width-sprite.width-border) ;
    sprite.y = this.randomInt(this.border, height-sprite.height-border);
    this.xv = this.randomInt(-3, 3);
    this.yv = this.randomInt(-3, 3);
    this.gravity = 0.1;
    this.damping = 0.9;
  }

  public register(stage : PIXI.Container) {
    stage.addChild(this.sprite);
  }

  public move(delta: number) : void {
    this.sprite.x += this.xv * delta;
    this.sprite.y += this.yv * delta;

    this.yv += (delta * this.gravity);

    if (this.sprite.x > (800-this.sprite.width-this.border)) {
      this.sprite.x = 2 * (800 - this.sprite.width - this.border) - this.sprite.x;
      this.xv *= -1;
    }
    /*
     * if x=3, border=5
     * x should be 7
     * (border) + (border-x)
     *
     * x - -1, border=5
     * x should be 11
     */
    if (this.sprite.x < this.border) {
      this.sprite.x = this.border + (this.border-this.sprite.x);
      this.xv *= -1;
    }
    if (this.sprite.y > (600-this.sprite.height-this.border)) {
      /*
       * if height = 10
       * width = 600
       * border = 10 (590)
       * x = 585 (5 pixels over the border)
       * needs to move to
       * x - 575
       * x - (width-border-sprite.width)  = x - (600 - 10 - 10) = 580
       * new x = (width-border-sprite.width) - (x - (width-border-sprite.width)
       * new x = 2 * (width-border-sprite.width) - x
       *  eg., 2 * (600-10-10) = 2 * 580 = 1060
       *  newX = 1060 - x
       *  eg., 1160 - 585 = 585
       * 1200 - 595 = 605
       *
       */
      this.sprite.y = 2 * (600 - this.sprite.height - this.border) - this.sprite.y;
      this.yv *= -1 * this.damping;
    }
    if (this.sprite.y < this.border) {
      this.sprite.y = this.border + (this.border-this.sprite.y);
      this.yv *= -1;
    }
  }
}

function main() : void {
  const width = 800;
  const height = 600;
  const border = 10;
  const num_skulls = 20;


  let app = new PIXI.Application(width, height,  {});
  let container = document.getElementById("main");
  container.appendChild(app.view)
  let actors : Array<Actor> = new Array();
  let dungeonMap : TileGrid = TileGrid.newRandomGrid(100, 100);
  let viewport = new Viewport(10.5, 10.5, 30, 28, 32*10*2, 32*7*2, dungeonMap);

  console.log("Starting: ", PIXI); 
  let type = "WebGL";
  if (!PIXI.utils.isWebGLSupported()) {
    type = "canvas";
  }
  let deltaText = new PIXI.Text('pixi', {fill: 0xFFFFFF});


  PIXI.utils.sayHello(type);
  PIXI.loader.add([
    "images/skull.png",
    "images/wall.png",
    "images/blocked.png",
    "images/floor.png"]).load(
    () => {
      let container = new PIXI.Container();
      container.x = 100;
      container.width =32 * 10*2;
      container.y = 100;
      container.height * 32*7*2;
      // let viewport = new Viewport(10, 10, 10, 7, 32*10, 32*7, dungeonMap, container);
      viewport.register(container);
      app.stage.addChild(container);
      let box = new Box(border, width-border, border, height-border);
      let viewbox = new Box(100, 100+32*10*2 , 100, 100 + 32*7*2);
      box.register(app.stage);
      viewbox.register(app.stage);
      for (let i=0; i < num_skulls; i++) {
        let sprite = new PIXI.Sprite(PIXI.loader.resources["images/skull.png"].texture);
        sprite.interactive = true;
        let actor = new Actor(sprite, width, height, border);
        sprite.on('mousedown', (event) => actor.jiggle() );
        actor.register(app.stage);
        actors.push(actor);
      }
      deltaText.x = 700;
      deltaText.y = 550;
      app.stage.addChild(deltaText);
    }
  );

  app.stage.filters = [
  ];

  let left = new Keyboard(37)
    .onPress(() => { viewportxv = -0.001; })
    .onRelease(() => { viewportxv = 0; })
    .register();

  let right = new Keyboard(39)
    .onPress(() => { viewportxv = 0.001; })
    .onRelease(() => { viewportxv = 0; })
    .register();
  let up = new Keyboard(38)
    .onPress(() => { viewportyv = -0.001; })
    .onRelease(() => { viewportyv = 0; })
    .register();
  let down = new Keyboard(40)
    .onPress(() => { viewportyv = 0.001; })
    .onRelease(() => { viewportyv = 0; })
    .register();

  app.stage.interactive = true;

  let fps = new MovingAverage(120);


  let viewportx = 1;
  let viewporty = 1;
  let viewportxv = 0.001;
  let viewportyv = 0.001;
  app.ticker.add(delta => {
    fps.next(app.ticker.FPS);
    deltaText.text = "" + fps.average();
    for (let actor of actors) {
      actor.move(delta);
      viewport.move(viewportx, viewporty);
      viewportx = viewportx + delta * viewportxv
      viewporty = viewporty + delta * viewportyv
      if (viewportx  < 2) {
        viewportxv = 0.001 ;
      }
      if (viewportx > dungeonMap.width-2) {
        viewportxv = -0.001;
      }
      if (viewporty < 2) {
        viewportyv = 0.001;
      }
      if (viewporty > dungeonMap.height-2) {
        viewportyv = -0.001;
      }
    }
  });
}


window.onload = main
