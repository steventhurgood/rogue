import * as PIXI from "pixi.js";

/*
 * Viewport converts map coordinates into screen coordinates.
 * 
 * For exmaple, if the viewport is 320x224, shows 10x7 tiles, and tile sare 32x32
 * A viewport has screen coordinates in pixels, and also offset in tile coordinates
 *
 * eg., the viewport may be at screen position 100, 100 and the tile offset position may be 3, -1
 *
 * As such a tile at 4,0 would have its sprite's x and y position converted to screen coordinates as:
 *
 * find map position: if map top left is (3, -1) then the relative position here would be (4, 1)
 *
 * We then convert this into screen coordinates relative to the viewport:
 *
 * 4 * 32 = 128
 * 1 * 32 = 32
 *
 */

// a tile represents an individual tile on the map
class Tile {
  constructor(public passable : boolean, public texture : string) {}

  setPassable(passable : boolean) {
    this.passable = passable;
  }
  setTexture(texture : string) {
    this.texture = texture;
  }
}

// a tilegrid contains a 2 dimensional array of tiles.
class TileGrid {

  static newRandomGrid(width: number, height: number) : TileGrid {
    let textures : Array<string> = ["blocked", "floor", "wall"];
    let grid = new TileGrid(width, height);
    for (let i=0; i < width * height; i++ ) {
      let textureIndex = Math.floor(Math.random() * 3);
      grid.tiles[i].setTexture(textures[textureIndex]);
    }
    return grid;
  }

  private tiles : Array<Tile>;
  constructor(public width : number, public height: number) {
    // width and height are measured in tiles
    this.tiles = new Array();
    for (let i = 0; i < width * height; i++) {
      this.tiles.push(new Tile(false, "fill"));
    }
  }

  getTile(x : number, y : number) : Tile{
    if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
      return undefined;
    }
    return this.tiles[y*this.width + x];
  }

  // set a tile position to a value. Return the old tile.
  setTile(x : number, y : number, tile : Tile) : Tile {
    if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
      return undefined;
    }
    let oldTile : Tile = this.tiles[y*this.width + x];
    this.tiles[y*this.width + x] = tile;
    return oldTile;
  }
}

// a widthxheight array of sprites.
// each sprite has a position x/y, and a width that is controlled by a viewport.
// when the viewport is moved, the sprite grid updates such that the x/y values for individual tiles are correct, and that
// the tiles' texture values are correct
// inefficient naive method; every time the viewport is moved, reload every sprite in the array.
class SpriteGrid {
  private sprites : Array<PIXI.Sprite>;

  constructor(private width: number, private height: number) {
    // assume that the sprites have already been loaded into the texture cache.
    this.sprites = new Array();
  }
}

class Viewport {
  private sprites : Array<PIXI.Sprite>;
  private tileWidth : number;
  private tileHeight : number;
  private spriteGridWidth : number;
  private spriteGridHeight: number;
  private spriteWidth : number;
  private spriteHeight : number;

  constructor(
    private left : number, // left position in tile coordinates
    private top : number, // top position in tile coordinates
    private width: number, // width in tiles,
    private height: number, // height in tiles,
    private windowWidth: number, // with of window in pixels,
    private windowHeight: number, // height of window in pixels,
    private map : TileGrid, // underlying map
  ) {

    this.spriteWidth = windowWidth / width;
    this.spriteHeight = windowHeight / height;
    this.spriteGridWidth = width + 2;
    this.spriteGridHeight = height + 2;

    // viewport cannot go within 1 tile of the edge, because of the need to maintain a border.
    // eg., left=1, sprites at x=0 loaded.
    if (this.left < 1) {
      this.left = 1;
    }
    // eg., width=10, map=20. Maximum rightmost tile=19, so max left=8
    if (this.left + this.width > map.width - 2) {
      this.left = map.width - this.width - 2;
    }
    if (this.top < 1) {
      this.top = 1;
    }
    if (this.top + this.height > map.height - 2) {
      this.top = map.height - this.height - 2;
    }
  }

  register(container : PIXI.Container) : void {

    // if viewport is tileWidth tiles wide, we need n+2 sprites.
    // populate sprites from left-1, top-1 to left+1
    //
    // if left=9.5 then the tile at the leftmost edge should be 9
    this.sprites = new Array<PIXI.Sprite>();
    for (let i=0; i<this.height+2; i++) {
      for (let j=0; j < this.width+2; j++) {

        let x = Math.floor(this.left + j);
        let y = Math.floor(this.top + i);
        let tile = this.map.getTile(x, y);
        if (!tile) {
          console.log("Error - could not get tile at " + x + ", " + y);
          return;
        }
        let newSprite = new PIXI.Sprite(PIXI.loader.resources["images/" + tile.texture + ".png"].texture);
        newSprite.width = this.spriteWidth;
        newSprite.height = this.spriteHeight;
        newSprite.x = (x-this.left)*this.spriteWidth;
        newSprite.y = (y-this.top)*this.spriteHeight;
        this.sprites.push(newSprite);
        let cliprect = new PIXI.Graphics().drawRect(container.x, container.y, this.windowWidth, this.windowHeight);
        container.mask = cliprect;
        container.addChild(newSprite);
      }
    }
  }

  move(left : number, top : number) {
    if (left === this.left && top === this.top) {
      return;
    }
    // viewport cannot go within 1 tile of the edge, because of the need to maintain a border.
    // eg., left=1, sprites at x=0 loaded.
    if (left < 1) {
      left = 1;
    }
    // eg., width=10, map=20. Maximum rightmost tile=19, so max left=8
    if (left + this.width > this.map.width - 2) {
      left = this.map.width - this.width - 2;
    }
    if (top < 1) {
      top = 1;
    }
    if (top + this.height > this.map.height - 2) {
      top = this.map.height - this.height - 2;
    }
    this.left = left;
    this.top = top;

    // if viewport is tileWidth tiles wide, we need n+2 sprites.
    // populate sprites from left-1, top-1 to left+1
    let offset=0;
    for (let i=0; i<this.height+2; i++) {
      for (let j=0; j < this.width+2; j++) {

        let x = Math.floor(this.left + j);
        let y = Math.floor(this.top + i);
        let tile = this.map.getTile(x, y);
        if (!tile) {
          console.log("Error - could not get tile at " + x + ", " + y);
          return;
        }
        let newSprite = this.sprites[offset]; new PIXI.Sprite(PIXI.loader.resources["images/" + tile.texture + ".png"].texture);
        newSprite.x = (x-this.left)*this.spriteWidth;
        newSprite.y = (y-this.top)*this.spriteHeight;
        newSprite.texture = PIXI.loader.resources["images/" + tile.texture + ".png"].texture;
        offset += 1;
      }
    }
  }
}

export {Viewport, TileGrid};
