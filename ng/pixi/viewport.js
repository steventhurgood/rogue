"use strict";
exports.__esModule = true;
var PIXI = require("pixi.js");
/*
 * Viewport converts map coordinates into screen coordinates.
 *
 * For exmaple, if the viewport is 320x224, shows 10x7 tiles, and tile sare 32x32
 * A viewport has screen coordinates in pixels, and also offset in tile coordinates
 *
 * eg., the viewport may be at screen position 100, 100 and the tile offset position may be 3, -1
 *
 * As such a tile at 4,0 would have its sprite's x and y position converted to screen coordinates as:
 *
 * find map position: if map top left is (3, -1) then the relative position here would be (4, 1)
 *
 * We then convert this into screen coordinates relative to the viewport:
 *
 * 4 * 32 = 128
 * 1 * 32 = 32
 *
 */
// a tile represents an individual tile on the map
var Tile = /** @class */ (function () {
    function Tile(passable, texture) {
        this.passable = passable;
        this.texture = texture;
    }
    Tile.prototype.setPassable = function (passable) {
        this.passable = passable;
    };
    Tile.prototype.setTexture = function (texture) {
        this.texture = texture;
    };
    return Tile;
}());
// a tilegrid contains a 2 dimensional array of tiles.
var TileGrid = /** @class */ (function () {
    function TileGrid(width, height) {
        this.width = width;
        this.height = height;
        // width and height are measured in tiles
        this.tiles = new Array();
        for (var i = 0; i < width * height; i++) {
            this.tiles.push(new Tile(false, "fill"));
        }
    }
    TileGrid.newRandomGrid = function (width, height) {
        var textures = ["blocked", "floor", "wall"];
        var grid = new TileGrid(width, height);
        for (var i = 0; i < width * height; i++) {
            var textureIndex = Math.floor(Math.random() * 3);
            grid.tiles[i].setTexture(textures[textureIndex]);
        }
        return grid;
    };
    TileGrid.prototype.getTile = function (x, y) {
        if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
            return undefined;
        }
        return this.tiles[y * this.width + x];
    };
    // set a tile position to a value. Return the old tile.
    TileGrid.prototype.setTile = function (x, y, tile) {
        if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
            return undefined;
        }
        var oldTile = this.tiles[y * this.width + x];
        this.tiles[y * this.width + x] = tile;
        return oldTile;
    };
    return TileGrid;
}());
exports.TileGrid = TileGrid;
// a widthxheight array of sprites.
// each sprite has a position x/y, and a width that is controlled by a viewport.
// when the viewport is moved, the sprite grid updates such that the x/y values for individual tiles are correct, and that
// the tiles' texture values are correct
// inefficient naive method; every time the viewport is moved, reload every sprite in the array.
var SpriteGrid = /** @class */ (function () {
    function SpriteGrid(width, height) {
        this.width = width;
        this.height = height;
        // assume that the sprites have already been loaded into the texture cache.
        this.sprites = new Array();
    }
    return SpriteGrid;
}());
var Viewport = /** @class */ (function () {
    function Viewport(left, // left position in tile coordinates
    top, // top position in tile coordinates
    width, // width in tiles,
    height, // height in tiles,
    windowWidth, // with of window in pixels,
    windowHeight, // height of window in pixels,
    map) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this.map = map;
        this.spriteWidth = windowWidth / width;
        this.spriteHeight = windowHeight / height;
        this.spriteGridWidth = width + 2;
        this.spriteGridHeight = height + 2;
        // viewport cannot go within 1 tile of the edge, because of the need to maintain a border.
        // eg., left=1, sprites at x=0 loaded.
        if (this.left < 1) {
            this.left = 1;
        }
        // eg., width=10, map=20. Maximum rightmost tile=19, so max left=8
        if (this.left + this.width > map.width - 2) {
            this.left = map.width - this.width - 2;
        }
        if (this.top < 1) {
            this.top = 1;
        }
        if (this.top + this.height > map.height - 2) {
            this.top = map.height - this.height - 2;
        }
    }
    Viewport.prototype.register = function (container) {
        // if viewport is tileWidth tiles wide, we need n+2 sprites.
        // populate sprites from left-1, top-1 to left+1
        //
        // if left=9.5 then the tile at the leftmost edge should be 9
        this.sprites = new Array();
        for (var i = 0; i < this.height + 2; i++) {
            for (var j = 0; j < this.width + 2; j++) {
                var x = Math.floor(this.left + j);
                var y = Math.floor(this.top + i);
                var tile = this.map.getTile(x, y);
                if (!tile) {
                    console.log("Error - could not get tile at " + x + ", " + y);
                    return;
                }
                var newSprite = new PIXI.Sprite(PIXI.loader.resources["images/" + tile.texture + ".png"].texture);
                newSprite.width = this.spriteWidth;
                newSprite.height = this.spriteHeight;
                newSprite.x = (x - this.left) * this.spriteWidth;
                newSprite.y = (y - this.top) * this.spriteHeight;
                this.sprites.push(newSprite);
                var cliprect = new PIXI.Graphics().drawRect(container.x, container.y, this.windowWidth, this.windowHeight);
                container.mask = cliprect;
                container.addChild(newSprite);
            }
        }
    };
    Viewport.prototype.move = function (left, top) {
        if (left === this.left && top === this.top) {
            return;
        }
        // viewport cannot go within 1 tile of the edge, because of the need to maintain a border.
        // eg., left=1, sprites at x=0 loaded.
        if (left < 1) {
            left = 1;
        }
        // eg., width=10, map=20. Maximum rightmost tile=19, so max left=8
        if (left + this.width > this.map.width - 2) {
            left = this.map.width - this.width - 2;
        }
        if (top < 1) {
            top = 1;
        }
        if (top + this.height > this.map.height - 2) {
            top = this.map.height - this.height - 2;
        }
        this.left = left;
        this.top = top;
        // if viewport is tileWidth tiles wide, we need n+2 sprites.
        // populate sprites from left-1, top-1 to left+1
        var offset = 0;
        for (var i = 0; i < this.height + 2; i++) {
            for (var j = 0; j < this.width + 2; j++) {
                var x = Math.floor(this.left + j);
                var y = Math.floor(this.top + i);
                var tile = this.map.getTile(x, y);
                if (!tile) {
                    console.log("Error - could not get tile at " + x + ", " + y);
                    return;
                }
                var newSprite = this.sprites[offset];
                new PIXI.Sprite(PIXI.loader.resources["images/" + tile.texture + ".png"].texture);
                newSprite.x = (x - this.left) * this.spriteWidth;
                newSprite.y = (y - this.top) * this.spriteHeight;
                newSprite.texture = PIXI.loader.resources["images/" + tile.texture + ".png"].texture;
                offset += 1;
            }
        }
    };
    return Viewport;
}());
exports.Viewport = Viewport;
