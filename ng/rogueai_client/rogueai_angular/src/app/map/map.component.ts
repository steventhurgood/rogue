import { Component, OnInit } from '@angular/core';
import {MapService} from '../map.service';
import {Tile} from '../tile'

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']

})
export class MapComponent implements OnInit {
  hero: string;
  tiles: Array<Array<Tile>>;
  constructor(private mapService: MapService ) { }

  public d(tiles) {
    return true;
  }

  private mapToTiles(response) : Array<Array<Tile>> {
    let layout = response.layout;
    let width = layout.width;
    let height = layout.height;

		let rows : Array<Array<Tile>> = new Array();
		let cols : Array<Tile> = new Array();

    for (let i = 0; i < layout.tiles.length; i++) {
      if (i % width === 0 && i > 0) {
        rows.push(cols);
        cols = new Array();
      }
      let t = layout.tiles[i];
      let tile = new Tile(t.char, t.blocked);
      cols.push(tile);
    }
    return  rows;
  }

  getMap(): void {
    console.log("Getting map");
    this.mapService.getMap().subscribe(
      (resp) => this.tiles = this.mapToTiles(resp));
    }

  ngOnInit() {
    this.getMap();
    this.hero = "Foo";
  }

}
