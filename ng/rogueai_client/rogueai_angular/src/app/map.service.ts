import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Tile } from './tile'

@Injectable({
  providedIn: 'root'
})
export class MapService {
  static readonly url : string = "http://10.0.0.18:8080/v1/dungeon/get";
  private map: Array<Array<Tile>>;
  public getMap() {
    let req = {spec: {
      "num_rooms": 10,
      "wall_cost": 2.0,
      "turn_cost": 5.0,
      "map_size": 30,
      "room_width": 10,
      "padding": 5,
      "force_scale": 10.0,
      "damping": 0.9,
      "max_iterations": 10}};

      return this.http.post(MapService.url, req, {});
  }
  constructor(private http: HttpClient) { }
}
