import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PixiviewComponent } from './pixiview.component';

describe('PixiviewComponent', () => {
  let component: PixiviewComponent;
  let fixture: ComponentFixture<PixiviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PixiviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PixiviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
