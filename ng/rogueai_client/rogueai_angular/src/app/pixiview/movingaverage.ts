
export class MovingAverage {
  private points : Array<number>;
  private total : number;
  private nextI : number;

  constructor(private n : number) {
    this.points = new Array();
    this.total = 0;
    this.nextI=0;
    for (let i =0; i < n; i++) {
      this.points.push(0.0);
    }
  }

  next(value : number) {
    this.total -= this.points[this.nextI];
    this.total += value;
    this.points[this.nextI] = value;
    this.nextI = (this.nextI + 1) % this.n;
  }

  average() {
    return this.total / this.n;
  }
}

