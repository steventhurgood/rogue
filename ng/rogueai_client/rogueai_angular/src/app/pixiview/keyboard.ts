export class Keyboard {
  private press : () => void;
  private release : () => void; private downHandler : (any) => void;
  private upHandler : (any) => void;
  private isUp : boolean;
  private isDown : boolean;

  constructor(private code: number) {
    this.isDown = false;
    this.isUp = true;
  };

  onPress(handler: () => void) : Keyboard {
    this.press = handler;
    return this;
  }
  onRelease(handler: () => void) : Keyboard {
    this.release = handler;
    return this;
  }

  register() : void {
    this.downHandler = (event) => {
      if (event.keyCode == this.code) {
        if (this.isUp && this.press) {
          this.press();
        }
        this.isDown = true;
        this.isUp = false;
      event.preventDefault();
      }
    };
    this.upHandler = (event) => {
      if (event.keyCode == this.code) {
        if (this.isDown && this.release) {
          this.release();
        }
        this.isDown = false;
        this.isUp = true;
      event.preventDefault();
      }
    };
    window.addEventListener("keydown", (event) => this.downHandler(event));
    window.addEventListener("keyup", (event) => this.upHandler(event));
  }
}

