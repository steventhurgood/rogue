import { Component, OnInit } from '@angular/core';
import {MapService} from '../map.service';
import { MovingAverage } from "./movingaverage";
import * as PIXI from "pixi.js";
import { Viewport, TileGrid, Tile } from "./viewport";
import { Keyboard } from "./keyboard";

@Component({
  selector: 'app-pixiview',
  templateUrl: './pixiview.component.html',
  styleUrls: ['./pixiview.component.css']
})
export class PixiviewComponent implements OnInit {

  fps : MovingAverage;
  tiles: TileGrid;
  viewport : Viewport;
  app: PIXI.Application;
  static charToTexture = {
    " " : "blocked",
    "+" : "wall",
    "|" : "wall",
    "-" : "wall",
  }

  constructor(private mapService: MapService) {
    this.fps = new MovingAverage(30);
  }

  private setDX(dx : number) : PixiviewComponent {
    if (this.viewport) {
      this.viewport.dx = dx;
    }
    return this;
  }
  private setDY(dy : number) : PixiviewComponent {
    if (this.viewport) {
      this.viewport.dy = dy;
    }
    return this;
  }


  private mapToTileGrid(response) : TileGrid {
    let layout = response.layout;
    let width = layout.width;
    let height = layout.height;

    let grid : TileGrid = new TileGrid(width, height);

    for (let i = 0; i < layout.tiles.length; i++) {
      let x = i % width;
      let y = (i-x)/width;
      let t = layout.tiles[i];
      let texture = "";
      if (t.blocked) {
        texture = PixiviewComponent.charToTexture[t.char];
        if (!texture) {
          console.log("Unknown texture: ", t);
        }
      } else {
        texture = "floor";
      }
      grid.setTile(x, y, new Tile(!t.blocked, texture, x, y));
    }
    return grid;
  }

  getMap(): void {
    console.log("Getting map");
    this.mapService.getMap().subscribe(
      (resp) => {
        this.tiles = this.mapToTileGrid(resp)
        this.viewport = new Viewport(2, 2, Math.min(this.tiles.width, 2*10), Math.min(this.tiles.height, 2*7), 32*10*3, 32*7*3, this.tiles);
        PIXI.Loader.shared.add([
          "images/skull.png",
          "images/wall.png",
          "images/blocked.png",
          "images/floor.png"]).load(
            (loader, resources) => {
              this.viewport.resources = resources;
            let container = new PIXI.Container();
            container.x = 0;
            container.width =32 * 10*2;
            container.y = 0;
            container.height * 32*7*2;
            // let viewport = new Viewport(10, 10, 10, 7, 32*10, 32*7, dungeonMap, container);
            this.viewport.register(container);
            this.app.stage.addChild(container);
              let deltaText = new PIXI.Text('pixi', {fill: 0xFFFFFF});
              deltaText.x = (32*10*3-100);
              deltaText.y = (32*7*3-30);
              this.app.stage.addChild(deltaText);
            // let box = new Box(border, width-border, border, height-border);
            // let viewbox = new Box(100, 100+32*10*2 , 100, 100 + 32*7*2);
            // box.register(app.stage);
              // viewbox.register(app.stage);
              this.app.ticker.add((delta) => {
                this.fps.next(this.app.ticker.FPS);
                this.viewport.update(delta);
               deltaText.text = "" + this.fps.average();
            });
            });
          })
      }

  ngOnInit() {
    this.getMap();
    console.log(this.tiles);
    console.log("WebGL:",PIXI.utils.isWebGLSupported());

    this.app = new PIXI.Application({width: 32*10*3, height: 32*7*3, antialias: true, transparent: false, resolution: 1});
    document.getElementById("pixiwindow").appendChild(this.app.view);

    //this.app.ticker.add((delta) => {
    //  console.log(this);
    // if (this.viewport) {
    //   this.viewport.update(delta);
    // }
    //});
  let left = new Keyboard(37)
    .onPress(() => { this.setDX(-0.05)})
    .onRelease(() => { this.setDX(0)})
    .register();

  let right = new Keyboard(39)
    .onPress(() => { this.setDX(0.05)})
    .onRelease(() => { this.setDX(0)})
    .register();
  let up = new Keyboard(38)
    .onPress(() => { this.setDY(-0.05)})
    .onRelease(() => { this.setDY(0)})
    .register();
  let down = new Keyboard(40)
    .onPress(() => { this.setDY(0.05)})
    .onRelease(() => { this.setDY(0)})
    .register();
  }
}
