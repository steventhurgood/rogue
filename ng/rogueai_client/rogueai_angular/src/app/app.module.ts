import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
// import { MapComponent } from './map/map.component';
import { PixiviewComponent } from './pixiview/pixiview.component';

@NgModule({
  declarations: [
    AppComponent,
    PixiviewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
