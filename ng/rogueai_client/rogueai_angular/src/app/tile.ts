export class Tile {
  public char: string;
  public blocked: boolean;

  public getClass() : string {
    if (this.blocked) {
      if (this.char === "|" || this.char === "-" || this.char === "+") {
          return "wall";
      }
      return "blocked";
    }
    if (this.char === "#") {
      return "tunnel";
    }
    return "unblocked";
  }

  constructor(char: string, blocked: boolean) {
    this.char = char;
    this.blocked = blocked;
  }
}
