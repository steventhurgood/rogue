import {grpc} from "grpc-web-client"
import {DungeonRequest, DungeonSpec, DungeonResponse} from "./generated/dungeon_pb"
import {Dungeon} from "./generated/dungeon_pb_service"

const hostname = "http://10.0.0.18:8081";

function render(response) {

let layout = response.layout;
console.log("Rendering tiles", layout);
let output = document.getElementById("output"); 
let width = layout.width;
let height = layout.height;
let num_tiles = width * height;
let out = ["<table>"];
	for (let i = 0; i < layout.tilesList.length; i++) {
	let ch = layout.tilesList[i].pb_char;

	let tileClass = "blocked";
	if (!layout.tilesList[i].blocked) {
	  tileClass = "unblocked";
	  if (ch === "#") {
	    tileClass = "tunnel";
	  }
	}
	if (ch === "|" || ch === "+" || ch === "-") {
	  tileClass = "wall";
	}
	if (ch === " ") {
	  ch = "&nbsp;";
	}
	if (i % width === 0) {
	    out.push("<tr>");
		    }
		    out.push("<td class=\"" + tileClass + "\">", ch, "</td>");
	if (i+1 % width == 0) {
	out.push("</tr>");
	}
	}
	out.push("</table>");
output.innerHTML = out.join('');
}

function foo(name: string) {
console.log("working, " + name);
let req = new DungeonRequest();
let spec = new DungeonSpec();
spec.setNumRooms(10);
spec.setWallCost(3.0);
spec.setTurnCost(2.0);
spec.setMapSize(50);
spec.setRoomWidth(10);
spec.setPadding(5);
spec.setForceScale(100.0);
spec.setDamping(0.9);
spec.setMaxIterations(10);
req.setSpec(spec);

grpc.unary(Dungeon.Get, {
  request: req,
  host: hostname,
  onEnd: res => {
    const {status, statusMessage, headers, message, trailers} = res;
    if (status === grpc.Code.OK && message) {
    console.log("All okay: ");
    render(message.toObject());
      } else {
      console.log("Error: ",statusMessage);
      }
  }
});
}
foo("Bob");
