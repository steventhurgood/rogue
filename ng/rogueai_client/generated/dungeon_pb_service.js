// package: rogueai
// file: dungeon.proto

var dungeon_pb = require("./dungeon_pb");
var grpc = require("grpc-web-client").grpc;

var Dungeon = (function () {
  function Dungeon() {}
  Dungeon.serviceName = "rogueai.Dungeon";
  return Dungeon;
}());

Dungeon.Get = {
  methodName: "Get",
  service: Dungeon,
  requestStream: false,
  responseStream: false,
  requestType: dungeon_pb.DungeonRequest,
  responseType: dungeon_pb.DungeonResponse
};

exports.Dungeon = Dungeon;

function DungeonClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

DungeonClient.prototype.get = function get(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  grpc.unary(Dungeon.Get, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          callback(Object.assign(new Error(response.statusMessage), { code: response.status, metadata: response.trailers }), null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
};

exports.DungeonClient = DungeonClient;

