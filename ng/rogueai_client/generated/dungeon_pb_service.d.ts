// package: rogueai
// file: dungeon.proto

import * as dungeon_pb from "./dungeon_pb";
import {grpc} from "grpc-web-client";

type DungeonGet = {
  readonly methodName: string;
  readonly service: typeof Dungeon;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof dungeon_pb.DungeonRequest;
  readonly responseType: typeof dungeon_pb.DungeonResponse;
};

export class Dungeon {
  static readonly serviceName: string;
  static readonly Get: DungeonGet;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }
export type ServiceClientOptions = { transport: grpc.TransportConstructor; debug?: boolean }

interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: () => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}

export class DungeonClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: ServiceClientOptions);
  get(
    requestMessage: dungeon_pb.DungeonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError, responseMessage: dungeon_pb.DungeonResponse|null) => void
  ): void;
  get(
    requestMessage: dungeon_pb.DungeonRequest,
    callback: (error: ServiceError, responseMessage: dungeon_pb.DungeonResponse|null) => void
  ): void;
}

