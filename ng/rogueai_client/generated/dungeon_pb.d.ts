// package: rogueai
// file: dungeon.proto

import * as jspb from "google-protobuf";

export class DungeonRequest extends jspb.Message {
  hasSpec(): boolean;
  clearSpec(): void;
  getSpec(): DungeonSpec | undefined;
  setSpec(value?: DungeonSpec): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DungeonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DungeonRequest): DungeonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DungeonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DungeonRequest;
  static deserializeBinaryFromReader(message: DungeonRequest, reader: jspb.BinaryReader): DungeonRequest;
}

export namespace DungeonRequest {
  export type AsObject = {
    spec?: DungeonSpec.AsObject,
  }
}

export class DungeonSpec extends jspb.Message {
  getNumRooms(): number;
  setNumRooms(value: number): void;

  getWallCost(): number;
  setWallCost(value: number): void;

  getTurnCost(): number;
  setTurnCost(value: number): void;

  getMapSize(): number;
  setMapSize(value: number): void;

  getRoomWidth(): number;
  setRoomWidth(value: number): void;

  getPadding(): number;
  setPadding(value: number): void;

  getForceScale(): number;
  setForceScale(value: number): void;

  getDamping(): number;
  setDamping(value: number): void;

  getMaxIterations(): number;
  setMaxIterations(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DungeonSpec.AsObject;
  static toObject(includeInstance: boolean, msg: DungeonSpec): DungeonSpec.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DungeonSpec, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DungeonSpec;
  static deserializeBinaryFromReader(message: DungeonSpec, reader: jspb.BinaryReader): DungeonSpec;
}

export namespace DungeonSpec {
  export type AsObject = {
    numRooms: number,
    wallCost: number,
    turnCost: number,
    mapSize: number,
    roomWidth: number,
    padding: number,
    forceScale: number,
    damping: number,
    maxIterations: number,
  }
}

export class Tile extends jspb.Message {
  getBlocked(): boolean;
  setBlocked(value: boolean): void;

  getChar(): string;
  setChar(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Tile.AsObject;
  static toObject(includeInstance: boolean, msg: Tile): Tile.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Tile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Tile;
  static deserializeBinaryFromReader(message: Tile, reader: jspb.BinaryReader): Tile;
}

export namespace Tile {
  export type AsObject = {
    blocked: boolean,
    pb_char: string,
  }
}

export class DungeonLayout extends jspb.Message {
  getWidth(): number;
  setWidth(value: number): void;

  getHeight(): number;
  setHeight(value: number): void;

  clearTilesList(): void;
  getTilesList(): Array<Tile>;
  setTilesList(value: Array<Tile>): void;
  addTiles(value?: Tile, index?: number): Tile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DungeonLayout.AsObject;
  static toObject(includeInstance: boolean, msg: DungeonLayout): DungeonLayout.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DungeonLayout, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DungeonLayout;
  static deserializeBinaryFromReader(message: DungeonLayout, reader: jspb.BinaryReader): DungeonLayout;
}

export namespace DungeonLayout {
  export type AsObject = {
    width: number,
    height: number,
    tilesList: Array<Tile.AsObject>,
  }
}

export class DungeonResponse extends jspb.Message {
  hasLayout(): boolean;
  clearLayout(): void;
  getLayout(): DungeonLayout | undefined;
  setLayout(value?: DungeonLayout): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DungeonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DungeonResponse): DungeonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DungeonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DungeonResponse;
  static deserializeBinaryFromReader(message: DungeonResponse, reader: jspb.BinaryReader): DungeonResponse;
}

export namespace DungeonResponse {
  export type AsObject = {
    layout?: DungeonLayout.AsObject,
  }
}

