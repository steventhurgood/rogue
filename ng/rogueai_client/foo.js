"use strict";
exports.__esModule = true;
var grpc_web_client_1 = require("grpc-web-client");
var dungeon_pb_1 = require("./generated/dungeon_pb");
var dungeon_pb_service_1 = require("./generated/dungeon_pb_service");
var hostname = "http://10.0.0.18:8081";
function render(response) {
    var layout = response.layout;
    console.log("Rendering tiles", layout);
    var output = document.getElementById("output");
    var width = layout.width;
    var height = layout.height;
    var num_tiles = width * height;
    var out = ["<table>"];
    for (var i = 0; i < layout.tilesList.length; i++) {
        var ch = layout.tilesList[i].pb_char;
        var tileClass = "blocked";
        if (!layout.tilesList[i].blocked) {
            tileClass = "unblocked";
            if (ch === "#") {
                tileClass = "tunnel";
            }
        }
        if (ch === "|" || ch === "+" || ch === "-") {
            tileClass = "wall";
        }
        if (ch === " ") {
            ch = "&nbsp;";
        }
        if (i % width === 0) {
            out.push("<tr>");
        }
        out.push("<td class=\"" + tileClass + "\">", ch, "</td>");
        if (i + 1 % width == 0) {
            out.push("</tr>");
        }
    }
    out.push("</table>");
    output.innerHTML = out.join('');
}
function foo(name) {
    console.log("working, " + name);
    var req = new dungeon_pb_1.DungeonRequest();
    var spec = new dungeon_pb_1.DungeonSpec();
    spec.setNumRooms(10);
    spec.setWallCost(3.0);
    spec.setTurnCost(2.0);
    spec.setMapSize(50);
    spec.setRoomWidth(10);
    spec.setPadding(5);
    spec.setForceScale(100.0);
    spec.setDamping(0.9);
    spec.setMaxIterations(10);
    req.setSpec(spec);
    grpc_web_client_1.grpc.unary(dungeon_pb_service_1.Dungeon.Get, {
        request: req,
        host: hostname,
        onEnd: function (res) {
            var status = res.status, statusMessage = res.statusMessage, headers = res.headers, message = res.message, trailers = res.trailers;
            if (status === grpc_web_client_1.grpc.Code.OK && message) {
                console.log("All okay: ");
                render(message.toObject());
            }
            else {
                console.log("Error: ", statusMessage);
            }
        }
    });
}
foo("Bob");
