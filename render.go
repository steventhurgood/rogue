package rogue

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math/rand"
	"os"
)

type TileType int

const (
	RockTile TileType = iota
	RoomTile
	TunnelTile
	WallTile
)

type Tile struct {
	Zone *Zone
	Room Room
	Type TileType

	Attributes TileAttributes
}

type TileAttributes struct {
	Blocked  bool
	Hardness float64
}

func Render(l *Level, rnd *rand.Rand) ([][]Tile, error) {
	width, height := l.Width(), l.Height()
	// x, y := l.X(), l.Y()
	m := make([][]Tile, width)
	for x := 0; x < width; x++ {
		m[x] = make([]Tile, height)
		for y := 0; y < height; y++ {
			m[x][y] = Tile{
				Type: RockTile,
				Attributes: TileAttributes{
					Blocked:  true,
					Hardness: 1.0,
				},
			}
		}
	}
	for _, z := range l.Zones {
		for _, r := range z.Rooms {
			err := r.Render(m, z, rnd)
			if err != nil {
				return nil, err
			}
		}
	}
	return m, nil
}

func AsciiTiles(m [][]Tile) string {
	var b bytes.Buffer
	for x := range m {
		for y := range m[x] {
			rm := m[x][y]
			chr := " "
			if rm.Attributes.Blocked {
				chr = "#"
			}
			fmt.Fprint(&b, chr)
		}
		fmt.Fprint(&b, "\n")
	}
	return b.String()
}

func PngTiles(m [][]Tile, filename string) error {
	out, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("Error writing png: %v", err)
	}
	i := image.NewRGBA(image.Rect(0, 0, len(m), len(m[0])))
	for x := range m {
		for y := range m[x] {
			rm := m[x][y]
			colour := color.Gray16{0xaaff}
			if rm.Attributes.Blocked {
				colour = color.Black
			}
			i.Set(x, y, colour)
		}
	}
	png.Encode(out, i)
	return nil
}
