package mesh

import (
	"fmt"
	"image/color"

	"github.com/golang/glog"
	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg"
)

// AsPoints provides the methods necessary for rendering vertices as points on a graph
type AsPoints []Vertex

func (a AsPoints) Len() int {
	return len(a)
}

func (a AsPoints) Label(i int) string {
	return fmt.Sprint(i)
}

func (a AsPoints) XYZ(i int) (float64, float64, float64) {
	return a[i].X, a[i].Y, 0.0
}

func (a AsPoints) XY(i int) (float64, float64) {
	return a[i].X, a[i].Y
}

// Methods for plotting a face as 3 lines, 4 points.
func (f *Face) Len() int {
	return 4 // because this is a closed loop: 0 -> 1 -> 2 -> 0
}

func (f *Face) XY(i int) (float64, float64) {
	e := f.E
	for j := 0; j < i; j++ {
		e = e.Next
	}

	v := e.m.P[e.To]
	return v.X, v.Y
}

// TopEdgeLine turns the advancing front slice of HalfEdge structs into a format for plotting as a line
type TopEdgeLine []*HalfEdge

// BottomEdgeLine turns the convex hull slice of HalfEdge structs into a format for plotting as a line
type BottomEdgeLine []*HalfEdge

func (e TopEdgeLine) Len() int {
	if len(e) == 0 {
		return 0
	}
	return len(e) + 1
}

func (e BottomEdgeLine) Len() int {
	if len(e) == 0 {
		return 0
	}
	return len(e) + 1
}

func (e TopEdgeLine) XY(i int) (float64, float64) {
	var v int
	if i == len(e) {
		v = e[len(e)-1].Next.Next.To
	} else {
		v = e[i].To
	}
	return e[0].m.P[v].X, e[0].m.P[v].Y
}

func (e BottomEdgeLine) XY(i int) (float64, float64) {
	var v int
	if i == 0 {
		v = e[i].Next.Next.To
	} else {
		v = e[i-1].To
	}
	return e[0].m.P[v].X, e[0].m.P[v].Y
}

// DebugSave saves images with incrementing numbers of the state of the mesh
func (m *Mesh) DebugSave(key string) {
	glog.V(2).Infof("Saving: %v %v", m.debugCounter, key)
	filename := fmt.Sprintf("debug-%v-%v.png", m.debugCounter, key)
	m.debugCounter++
	m.Save(filename)
}

// Save saves a png image of the state of the mesh
func (m *Mesh) Save(filename string) error {
	p, err := plot.New()
	if err != nil {
		return err
	}
	PlotMesh(m, p)
	p.Save(vg.Length(10)*vg.Centimeter, vg.Length(10)*vg.Centimeter, filename)
	return nil
}

func PlotMesh(m *Mesh, p *plot.Plot) error {
	dots, err := plotter.NewBubbles(AsPoints(m.P), vg.Length(0.1)*vg.Centimeter, vg.Length(0.1)*vg.Centimeter)
	if err != nil {
		return err
	}
	p.Add(dots)

	labels, err := plotter.NewLabels(AsPoints(m.P))
	if err != nil {
		return err
	}
	p.Add(labels)

	afLine, err := plotter.NewLine(TopEdgeLine(m.advancingFront))
	if err != nil {
		return err
	}
	afLine.Color = color.RGBA{255, 128, 128, 255}
	afLine.Width = vg.Length(0.1) * vg.Centimeter

	p.Add(afLine)
	chLine, err := plotter.NewLine(BottomEdgeLine(m.convexHull))
	glog.V(2).Infof("Convex Hull Line: %v", chLine)
	if err != nil {
		return err
	}
	chLine.Color = color.RGBA{128, 255, 128, 255}
	chLine.Width = vg.Length(0.1) * vg.Centimeter
	p.Add(chLine)

	for _, f := range m.F {
		line, err := plotter.NewLine(f)
		if err != nil {
			return err
		}
		p.Add(line)
	}
	return nil
}
