package mesh

import (
	"bytes"
	"fmt"
	"math"
	"sort"

	"github.com/golang/glog"
)

// package mesh contains functions for turning a set of points into various graphs
// and meshes

// Vertex is a single point in a mesh
type Vertex struct {
	X, Y  float64
	E     *HalfEdge // an edge leading outwards from this point
	Index int       // the original index of this vertex, which will be lost on sorting
}

// ByY allows sorting a graph's vertices by their Y component
type ByY []Vertex

func (v ByY) Len() int {
	return len(v)
}

func (v ByY) Less(a, b int) bool {
	if v[a].Y == v[b].Y {
		return v[a].X < v[b].X
	}
	return v[a].Y < v[b].Y
}

func (v ByY) Swap(a, b int) {
	v[a], v[b] = v[b], v[a]
}

func (m *Mesh) SortByY() {
	sort.Sort(ByY(m.P))
}

// Face is a set of three Vertexes joined by HalfEdges
type Face struct {
	E *HalfEdge // one of the half-edges of this face.
	m *Mesh
}

// Mesh is a set of triangles that form a continuous convex hull around a set of points.
type Mesh struct {
	P            []Vertex
	F            []*Face
	debugCounter int // used to increment filenames every time a debugging image is saved

	advancingFront      []*HalfEdge
	convexHull          []*HalfEdge
	leftMost, rightMost int
}

// TestCase returns a string suitable for using in a regression test for triangulating this particular set of points
func TestCase(m *Mesh) string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "[]Vertex{")
	for i, p := range m.P {
		if i > 0 {
			fmt.Fprint(&b, ", ")
		}
		fmt.Fprintf(&b, "{X: %v, Y: %v}", p.X, p.Y)
	}
	fmt.Fprintf(&b, "}")
	return b.String()
}

type HalfEdge struct {
	To   int       // the index of vertex at the end of this half-edge
	Pair *HalfEdge // the opposite half-edge
	Next *HalfEdge // the next half-edge around the triangle, counter-clockwise
	F    *Face
	m    *Mesh
}

// Triangulate - take a graph containing points and no faces, and turn it into a convex hull mesh
func (m *Mesh) Triangulate() error {
	// Initialise the first triangle
	skipped, err := m.initializeTriangulation()
	if err != nil {
		return err
	}
	m.DebugSave("Initialized")
	p := m.P
	// done initialising; start iterating over the remaining points
	for v := 3 + skipped; v < len(m.P); v++ {
		glog.V(3).Infof("Vertex: %v", v)
		pv := p[v]
		switch {
		case pv.X < p[m.leftMost].X:
			{
				glog.V(2).Infof("Vertex %v left of leftMost: %v", v, m.leftMost)
				m.leftMost = v
				e := m.FillConcaveTopLeft(v, 0)
				ePair := m.FillConcaveBottomLeft(v, 0)
				// 3 cases:
				// e is defined and not ePair, so the e.F makes up the new convex hull
				// ePair is defined and not e, so ePair.F makes up the advancing front
				// e and ePair are defined, so the advancing front and convex hull have already been created
				switch {
				case e != nil && ePair != nil:
					{
						glog.V(2).Infof("Expanded both convex hull and advancing front")
						glog.V(2).Infof("convex hull: %v", m.convexHull)
						glog.V(2).Infof("advancing front: %v", m.advancingFront)
						m.Pair(ePair, e)
						m.InvalidFlip(ePair)

					}
				case e != nil && ePair == nil:
					{
						glog.V(2).Infof("Lower left convex hull expanding: %v (%v)", e, m.convexHull)
						m.convexHull = append([]*HalfEdge{e}, m.convexHull...)
						glog.V(2).Infof("Convex hull: %v", m.convexHull)
					}
				case ePair != nil && e == nil:
					{
						glog.V(2).Infof("left advancing front expanding: %v (%v)", e, m.advancingFront)
						m.advancingFront = append([]*HalfEdge{ePair}, m.advancingFront...)
					}
				default:
					{
						glog.V(2).Infof("Error: leftmost vertex resulted in no new upper or lower vertices")
					}
				}
				glog.V(2).Infof("new top concave %v", e)
				glog.V(2).Infof("new bottom concave %v", ePair)
			}
		case pv.X > p[m.rightMost].X:
			{
				glog.V(2).Infof("Vertex %v right of rightMost: %v", v, m.rightMost)
				m.rightMost = v
				e := m.FillConcaveTopRight(v, len(m.advancingFront)-1)
				ePair := m.FillConcaveBottomRight(v, len(m.convexHull)-1)
				glog.V(2).Infof("new top concave %v", e)
				glog.V(2).Infof("new bottom concave %v", ePair)
				switch {
				case e != nil && ePair != nil:
					{
						glog.V(2).Infof("Expanded both convex hull and advancing front")
						glog.V(2).Infof("convex hull: %v", m.convexHull)
						glog.V(2).Infof("advancing front: %v", m.advancingFront)
						m.Pair(ePair, e)
						m.InvalidFlip(ePair)

					}
				case e != nil && ePair == nil:
					{
						glog.V(2).Infof("Lower right convex hull expanding: %v (%v)", e, m.convexHull)
						m.convexHull = append(m.convexHull, e)
						glog.V(2).Infof("Convex hull: %v", m.convexHull)
					}
				case ePair != nil && e == nil:
					{
						glog.V(2).Infof("Lower right advancing front expanding: %v (%v)", ePair, m.advancingFront)
						m.advancingFront = append(m.advancingFront, ePair)
						glog.V(2).Infof("advancing front: %v", m.advancingFront)
					}
				default:
					{
						glog.V(2).Infof("Error: rightmost vertex resulted in no new upper or lower vertices")
					}
				}
			}
		case pv.X >= p[m.leftMost].X && pv.X <= p[m.rightMost].X:
			{
				i, err := m.findEdge(&pv)
				if err != nil {
					return err
				}
				e := m.advancingFront[i]
				glog.V(2).Infof("Vertex %v[%v] found on edge %v", pv, v, e)
				l := m.advancingFront[i].To
				r := m.advancingFront[i].Next.Next.To
				f := m.NewFace(l, r, v)

				// stitch new face to existing triangle
				m.Pair(f.E, m.advancingFront[i])
				// flip deferred to end of the function

				// remove old edge from advancing front
				newFront := make([]*HalfEdge, len(m.advancingFront)+1)
				// leftFront := g.advancingFront[:i]
				copy(newFront, m.advancingFront[:i])
				glog.V(2).Infof("newFront a: %v", newFront)
				newFront[i] = f.E.Next.Next
				glog.V(2).Infof("newFront b: %v", newFront)
				newFront[i+1] = f.E.Next
				glog.V(2).Infof("newFront c: %v", newFront)
				copy(newFront[i+2:], m.advancingFront[i+1:])
				glog.V(2).Infof("newFront d: %v", newFront)

				// rightFront := g.advancingFront[i+1:]
				// g.advancingFront = append(leftFront, f.E.Next.Next)
				// g.advancingFront = append(g.advancingFront, f.E.Next)
				m.advancingFront = newFront // append(g.advancingFront, rightFront...)

				glog.V(2).Infof("New vertex %v %v on edge %v", v, pv, i)
				// m.DebugSave("mid-face")
				eLeft := m.FillConcaveRightwards(v, i+1)
				if eLeft != nil {
					// debugLog("joining concave rightwards: %v", eLeft)
					// g.Pair(f.E.Next, eLeft)
					m.InvalidFlip(eLeft)
				} else {
					glog.V(2).Infof("filling concave rightwards did not give an edge to join")
				}
				eRight := m.FillConcaveLeftwards(v, i)
				if eRight != nil {
					// debugLog("joining concave leftwards: %v", eRight)
					// g.Pair(f.E.Next.Next, eRight)
					m.InvalidFlip(eRight)
				} else {
					glog.V(2).Infof("filling concave leftwards did not give an edge to join")
				}
				m.InvalidFlip(e)
			}
		default:
			{
				glog.V(2).Infof("No idea where %v is", v)
			}

		}
		m.DebugSave("iterating")
		// for i := range m.P {
		//	fmt.Printf("iteration vertex %v vertex %v: %+v\n", v, i, m.P[i])
		//}
	}
	return nil
}

// Flip - flip the quadrilateral with e
// return one of the newly created edges
func (e *HalfEdge) Flip() *HalfEdge {
	//     l
	// 2-------- 3
	// |\        |
	// |  \  e'  |
	//i|  e \    |k
	// |      \  |
	// 0 ------- 1
	//      j
	//
	//     l
	// 2-------- 3
	// |       / |
	// |   b /   |
	//i|   / a   |k
	// | /       |
	// 0 ------- 1
	//      j
	//
	// triangle A = 0 - 1 - 2
	// e = 1-2
	// triangle B = 1 - 3 - 2
	// ePair = 2-1
	// i = 2-0
	// j = 0-1
	// k = 1-3
	// l = 3-2
	// fmt.Printf("e: %+v\n", e)
	m := e.m
	ePair := e.Pair
	if ePair == nil {
		glog.V(2).Infof("Attempting to flip edge with no pair: %v", e)
		return nil
	}
	i := e.Next
	j := e.Next.Next
	k := ePair.Next
	l := ePair.Next.Next

	// fmt.Printf("Pre-flip:\n")
	// fmt.Printf("Triangle A edges:\n  e: %v\n  i: %v\n  j: %v\n\n", e, i, j)
	// fmt.Printf("Triangle A points:\n  e.To: (%v)%v\n  i.To: (%v)%v\n  j.To: (%v)%v\n\n", e.To, e.m.P[e.To], i.To, e.m.P[i.To], j.To, e.m.P[j.To])

	// fmt.Printf("Triangle B edges:\n  ePair: %v\n  k, %v\n  l: %v\n\n", ePair, k, l)
	// fmt.Printf("Triangle B points:\n  ePair.To: (%v)%v\n  k.To: (%v)%v\n  l.To: (%v)%v\n\n", ePair.To, ePair.m.P[ePair.To], k.To, ePair.m.P[k.To], l.To, ePair.m.P[l.To])

	a := &HalfEdge{
		To: i.To,
		F:  j.F,
		m:  m,
	}
	k.F = j.F // a.F, k.F, j.F all the same
	e = nil
	ePair = nil
	// e.To = i.To
	// e.F = j.F

	b := &HalfEdge{
		To:   k.To,
		F:    l.F,
		Pair: a,
		m:    m,
	}
	a.Pair = b
	i.F = l.F // b.F, i.F, l.F all the same
	// ePair.To = k.To
	// ePair.F = l.F

	j.F.E = j
	l.F.E = l

	a.Next = j
	j.Next = k
	k.Next = a

	b.Next = l
	l.Next = i
	i.Next = b

	m.P[i.To].E = b
	m.P[j.To].E = k
	m.P[k.To].E = a
	m.P[l.To].E = i

	glog.V(2).Infof("Flipping (%v %v %v)(%v %v %v)", a, j, k, b, l, i)
	// fmt.Printf("Post-flip:\n")
	// fmt.Printf("Triangle A edges:\n  a: %v\n  a.Next: %v\n  a.Next.Next: %v\n\n", a, a.Next, a.Next.Next)
	// fmt.Printf("Triangle A points:\n  a.To: (%v)%v\n  a.Next.To: (%v)%v\n  a.Next.Next.To: (%v)%v\n\n", a.To, m.P[a.To], a.Next.To, m.P[a.Next.To], a.Next.Next.To, m.P[a.Next.Next.To])

	// fmt.Printf("Triangle B edges:\n  b: %v\n  b.Next: %v\n  b.Next.Next: %v\n", b, b.Next, b.Next.Next)
	// fmt.Printf("Triangle B points:\n  (%v)%v\n  (%v)%v\n  (%v)%v\n\n", b.To, m.P[b.To], b.Next.To, m.P[b.Next.To], b.Next.Next.To, m.P[b.Next.Next.To])

	return a
}

// Create a new face with vertices a, b, c counterclockwise
// Return the new face, whose edge is ab
func (m *Mesh) NewFace(a, b, c int) *Face {
	glog.V(2).Infof("Creating new face: %v - %v - %v", a, b, c)
	ab := &HalfEdge{
		To: b,
		m:  m,
	}
	if m.P[a].E == nil {
		m.P[a].E = ab
	}
	bc := &HalfEdge{
		To: c,
		m:  m,
	}
	if m.P[b].E == nil {
		m.P[b].E = bc
	}
	ca := &HalfEdge{
		To: a,
		m:  m,
	}
	if m.P[c].E == nil {
		m.P[c].E = ca
	}

	f := &Face{
		E: ab,
		m: m,
	}
	ab.F = f
	bc.F = f
	ca.F = f
	ab.Next = bc
	bc.Next = ca
	ca.Next = ab
	m.F = append(m.F, f)
	return f
}

// angle gives the angle between vertices a, b, c which are defined in l-r order along the top of the hull
// if bottom, treates the angle as underneath the edges
func angle(m *Mesh, a, b, c int, bottom bool) float64 {
	ba := Vertex{
		X: m.P[a].X - m.P[b].X,
		Y: m.P[a].Y - m.P[b].Y,
	}
	bc := Vertex{
		X: m.P[c].X - m.P[b].X,
		Y: m.P[c].Y - m.P[b].Y,
	}
	baRad := math.Atan2(ba.Y, ba.X)

	if bottom {
		baRad = -baRad
	}
	// baRad is in the left half of the quadrant, hence if negative, add 2*math.Pi
	if baRad < 0 {
		baRad += 2 * math.Pi
	}
	bcRad := math.Atan2(bc.Y, bc.X)
	if bottom {
		bcRad = -bcRad
	}
	diff := (baRad - bcRad)
	return diff
}

func (m *Mesh) topAngle(a, b, c int) float64 {
	return angle(m, a, b, c, false)
}

// bottomAngle gives the angle between vertices a, b, c which are defined in l-r order along the bottom of the hull
func (m *Mesh) bottomAngle(a, b, c int) float64 {
	return angle(m, a, b, c, true)
}

// Pair takes two half-edges and joins them together
func (m *Mesh) Pair(a, b *HalfEdge) {
	if a == nil || b == nil {
		glog.Warningf("Attempting to pair non-existant edge: %v and %v", a, b)
		return
	}
	glog.V(2).Infof("Pairing edges %v and %v", a, b)
	a.Pair = b
	b.Pair = a
	// g.InvalidFlip(a)
}

// findEdge -find the edge in e which cover's v's .X
// on the advancing front, counterclockwise edges go right to left,
// so edge.To is on the left-hand side.
// findEdge assumes that v.X is contained within the advancing front.
func (m *Mesh) findEdge(v *Vertex) (int, error) {
	e := m.advancingFront
	for i := 0; i < len(e); i++ {
		l := e[i].To
		r := e[i].Next.Next.To
		glog.V(2).Infof("looking for x=%v on edge %v(%v[x=%v], %v[x=%v])", v.X, i, l, m.P[l].X, r, m.P[r].X)
		if m.P[l].X <= v.X && v.X <= m.P[r].X {
			return i, nil
		}
	}
	m.DebugSave("error-finding-vertex-on-advancing-front")
	return -1, fmt.Errorf("Error finding vertex %v on advancing front:\n%v", v, m.advancingFront)
}

func (m *Mesh) InvalidFlip(e *HalfEdge) {
	glog.V(2).Infof("Testing validity: %v", e)
	if !e.Valid() {
		glog.V(2).Infof("Testing validity: %v", e)
		a := e.Flip()
		// m.DebugSave("invalidflip")
		adjacent := []*HalfEdge{
			a.Next, a.Next.Next,
			a.Pair.Next, a.Pair.Next.Next,
		}
		for _, b := range adjacent {
			m.InvalidFlip(b)
		}
	}
}
