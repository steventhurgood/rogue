package mesh

import (
	"bytes"
	"fmt"
)

// methods for returning string representations of graph elements.
func (m *Mesh) String() string {
	var b bytes.Buffer
	for i, v := range m.P {
		fmt.Fprintf(&b, "%v: (%v, %v)\n", i, v.X, v.Y)
	}
	fmt.Fprint(&b, "\n")
	for _, f := range m.F {
		fmt.Fprintf(&b, "Face: ")
		e := f.E
		for i := 0; i < 3; i++ {
			fmt.Fprintf(&b, "-> (%v, %v)[%v]", m.P[e.To].X, m.P[e.To].Y, e.To)
			if i < 2 {
				fmt.Fprint(&b, ", ")
			}
			e = e.Next
		}
		fmt.Fprint(&b, "\n")
	}
	return b.String()
}

func (e *HalfEdge) String() string {
	var b bytes.Buffer
	fmt.Fprint(&b, "(")
	if e.Next != nil && e.Next.Next != nil {
		fmt.Fprintf(&b, "From: %v, ", e.Next.Next.To)
	}
	fmt.Fprintf(&b, "To: %v", e.To)
	fmt.Fprintf(&b, "[%p])", e)
	return b.String()
}

func (f *Face) String() string {
	e := f.E
	var b bytes.Buffer
	fmt.Fprint(&b, "Face: ")
	for i := 0; i < 3; i++ {
		fmt.Fprint(&b, e)
		e = e.Next
		if i < 2 {
			fmt.Fprint(&b, " -> ")
		}
	}
	return b.String()
}
