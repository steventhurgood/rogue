package mesh

import (
	"fmt"
	"math"

	"github.com/golang/glog"
)

const (
	piThreshold = 1e-10 // how close to pi do we consider a straight line
)

// initializeTriangulation handles the initial 3 (or more) vertices before the sweep algorithm starts
// may skip some verticies in addition to the required 3; return how many
func (m *Mesh) initializeTriangulation() (int, error) {
	skipped := 0
	if len(m.P) < 3 {
		return 0, fmt.Errorf("Cannot triangulate a graph with < 3 nodes")
		// TODO: correctly handle n<3 cases
	}
	m.SortByY()

	// var leftMost, rightMost int // the indices of the extreme vertices

	p := m.P

	v := 0 // v is the new vertex we are adding
	l := 1 // l is the left of several vertices
	r := 2 // r is the right of several vertices

	if p[r].X < p[l].X {
		l, r = r, l
	}

	pv := p[v]
	pl := p[l]
	pr := p[r]

	switch {
	case pl.X < pv.X && pv.X < pr.X:
		{
			// the bottom vertex is in the middle
			// new triangle 0 - r - l, with r-l on the advancing front, l-v, v-r on the convex hull
			f := m.NewFace(v, r, l) // f.E is v-r
			m.advancingFront = append(m.advancingFront, f.E.Next)
			m.convexHull = append(m.convexHull, f.E.Next.Next, f.E)
			m.leftMost = l
			m.rightMost = r
		}
	case pr.X <= pv.X:
		{
			// both to the left
			m.leftMost = l
			m.rightMost = v
			a := m.topAngle(l, r, v)
			switch {
			case a < math.Pi:
				{
					// l-v is on the advancing Front
					// l-r, r-v is on the convex hull
					f := m.NewFace(v, l, r) // f.E is v-l
					m.advancingFront = append(m.advancingFront, f.E)
					m.convexHull = append(m.convexHull, f.E.Next, f.E.Next.Next)

				}
			case a > math.Pi:
				{
					// l-r, r-v is on the advancing front
					// l-v is on the convex hull
					f := m.NewFace(v, r, l) // f.E is v-r
					m.advancingFront = append(m.advancingFront, f.E.Next, f.E)
					m.convexHull = append(m.convexHull, f.E.Next.Next)
				}
			default:
				{
					// both l & r are to the left, in a straight line l -> r -> v
					var err error
					skipped, err = m.fillColinearLeft(v, l, r)
					if err != nil {
						return 0, err
					}
				}
			}
		}
	case pl.X >= pv.X:
		{
			// both to the right
			m.leftMost = v
			m.rightMost = r
			a := m.topAngle(v, l, r)
			switch {
			case a < math.Pi:
				{
					// v-r is on the advancing Front
					// v-l, l-r is on the convex hull
					f := m.NewFace(v, l, r) // f.E is v-l
					m.advancingFront = append(m.advancingFront, f.E.Next.Next)
					m.convexHull = append(m.convexHull, f.E, f.E.Next)
				}
			case a > math.Pi:
				{
					// l-r, r-v is on the advancing front
					// l-v is on the convex hull
					f := m.NewFace(v, r, l) // f.E is v-r
					m.advancingFront = append(m.advancingFront, f.E.Next.Next, f.E.Next)
					m.convexHull = append(m.convexHull, f.E)
				}
			default:
				{
					// both l & r are to the right, in a straight line v -> l -> r
					var err error
					skipped, err = m.fillColinearRight(v, l, r)
					if err != nil {
						return 0, err
					}
				}
			}
		}
	default:
		{
			// lines go straight up.
			var err error
			skipped, err = m.fillColinearVertical(v, l, r)
			if err != nil {
				return 0, err
			}
		}
	}
	glog.V(3).Infof("leftMost: %v, rightMost: %v", m.leftMost, m.rightMost)
	glog.V(3).Infof("Advancing Front: %v", m.advancingFront)
	glog.V(3).Infof("Convex Hull: %v", m.convexHull)
	if glog.V(3) {
		m.DebugSave("initialized")
		glog.V(2).Infof("Mesh: %+v", m)
		glog.V(2).Infof("Advancing Front: %+v", m.advancingFront)
		glog.V(2).Infof("Convex Hull: %+v", m.convexHull)
	}
	return skipped, nil
}

// fillColinearVertical handles the case when the initial 3 vertices are vertical
func (m *Mesh) fillColinearVertical(v, l, r int) (int, error) {
	glog.V(2).Infof("Filling colinear vertical: %v %v %v", v, l, r)
	skipped := 1
	pv := m.P[v]
	n := len(m.P)
	for i := 3; i < n; i++ {
		pc := m.P[i] // point candidate
		var prev *HalfEdge
		switch {
		case pc.X < pv.X:
			{
				// non-linear, point to left
				for j := 0; j < i-1; j++ {
					f := m.NewFace(j, j+1, i)
					if j > 0 {
						m.Pair(prev, f.E.Next.Next)
					}
					prev = f.E.Next
				}
				m.advancingFront = append(m.advancingFront, m.F[i-2].E.Next)
				m.convexHull = append(m.convexHull, m.F[0].E.Next.Next)
				for j := 0; j < i-1; j++ {
					m.convexHull = append(m.convexHull, m.F[j].E)
				}
				return skipped, nil
			}
		case pc.X > pv.X:
			{
				// non-linear, point to right
				for j := 0; j < i-1; j++ {
					f := m.NewFace(j, i, j+1)
					if j > 0 {
						m.Pair(prev, f.E)
					}
					prev = f.E.Next
				}
				for j := 0; j < i-1; j++ {
					m.advancingFront = append(m.advancingFront, m.F[j].E.Next.Next)
				}
				m.advancingFront = append(m.advancingFront, m.F[i-2].E.Next)
				m.convexHull = append(m.convexHull, m.F[0].E)
				return skipped, nil
			}
		default:
			{
				//linear
				skipped++
			}
		}
	}
	return skipped, fmt.Errorf("All points were colinear vertically: %v", m.P)
}

// fillColinearLeft handles the case when the initial 3 vertices are colinear, going to the right: l, r, v
func (m *Mesh) fillColinearLeft(v, l, r int) (int, error) {
	glog.V(2).Infof("Filling colinear left: %v %v %v", v, l, r)
	skipped := 1
	pv := m.P[v]
	n := len(m.P)
	for i := 3; i < n; i++ {
		pc := m.P[i]   // point candidate
		pp := m.P[i-1] // previous point

		a := m.topAngle(i, i-1, v)
		var prev *HalfEdge
		switch {
		case a < math.Pi-piThreshold:
			{
				for j := 0; j < i-1; j++ {
					f := m.NewFace(j, i, j+1)
					if j > 0 {
						m.Pair(prev, f.E)
					}
					prev = f.E.Next
				}
				switch {
				case pc.X > pv.X:
					{
						m.advancingFront = []*HalfEdge{m.F[i-2].E.Next}
						for j := i - 2; j >= 0; j-- {
							m.convexHull = append(m.convexHull, m.F[j].E.Next.Next)
						}
						m.convexHull = append(m.convexHull, m.F[0].E)
						return skipped, nil
					}
				case pc.X <= pv.X && pc.X > pp.X:
					{
						m.advancingFront = []*HalfEdge{
							m.F[i-2].E.Next,
							m.F[0].E,
						}
						for j := i - 2; j >= 0; j-- {
							m.convexHull = append(m.convexHull, m.F[j].E.Next.Next)

						}
						return skipped, nil
					}
				case pc.X <= pp.X:
					{
						m.advancingFront = []*HalfEdge{m.F[0].E}
						m.convexHull = append(m.convexHull, m.F[i-2].E.Next)
						for j := i - 2; j >= 0; j-- {
							m.convexHull = append(m.convexHull, m.F[j].E.Next.Next)
						}
						return skipped, nil
					}
				}
			}
		case a > math.Pi+piThreshold:
			{
				for j := 0; j < i-1; j++ {
					f := m.NewFace(j, j+1, i)
					if j > 0 {
						m.Pair(prev, f.E.Next.Next)
					}
					prev = f.E.Next
				}
				m.advancingFront = append(m.advancingFront, m.F[i-2].E.Next)
				for j := i - 2; j >= 0; j-- {
					m.advancingFront = append(m.advancingFront, m.F[j].E)
				}
				m.convexHull = append(m.convexHull, m.F[0].E.Next.Next)
				return skipped, nil
			}
		default:
			{
				skipped++
			}
		}
	}
	return skipped, fmt.Errorf("All points were colinear rightwards: %v", m.P)
}

// fillColinearRight handles the case when the initial 3 vertices are colinear, going to the right: v, l, r
func (m *Mesh) fillColinearRight(v, l, r int) (int, error) {
	glog.V(2).Infof("Filling colinear right: %v %v %v", v, l, r)
	skipped := 1
	pv := m.P[v]
	n := len(m.P)
	for i := 3; i < n; i++ {
		pc := m.P[i]   // point candidate
		pp := m.P[i-1] // previous point

		a := m.topAngle(v, i-1, i)
		var prev *HalfEdge
		switch {
		case a < math.Pi-piThreshold:
			{
				for j := 0; j < i-1; j++ {
					f := m.NewFace(j, j+1, i)
					if j > 0 {
						m.Pair(prev, f.E.Next.Next)
					}
					prev = f.E.Next
				}
				switch {
				case pc.X < pv.X:
					{
						m.advancingFront = []*HalfEdge{m.F[i-2].E.Next}
						m.convexHull = []*HalfEdge{m.F[0].E.Next.Next}
						for j := 0; j < i-1; j++ {
							m.convexHull = append(m.convexHull, m.F[j].E)
						}
						return skipped, nil
					}
				case pc.X >= pv.X && pc.X < pp.X:
					{
						m.advancingFront = []*HalfEdge{
							m.F[0].E.Next.Next,
							m.F[i-2].E.Next,
						}
						for j := 0; j < i-1; j++ {
							m.convexHull = append(m.convexHull, m.F[j].E)

						}
						return skipped, nil
					}
				case pc.X >= pp.X:
					{
						m.advancingFront = []*HalfEdge{m.F[0].E.Next.Next}
						for j := 0; j < i-1; j++ {
							m.convexHull = append(m.convexHull, m.F[j].E)
						}
						m.convexHull = append(m.convexHull, m.F[i-2].E.Next)
						return skipped, nil
					}
				}
			}
		case a > math.Pi+piThreshold:
			{
				for j := 0; j < i-1; j++ {
					f := m.NewFace(j, i, j+1)
					if j > 0 {
						m.Pair(prev, f.E)
					}
					prev = f.E.Next
				}
				for j := 0; j < i-1; j++ {
					m.advancingFront = append(m.advancingFront, m.F[j].E.Next.Next)
				}
				m.advancingFront = append(m.advancingFront, m.F[i-2].E.Next)
				m.convexHull = append(m.convexHull, m.F[0].E)
				return skipped, nil
			}
		default:
			{
				skipped++
			}
		}
	}
	return skipped, fmt.Errorf("All points were colinear rightwards: %v", m.P)
}
