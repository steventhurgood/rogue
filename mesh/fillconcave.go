package mesh

import (
	"math"

	"github.com/golang/glog"
)

// Skeleton for all concave-patching functions:
//
// a, b, c = the three vertices that'll be tested for concavity, including the new one.
//  angle =
//  if angle < ... the angle is concave
//  newface(a, c, b) // create a new face to patch the concavity
//  pair(...) // join the new face on to the existing face
//  convexhull/advancingfrontg = // patch the new edge into the convex hull or advancing front
//    if i ... //
//      eNext = recurse // keep patching holes until we have a convex shape
//      if eNext != nil:
//        pair // patch the results of our recursion
//      invalidflip // flip edges to maintain delaunay invariant
//   return edge on newly created convex shape
//  return nil // no new face was needed

// FillConcaveBottomLeft patches the convex hull from the left-hand side
// v is the new vertex at the leftmost side
// i is the index on the convex hull of the edge to the right of the new edge that we are testing.
// if a new face is created, return the left-most edge.
func (m *Mesh) FillConcaveBottomLeft(v, i int) *HalfEdge {
	e := m.convexHull
	glog.V(2).Infof("Filling convex hull from the bottom left, with vertex %v joining to edge %v(%v)", v, i, e[i])
	a := v
	b := e[i].Next.Next.To
	c := e[i].To
	angle := m.bottomAngle(a, b, c)
	glog.V(2).Infof("Bottom angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := m.NewFace(a, c, b)
		m.Pair(e[i], f.E.Next) // c-b
		ch := f.E
		ret := f.E.Next.Next
		m.InvalidFlip(e[i])
		// m.DebugSave("fill-concave-bottom-left")
		m.convexHull = append(m.convexHull[:i], append([]*HalfEdge{ch}, m.convexHull[i+1:]...)...)
		if i < len(m.convexHull)-1 {
			eNext := m.FillConcaveBottomLeft(v, i+1)
			if eNext != nil {
				m.Pair(f.E, eNext)
				m.InvalidFlip(eNext)
			}
		}
		return ret
	}
	return nil
}

// FillConcaveBottomRight patches the convex hull from the right-hand side
// v is the new vertex at the rightmost side
// i is the index on the convex hull of the edge to the left of the new edge that we are testing.
// if a new face is created, return the right-most edge.
func (m *Mesh) FillConcaveBottomRight(v, i int) *HalfEdge {
	e := m.convexHull
	glog.V(2).Infof("Filling convex hull from the bottom right, with vertex %v joining to edge %v(%v)", v, i, e[i])
	a := e[i].Next.Next.To
	b := e[i].To
	c := v
	angle := m.bottomAngle(a, b, c)
	glog.V(2).Infof("Bottom angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := m.NewFace(a, c, b)
		m.Pair(e[i], f.E.Next.Next) //b-a
		ch := f.E
		ret := f.E.Next
		m.InvalidFlip(e[i])
		// m.DebugSave("fill-concave-bottom-right")
		m.convexHull = append(m.convexHull[:i], ch)
		if i > 0 {
			eNext := m.FillConcaveBottomRight(v, i-1)
			if eNext != nil {
				m.Pair(f.E, eNext)
				m.InvalidFlip(eNext)
			}
		}
		return ret
	}
	return nil
}

// FillConcaveTopLeft patches the advancing front from the left-hand side
// v is the new vertex at the leftmost side
// i is the index on the advancing front of the edge to the right of the new edge that we are testing.
// if a new face is created, return the left-most edge.
func (m *Mesh) FillConcaveTopLeft(v, i int) *HalfEdge {

	e := m.advancingFront
	glog.V(2).Infof("Filling advancing front from the top left, with vertex %v joining to edge %v(%v)", v, i, e[i])
	a := v
	b := e[i].To
	c := e[i].Next.Next.To
	angle := m.topAngle(a, b, c)
	glog.V(2).Infof("Top angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := m.NewFace(b, c, a)
		m.Pair(e[i], f.E)
		af := f.E.Next
		ret := f.E.Next.Next
		m.InvalidFlip(e[i])
		// m.DebugSave("fill-concave-top-left")
		m.advancingFront = append([]*HalfEdge{af}, m.advancingFront[i+1:]...)
		if i < len(m.advancingFront)-1 {
			eNext := m.FillConcaveTopLeft(v, i+1)
			if eNext != nil {
				m.Pair(af, eNext)
				m.InvalidFlip(eNext)
			}
		}
		return ret
	}
	return nil
}

// FillConcaveTopRight patches the advancing front from the right-hand side
// v is the new vertex at the rightmost side
// i is the index on the advancing front of the edge to the left of the new edge that we are testing.
// if a new face is created, return the right-most edge.
func (m *Mesh) FillConcaveTopRight(v, i int) *HalfEdge {

	e := m.advancingFront
	glog.V(2).Infof("Filling advancing front from the top right with vertex %v joining to edge %v(%v)", v, i, e[i])
	a := e[i].To
	b := e[i].Next.Next.To
	c := v
	angle := m.topAngle(a, b, c)
	glog.V(2).Infof("Top angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := m.NewFace(a, b, c)
		m.Pair(e[i], f.E)
		af := f.E.Next.Next
		ret := f.E.Next
		m.InvalidFlip(e[i])
		// m.DebugSave("fill-concave-top-right")
		m.advancingFront = append(m.advancingFront[:i], af)
		if i > 0 {
			eNext := m.FillConcaveTopRight(v, i-1)
			if eNext != nil {
				m.Pair(f.E.Next.Next, eNext)
				m.InvalidFlip(eNext)
			}
		}
		return ret
	}
	return nil
}

// FillConcaveLeftwards - after creating a new face on the advancing front, fill concave gaps to the left
// i is the left edge of the new face
// v is the new point of the face
// returns the rightmost edge of a new face, if there is one
func (m *Mesh) FillConcaveLeftwards(v, i int) *HalfEdge {
	if i == 0 {
		// no concave gaps to the left
		return nil
	}

	e := m.advancingFront
	glog.V(2).Infof("Filling advancing front leftwards, with vertex %v joining to edge %v(%v)", v, i-1, e[i-1])
	a := e[i-1].To
	b := e[i].To
	c := v
	angle := m.topAngle(a, b, c)
	glog.V(2).Infof("Top angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := m.NewFace(a, b, c)
		edges := [3]*HalfEdge{f.E, f.E.Next, f.E.Next.Next}
		// m.Pair(e[i], f.E.Next)
		m.Pair(e[i], edges[1])
		// m.DebugSave("fill-concave-leftwards-flip0")
		// m.Pair(e[i-1], f.E)
		m.Pair(e[i-1], edges[0])
		// m.InvalidFlip(e[i-1])
		// m.DebugSave("fill-concave-leftwards-flip1")
		// m.DebugSave("fill-concave-leftwards")
		m.InvalidFlip(e[i])
		m.InvalidFlip(e[i-1])

		// m.advancingFront = append(m.advancingFront[:i-1], append([]*HalfEdge{f.E.Next.Next}, m.advancingFront[i+1:]...)...)
		m.advancingFront = append(m.advancingFront[:i-1], append([]*HalfEdge{edges[2]}, m.advancingFront[i+1:]...)...)
		if i > 0 {
			eNext := m.FillConcaveLeftwards(v, i-1)
			if eNext != nil {
				// m.Pair(f.E.Next.Next, eNext)
				m.Pair(edges[2], eNext)
				m.InvalidFlip(eNext)
			}
		}
		// ret := f.E.Next
		// m.InvalidFlip(e[i])
		// return ret
		return edges[1]
	}
	return nil
}

// FillConcaveRightwards - after creating a new face on the advancing front, fill concave gaps to the right
// i is the right edge of the new face
// v is the new point of the face
// returns the rightmost edge of a new face, if there is one
func (m *Mesh) FillConcaveRightwards(v, i int) *HalfEdge {
	if i == len(m.advancingFront)-1 {
		// no concave gaps to the right
		return nil
	}

	e := m.advancingFront
	glog.V(2).Infof("Filling advancing front rightwards, with vertex %v joining to edge %v(%v)", v, i+1, e[i+1])
	a := v
	b := e[i+1].To
	c := e[i+1].Next.Next.To
	angle := m.topAngle(a, b, c)
	glog.V(2).Infof("Top angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := m.NewFace(a, b, c)
		edges := [3]*HalfEdge{f.E, f.E.Next, f.E.Next.Next}
		m.Pair(e[i], edges[0])
		m.Pair(e[i+1], edges[1])
		// m.Pair(e[i], f.E)
		// m.Pair(e[i+1], f.E.Next)
		// m.DebugSave("fill-concave-rightwards")
		m.InvalidFlip(e[i])
		m.InvalidFlip(e[i+1])

		//m.advancingFront = append(m.advancingFront[:i], append([]*HalfEdge{f.E.Next.Next}, m.advancingFront[i+2:]...)...)
		m.advancingFront = append(m.advancingFront[:i], append([]*HalfEdge{edges[2]}, m.advancingFront[i+2:]...)...)
		if i < len(m.advancingFront)-1 {

			eNext := m.FillConcaveRightwards(v, i)
			if eNext != nil {
				// m.Pair(f.E.Next.Next, eNext)
				m.Pair(edges[2], eNext)
				m.InvalidFlip(eNext)
			}
		}
		// ret := f.E
		// m.InvalidFlip(e[i])
		m.InvalidFlip(e[i])
		return edges[0]
	}
	return nil
}
