// +build regression

package mesh

import (
	"math/rand"
	"testing"
)

func newRandomMesh(n int, seed int64) *Mesh {
	r := rand.New(rand.NewSource(seed))
	m := &Mesh{}
	for i := 0; i < n; i++ {
		m.P = append(m.P, Vertex{
			X: r.Float64(),
			Y: r.Float64(),
		})
	}
	return m
}

func TestTriangulateRandom(t *testing.T) {
	for i := startTestsAt; i < randomTests; i++ {
		m := newRandomMesh(randomTestSize, int64(i))
		err := m.Triangulate()
		if err != nil {
			t.Errorf("Error triangulating mesh:\n%v\nerr: %v", m, err)
		}
		if w, msg := isWellFormed(m); !w {
			t.Errorf("(seed: %v)\nMesh:\n%v\nis not well formed: %v", i, m, msg)
		}
		if v, err := m.DebugValid(); !v || err != nil {
			t.Errorf("(seed: %v)\nMesh:\n%v\nIs not valid (error: %v)", i, m, err)
		}
	}
}

func newParallelMesh(n int, xd, yd float64, r *rand.Rand) *Mesh {
	m := &Mesh{}
	for i := 0; i < n; i++ {
		if r.Float64() < 0.5 {
			m.P = append(m.P, Vertex{
				X: xd,
				Y: float64(i) * yd,
			})
		} else {
			m.P = append(m.P, Vertex{
				X: -xd,
				Y: float64(i) * yd,
			})
		}
	}
	return m
}

func TestParallel(t *testing.T) {
	for i := startTestsAt; i < randomTests; i++ {
		m := newRandomMesh(randomTestSize, int64(i))
		err := m.Triangulate()
		if err != nil {
			t.Errorf("Error triangulating mesh:\n%v\nerr: %v", m, err)
		}
		if w, msg := isWellFormed(m); !w {
			t.Errorf("(seed: %v)\nMesh:\n%v\nis not well formed: %v", i, m, msg)
		}
		if v, err := m.DebugValid(); !v || err != nil {
			t.Errorf("(seed: %v)\nMesh:\n%v\nIs not valid (error: %v)", i, m, err)
		}
	}
}

// Test previously known-bad test cases
func TestTestCases(t *testing.T) {
	tests := []*Mesh{
		//		&Mesh{
		//			P: []Vertex{{X: 176, Y: 221}, {X: 176.5, Y: 272}, {X: 176, Y: 284.5}, {X: 176, Y: 297}, {X: 176.5, Y: 309.5}, {X: 176, Y: 327}, {X: 176.5, Y: 343.5}, {X: 176, Y: 353}, {X: 176.5, Y: 366.5}, {X: 176.5, Y: 384.5}, {X: 176.5, Y: 414}, {X: 176.5, Y: 426.5}, {X: 176.5, Y: 438}, {X: 176.5, Y: 452}, {X: 176, Y: 468.5}, {X: 176.5, Y: 482}},/
		//		},
		&Mesh{
			P: []Vertex{{X: 17, Y: 106}, {X: 50.5, Y: 106}, {X: 209, Y: 106}, {X: 117.5, Y: 106}, {X: 219.5, Y: 106}, {X: 236.5, Y: 106}, {X: 261.5, Y: 106}, {X: 106, Y: 106}, {X: 188.5, Y: 106}, {X: 287, Y: 106}, {X: 129.5, Y: 106}, {X: 276, Y: 106}, {X: 142, Y: 106}, {X: 176.5, Y: 106}, {X: 61.5, Y: 106}, {X: 72, Y: 106.5}, {X: 154, Y: 106.5}, {X: 93, Y: 106.5}, {X: 199, Y: 106.5}, {X: 309.5, Y: 106.5}, {X: 39.5, Y: 106.5}, {X: 4, Y: 106.5}, {X: 321.5, Y: 106.5}, {X: 297.5, Y: 106.5}, {X: 29.5, Y: 106.5}, {X: 165, Y: 106.5}, {X: 249.5, Y: 106.5}, {X: 81.5, Y: 106.5}},
		},
	}
	for i, m := range tests {
		err := m.Triangulate()
		if err != nil {
			t.Errorf("Error triangulating mesh %i:\n%v\nerr: %v", i, m, err)
		}
		if w, msg := isWellFormed(m); !w {
			t.Errorf("Mesh %v:\n%v\nis not well formed: %v", i, m, msg)
		}
		if v, err := m.DebugValid(); !v || err != nil {
			t.Errorf("Mesh %v:\n%v\nIs not valid (error: %v)", i, m, err)
		}
	}
}
