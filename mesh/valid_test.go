package mesh

import "testing"

func baseQuadMesh() *Mesh {
	m := &Mesh{
		P: []Vertex{
			Vertex{X: 0, Y: 0},
			Vertex{X: 10, Y: 0},
			Vertex{X: 9.8, Y: -3},
			Vertex{X: 9.8, Y: 3},
		},
	}
	return m
}

func invalidQuadMesh() *Mesh {
	m := baseQuadMesh()
	e := []*HalfEdge{
		&HalfEdge{To: 2, m: m}, // 0-2
		&HalfEdge{To: 3, m: m}, // 2-3
		&HalfEdge{To: 0, m: m}, // 3-0
		&HalfEdge{To: 1, m: m}, // 2-1
		&HalfEdge{To: 3, m: m}, // 1-3
		&HalfEdge{To: 2, m: m}, // 3-2
	}
	e[0].Next = e[1]
	e[1].Next = e[2]
	e[2].Next = e[0]
	e[3].Next = e[4]
	e[4].Next = e[5]
	e[5].Next = e[3]

	e[1].Pair = e[5]
	e[5].Pair = e[1]

	f := []*Face{
		&Face{
			E: e[0],
			m: m,
		},
		&Face{
			E: e[3],
			m: m,
		},
	}
	e[0].F = f[0]
	e[1].F = f[0]
	e[2].F = f[0]
	e[3].F = f[1]
	e[4].F = f[1]
	e[5].F = f[1]
	m.F = f
	return m
}

func validQuadMesh() *Mesh {
	m := baseQuadMesh()
	e := []*HalfEdge{
		&HalfEdge{To: 2, m: m}, // 0-2
		&HalfEdge{To: 1, m: m}, // 2-1
		&HalfEdge{To: 0, m: m}, // 1-0
		&HalfEdge{To: 1, m: m}, // 0-1
		&HalfEdge{To: 3, m: m}, // 1-3
		&HalfEdge{To: 0, m: m}, // 3-0
	}
	e[0].Next = e[1]
	e[1].Next = e[2]
	e[2].Next = e[0]
	e[3].Next = e[4]
	e[4].Next = e[5]
	e[5].Next = e[3]

	e[2].Pair = e[3]
	e[3].Pair = e[2]

	f := []*Face{
		&Face{
			E: e[0],
			m: m,
		},
		&Face{
			E: e[3],
			m: m,
		},
	}
	e[0].F = f[0]
	e[1].F = f[0]
	e[2].F = f[0]
	e[3].F = f[1]
	e[4].F = f[1]
	e[5].F = f[1]
	m.F = f
	return m
}

func TestValid(t *testing.T) {
	valid := validQuadMesh()
	t.Log(valid)
	if v, err := valid.Valid(); !v || err != nil {
		t.Errorf("Valid mesh:\n%v\nis not valid (%v)", valid, err)
		valid.DebugSave("test-valid")
	}
}

func TestInvalid(t *testing.T) {
	invalid := invalidQuadMesh()
	t.Log(invalid)
	if v, err := invalid.Valid(); v || err != nil {
		t.Errorf("Invalid mesh:\n%v\nis valid (%v)", invalid, err)
		invalid.DebugSave("test-invalid")
	}
}
