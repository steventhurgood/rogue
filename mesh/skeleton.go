package mesh

import (
	"fmt"
	"image/color"
	"math"
	"math/rand"
	"sort"

	"github.com/golang/glog"
	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
)

type Skeleton struct {
	P []Vertex
	E []SkeletonEdge
}

type Joiner interface {
	Join(a, b int) error
}

// Join - Joins nodes in a graph-like object.
// If edges are on the spanning tree, then they are always connected.
// Otherwise, they are connected with probability p [0, 1]
func (s *Skeleton) Join(j Joiner, p float64, r *rand.Rand) error {
	// P    [2]int // edges are not directional; p[0] is always < p[1]
	for _, e := range s.E {
		aIndex, bIndex := e.P[0], e.P[1]
		a, b := s.P[aIndex].Index, s.P[bIndex].Index
		if e.Span || r.Float64() < p {
			if err := j.Join(a, b); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Skeleton) AddEdge(a, b int) {
	if _, ok := s.FindEdge(a, b); ok {
		// edge already exists
		return
	}
	switch {
	case a == b:
		{
			return
		}
	case a < b:
		{
			s.E = append(s.E, SkeletonEdge{s, [2]int{a, b}, false})
		}
	case b < a:
		{
			s.E = append(s.E, SkeletonEdge{s, [2]int{b, a}, false})
		}
	}
}

// Skeleton uses the dot product to get the smallest angle between two lines ab, ba, defined by three vertices.
func (s *Skeleton) Angle(a, b, c int) float64 {
	ba := Vertex{
		X: s.P[a].X - s.P[b].X,
		Y: s.P[a].Y - s.P[b].Y,
	}
	baMag := math.Sqrt(ba.X*ba.X + ba.Y*ba.Y)
	ba.X /= baMag // normalize
	ba.Y /= baMag // normalize

	bc := Vertex{
		X: s.P[c].X - s.P[b].X,
		Y: s.P[c].Y - s.P[b].Y,
	}
	bcMag := math.Sqrt(bc.X*bc.X + bc.Y*bc.Y)
	bc.X /= bcMag // normalize
	bc.Y /= bcMag // normalize

	dotProduct := ba.X*bc.X + ba.Y*bc.Y
	angle := math.Acos(dotProduct)
	return angle
}

type SkeletonEdge struct {
	s    *Skeleton
	P    [2]int // edges are not directional; p[0] is always < p[1]
	Span bool
}

// return the index of the desired edge, and a boolean which states whether it was found or not.
func (s *Skeleton) FindEdge(a, b int) (int, bool) {
	p, q := a, b
	if b < a {
		p, q = b, a
	}
	for i, e := range s.E {
		if e.P[0] == p && e.P[1] == q {
			return i, true
		}
	}
	return -1, false
}

func (s *Skeleton) markEdgeSpan(a, b int) {
	if e, ok := s.FindEdge(a, b); ok {
		s.E[e].Span = true
	} else {
		glog.Warningf("Failed to find edge: a-b")
	}
}

func (e *SkeletonEdge) Len() int {
	return 2
}

func (e *SkeletonEdge) XY(i int) (float64, float64) {
	// e.s.P - skeleton points
	// e.P[i] - index into points
	p := e.s.P[e.P[i]]
	return p.X, p.Y
}

func BetaSkeleton(m *Mesh, beta float64) (*Skeleton, error) {
	if beta < 1.0 {
		return nil, fmt.Errorf("Cannot create skeleton with beta < 1.0 (%v)", beta)
	}

	threshold := math.Asin(1 / beta)

	s := &Skeleton{
		P: make([]Vertex, len(m.P)),
	}
	// TODO - looped copy, rather than copy(), to avoid bringing *HalfEdge pointers with us
	copy(s.P, m.P)
	for _, f := range m.F {
		e := f.E
		for j := 0; j < 3; j++ {
			a := e.To
			c := e.Next.Next.To
			bs := []int{e.Next.To} // list of candidate points b to test angles a->b-c
			if e.Pair != nil {
				bs = append(bs, e.Pair.Next.To)
			}
			valid := true
			for _, b := range bs {
				angle := s.Angle(a, b, c)
				if angle > threshold {
					valid = false
					break
				}
			}
			if valid {
				s.AddEdge(a, c)
			}
			e = e.Next
		}
	}
	if err := s.spanningTree(); err != nil {
		return nil, err
	}
	return s, nil
}

type byEdgeLength []SkeletonEdge

func (e byEdgeLength) Len() int {
	return len(e)
}

func (e byEdgeLength) Less(a, b int) bool {
	aStartIndex := e[a].P[0]
	aEndIndex := e[a].P[1]

	aStartVertex := e[a].s.P[aStartIndex]
	aEndVertex := e[a].s.P[aEndIndex]

	aVector := Vertex{
		X: aEndVertex.X - aStartVertex.X,
		Y: aEndVertex.Y - aStartVertex.Y,
	}

	aLen := math.Sqrt(aVector.X*aVector.X + aVector.Y*aVector.Y)

	bStartIndex := e[b].P[0]
	bEndIndex := e[b].P[1]

	bStartVertex := e[b].s.P[bStartIndex]
	bEndVertex := e[b].s.P[bEndIndex]

	bVector := Vertex{
		X: bEndVertex.X - bStartVertex.X,
		Y: bEndVertex.Y - bStartVertex.Y,
	}

	bLen := math.Sqrt(bVector.X*bVector.X + bVector.Y*bVector.Y)

	return aLen < bLen
}

func (e byEdgeLength) Swap(a, b int) {
	e[a], e[b] = e[b], e[a]
}

// each vertex is in a group identified by an integer
type vertexGroup []int

// annotate the edges of the skeleton that are in the euclindian spanning tree
func (s *Skeleton) spanningTree() error {
	edges := make([]SkeletonEdge, len(s.E))
	copy(edges, s.E)
	groups := make(vertexGroup, len(s.P))
	for i := range groups {
		groups[i] = i
	}

	sort.Sort(byEdgeLength(edges))
	for i := range edges {
		p, q := edges[i].P[0], edges[i].P[1]
		pGroup, qGroup := groups[p], groups[q]
		if pGroup != qGroup {
			s.markEdgeSpan(p, q)
		}
		for vertex, group := range groups {
			if group == qGroup {
				groups[vertex] = pGroup
			}
		}
	}
	return nil
}

func PlotSkeleton(s *Skeleton, p *plot.Plot) error {
	for _, e := range s.E {
		// fmt.Printf("Plotting Edge: %v-%v (%v)\n", e.P[0], e.P[1], e.Span)
		l, err := plotter.NewLine(&e)
		if err != nil {
			return err
		}
		if e.Span {
			l.Color = color.RGBA{64, 64, 64, 255}
		} else {
			l.Color = color.RGBA{64, 64, 64, 128}
		}
		p.Add(l)
	}
	return nil
}

func MergeSkeletons(skeletons []*Skeleton) *Skeleton {
	skeleton := &Skeleton{}
	// P []Vertex
	// E []SkeletonEdge
	offset := 0
	for _, s := range skeletons {
		for _, p := range s.P {
			skeleton.P = append(skeleton.P, Vertex{X: p.X, Y: p.Y})
		}
		for _, e := range s.E {
			// s    *Skeleton
			// P    [2]int // edges are not directional; p[0] is always < p[1]
			// Span bool
			skeleton.E = append(skeleton.E, SkeletonEdge{
				s:    skeleton,
				P:    [2]int{e.P[0] + offset, e.P[1] + offset},
				Span: e.Span,
			})
		}
		offset = len(skeleton.P)
	}
	return skeleton
}
