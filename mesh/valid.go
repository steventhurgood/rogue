package mesh

import (
	"fmt"

	"github.com/golang/glog"
)

const (
	delaunayThreshold = 1e-10 // bump to handle edge conditions - eg., points all in a circle. This number can be considered "close enough"
)

// Valid - tests if all of the edges in all of the faces of a mesh are valid
func (m *Mesh) Valid() (bool, error) {
	for _, f := range m.F {
		if v, err := f.Valid(); !v || err != nil {
			return false, err
		}
	}
	return true, nil
}

// Valid - tests whether all edges in a face are valid
func (f *Face) Valid() (bool, error) {
	i := 0
	origin := f.E
	e := f.E
	for {
		if e.To != e.Next.Next.Next.To {
			return false, fmt.Errorf("%v edge %v (%v != %v)", f, i, e.To, e.Next.Next.Next.To)
		}
		if !e.Valid() {
			return false, nil
		}
		i++
		e = e.Next
		if e == origin {
			break
		}
		if i > 3 {
			return false, fmt.Errorf("Face %v has too many edges: %v", f, e.Next.Next.Next.Next)
		}
	}
	return true, nil
}

// Valid - tests if the point opposite the edge's pair is outside of the circle formed by the three points of e's triangle
func (e *HalfEdge) Valid() bool {
	if e.Pair == nil {
		return true
	}
	a := e.m.P[e.To]
	b := e.m.P[e.Next.To]
	c := e.m.P[e.Next.Next.To]

	d := e.m.P[e.Pair.Next.To]
	glog.V(2).Infof("Testing validity: (%v %v %v) - %v", a, b, c, d)

	m := [][]float64{
		[]float64{a.X - d.X, a.Y - d.Y, (a.X*a.X - d.X*d.X) + (a.Y*a.Y - d.Y*d.Y)},
		[]float64{b.X - d.X, b.Y - d.Y, (b.X*b.X - d.X*d.X) + (b.Y*b.Y - d.Y*d.Y)},
		[]float64{c.X - d.X, c.Y - d.Y, (c.X*c.X - d.X*d.X) + (c.Y*c.Y - d.Y*d.Y)},
	}
	det := determinant(m)
	return det <= delaunayThreshold
}

// calculates the determinant of a 3x3 matrix
func determinant(m [][]float64) float64 {
	a := m[0][0]
	b := m[0][1]
	c := m[0][2]

	d := m[1][0]
	e := m[1][1]
	f := m[1][2]

	g := m[2][0]
	h := m[2][1]
	i := m[2][2]

	det := a*(e*i-f*h) - b*(d*i-f*g) + c*(d*h-e*g)
	return det
}

// DebugValid - tests if all of the edges in all of the faces of a mesh are valid
func (m *Mesh) DebugValid() (bool, error) {
	for _, f := range m.F {
		if v, err := f.DebugValid(); !v || err != nil {
			return false, err
		}
	}
	return true, nil
}

// Valid - tests whether all edges in a face are valid
func (f *Face) DebugValid() (bool, error) {
	i := 0
	origin := f.E
	e := f.E
	for {
		if e.To != e.Next.Next.Next.To {
			return false, fmt.Errorf("%v edge %v (%v != %v)", f, i, e.To, e.Next.Next.Next.To)
		}
		if !e.DebugValid() {
			fmt.Printf("*INVALID*\nEdge: %v, Face: %v\n", e, f)
			return false, nil
		}
		i++
		e = e.Next
		if e == origin {
			break
		}
		if i > 3 {
			return false, fmt.Errorf("Face %v has too many edges: %v", f, e.Next.Next.Next.Next)
		}
	}
	return true, nil
}

// Valid - tests if the point opposite the edge's pair is outside of the circle formed by the three points of e's triangle
func (e *HalfEdge) DebugValid() bool {
	if e.Pair == nil {
		return true
	}
	a := e.m.P[e.To]
	b := e.m.P[e.Next.To]
	c := e.m.P[e.Next.Next.To]

	d := e.m.P[e.Pair.Next.To]
	glog.V(2).Infof("Testing validity: (%v %v %v) - %v", a, b, c, d)

	m := [][]float64{
		[]float64{a.X - d.X, a.Y - d.Y, (a.X*a.X - d.X*d.X) + (a.Y*a.Y - d.Y*d.Y)},
		[]float64{b.X - d.X, b.Y - d.Y, (b.X*b.X - d.X*d.X) + (b.Y*b.Y - d.Y*d.Y)},
		[]float64{c.X - d.X, c.Y - d.Y, (c.X*c.X - d.X*d.X) + (c.Y*c.Y - d.Y*d.Y)},
	}
	det := determinant(m)
	if det > delaunayThreshold {
		fmt.Printf("Invalid configuration: ([%v] - %v - %v) %v. det: %v\n", e, e.Next, e.Next.Next, e.Pair.Next.To, det)
	}
	return det <= delaunayThreshold
}
