package mesh

import (
	"fmt"
	"math"
	"testing"
)

const (
	startTestsAt   = 0
	randomTests    = 100
	randomTestSize = 10
)

// isWellFormed tests whether a mesh is properly triangular - all triangles link up correctly, etc.
func isWellFormed(m *Mesh) (bool, string) {
	for i, p := range m.P {
		if p.E == nil {
			return false, fmt.Sprintf("Vertex %x E is nil", i)
		}
		if p.E.Next == nil {
			return false, fmt.Sprintf("Vertex %x E.Next is nil", i)
		}
		if p.E.Next.Next == nil {
			return false, fmt.Sprintf("Vertex %x E.Next.Next is nil", i)
		}
		if i != p.E.Next.Next.To {
			return false, fmt.Sprintf("Vertex %x != p.E.Next.Next.To (%v): %v - %v - %v", i, p.E.Next.Next.To, p.E, p.E.Next, p.E.Next.Next)
		}
	}
	for _, f := range m.F {
		e := f.E
		for i := 0; i < 3; i++ {
			if e != e.Next.Next.Next {
				return false, fmt.Sprintf("%v edge+3 - %v != %v", f, e, e.Next.Next.Next)
			}
			if e.Pair != nil {
				if e != e.Pair.Pair {
					return false, fmt.Sprintf("%v pair - %v != %v.Pair (%v)", f, e, e.Pair, e.Pair.Pair)
				}
			}
		}
	}
	return true, ""
}
func newLinearMesh(n int, xd, yd float64) *Mesh {
	m := &Mesh{}
	for i := 0; i < n; i++ {
		m.P = append(m.P, Vertex{
			X: float64(i) * xd,
			Y: float64(i) * yd,
		})
	}
	return m
}

func newTriangularMesh(n int, xd, yd, finalX, finalY float64) *Mesh {
	m := &Mesh{}

	for i := 0; i < n; i++ {
		m.P = append(m.P, Vertex{
			X: float64(i) * xd,
			Y: float64(i) * yd,
		})
	}
	m.P = append(m.P, Vertex{
		X: finalX,
		Y: float64(n)*yd + finalY,
	})
	return m
}
func newCircularMesh(n int) *Mesh {
	m := &Mesh{}
	for i := 0; i < n; i++ {
		a := 2 * math.Pi * float64(i) / float64(n)
		m.P = append(m.P, Vertex{
			X: math.Cos(a),
			Y: math.Sin(a),
		})
	}
	return m
}
func TestTriangulateCircular(t *testing.T) {
	m := newCircularMesh(10)
	err := m.Triangulate()
	if err != nil {
		t.Errorf("Error triangulating mesh:\n%v\nerr: %v", m, err)
	}
	if w, msg := isWellFormed(m); !w {
		t.Errorf("Mesh:\n%v\nis not well formed: %v", m, msg)
	}
	if v, err := m.DebugValid(); !v || err != nil {
		t.Errorf("Mesh:\n%v\nIs not valid (error: %v)", m, err)
	}
}

func TestTriangulateLinear(t *testing.T) {
	m := newLinearMesh(10, 0.3, 0.5)
	err := m.Triangulate()
	if err == nil {
		t.Errorf("Triangulating linear mesh should give error:\n%v", m)
	}
}

func TestTriangulateTriangle(t *testing.T) {
	tests := []*Mesh{
		// up and to the right
		newTriangularMesh(10, 0.3, 0.5, 0.0, 0.0),
		newTriangularMesh(10, 0.3, 0.5, -1.0, 0.0),
		newTriangularMesh(10, 0.3, 0.5, 2.0, 0.0),
		newTriangularMesh(10, 0.3, 0.5, 3.1, 4.0),
		newTriangularMesh(10, 0.3, 0.5, 6.0, 0.0),

		// straight vertical and horizontal
		newTriangularMesh(10, 0.0, 1.0, 1.0, 1.0),
		newTriangularMesh(10, 0.0, 1.0, 1.0, -1.0),
		newTriangularMesh(10, 1.0, 0.0, 1.0, 1.0),
		newTriangularMesh(10, -1.0, 0.0, 1.0, 1.0),

		// up and to the left
		newTriangularMesh(10, -0.3, 0.5, 0.0, 0.0),
		newTriangularMesh(10, -0.3, 0.5, 1.0, 0.0),
		newTriangularMesh(10, -0.3, 0.5, -2.0, 0.0),
		newTriangularMesh(10, -0.3, 0.5, -3.1, 4.0),
		newTriangularMesh(10, -0.3, 0.5, -6.0, 0.0),
	}
	for i, m := range tests {
		err := m.Triangulate()
		if err != nil {
			t.Errorf("Error triangulating mesh:\n%v\nerr: %v", m, err)
		} else {
			m.DebugSave(fmt.Sprintf("testing-triangular-%v", i))
			if w, msg := isWellFormed(m); !w {
				t.Errorf("Mesh:\n%v\nis not well formed: %v", m, msg)
			}
			if v, err := m.DebugValid(); !v || err != nil {
				t.Errorf("Mesh:\n%v\nIs not valid (error: %v)", m, err)
			}
		}
	}
}

func TestBottomAngle(t *testing.T) {
	tests := []struct {
		a    Vertex
		b    Vertex
		c    Vertex
		want float64
	}{
		{
			a:    Vertex{-1, -1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, 1, nil, 0},
			want: math.Pi,
		}, {
			a:    Vertex{-1, 1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, 1, nil, 0},
			want: 3 * math.Pi / 2,
		}, {
			a:    Vertex{-1, -1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, -1, nil, 0},
			want: math.Pi / 2,
		}, {
			a:    Vertex{-1, 1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, -1, nil, 0},
			want: math.Pi,
		}, {
			a:    Vertex{-1, 0, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, 0, nil, 0},
			want: math.Pi,
		},
	}
	for _, test := range tests {
		m := Mesh{
			P: []Vertex{test.a, test.b, test.c},
		}
		got := m.bottomAngle(0, 1, 2)
		if got != test.want {
			t.Errorf("bottomAngle(%v, %v, %v) -> %v. Want: %v", test.a, test.b, test.c, got, test.want)
		}
	}

}

func TestTopAngle(t *testing.T) {
	tests := []struct {
		a    Vertex
		b    Vertex
		c    Vertex
		want float64
	}{
		{
			a:    Vertex{-1, -1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, 1, nil, 0},
			want: math.Pi,
		}, {
			a:    Vertex{-1, 1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, 1, nil, 0},
			want: math.Pi / 2,
		}, {
			a:    Vertex{-1, -1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, -1, nil, 0},
			want: 3 * math.Pi / 2,
		}, {
			a:    Vertex{-1, 1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, -1, nil, 0},
			want: math.Pi,
		}, {
			a:    Vertex{-1, 0, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{1, 0, nil, 0},
			want: math.Pi,
		}, {
			a:    Vertex{-1, -1, nil, 0},
			b:    Vertex{0, 0, nil, 0},
			c:    Vertex{-1, 1, nil, 0},
			want: math.Pi / 2,
		},
	}
	for _, test := range tests {
		m := Mesh{
			P: []Vertex{test.a, test.b, test.c},
		}
		got := m.topAngle(0, 1, 2)
		if got != test.want {
			t.Errorf("topAngle(%v, %v, %v) -> %v. Want: %v", test.a, test.b, test.c, got, test.want)
		}
	}

}
