package rogue

import (
	"fmt"
	"math/rand"

	"github.com/golang/glog"
)

var (
	layoutRegistry = make(map[string]Layout)
)

type Layout interface {
	Name() string
	Position(BoxLike, *Zone, *rand.Rand) error
}

// LayoutOptions provide hints to layout engines
type LayoutOptions struct {
	Jitter  float64
	Density float64
}

func RegisterLayout(l Layout) error {
	name := l.Name()
	if _, ok := layoutRegistry[name]; ok {
		return fmt.Errorf("Layout %v already registered", name)
	}
	glog.Infof("Registering layout: %v", name)
	layoutRegistry[name] = l
	return nil
}

func GetLayoutOrDie(layoutName string) Layout {
	l, err := GetLayout(layoutName)
	if err != nil {
		glog.Fatal(err)
	}
	return l
}

func GetLayout(layoutName string) (Layout, error) {
	if l, ok := layoutRegistry[layoutName]; ok {
		return l, nil
	}
	return nil, fmt.Errorf("Layout %v not registered (%v)", layoutName, layoutRegistry)
}
