package rogue

import "testing"

func TestZoneToBoxLike(t *testing.T) {
	z := []*Zone{
		&Zone{
			Box: Box{
				x:      0,
				y:      0,
				width:  10,
				height: 10,
			},
		},
	}
	b := ZoneToBoxLike(z)
	b[0].SetX(10)

	if z[0].X() != 10 {
		t.Errorf("ZoneToBoxLike(%v) -> %v", r, b)
	}
}
