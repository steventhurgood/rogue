package rogue

import (
	"bytes"
	"fmt"

	"github.com/golang/glog"
)

var (
	themeRegistry = make(map[string]*Theme)
)

type Theme struct {
	Name string
	ZoneAttributes
}

func (t *Theme) String() string {
	var b bytes.Buffer
	fmt.Fprintf(&b, "Name: %v\n", t.Name)
	fmt.Fprintf(&b, "Attributes: %+v\n", t.ZoneAttributes)
	return b.String()
}

func RegisterTheme(t *Theme) error {
	if _, ok := themeRegistry[t.Name]; ok {
		return fmt.Errorf("Theme %v already registered", t.Name)
	}
	glog.Infof("Registering theme: %v", t.Name)
	themeRegistry[t.Name] = t
	return nil
}

func GetThemeOrDie(themeName string) *Theme {
	t, err := GetTheme(themeName)
	if err != nil {
		glog.Fatal(err)
	}
	return t
}

func GetTheme(themeName string) (*Theme, error) {
	if t, ok := themeRegistry[themeName]; ok {
		return t, nil
	}
	return nil, fmt.Errorf("Theme %v not registered (%v)", themeName, themeRegistry)
}
