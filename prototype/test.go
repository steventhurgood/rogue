package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"log"
	"math"
	"math/rand"
	"os"

	"bitbucket.org/steventhurgood/main/astar"
)

type Material int

type Direction int

const (
	North Direction = iota
	South
	East
	West
)

type MapPosition struct {
	m              *Map
	x, y           int
	dest_x, dest_y int
	dir            Direction
	cost           int
}

func (m *MapPosition) To() astar.Node {
	return m
}

func (m *MapPosition) Cost() int {
	return m.cost
}

func (m *MapPosition) Id() astar.NodeId {
	return astar.NodeId(fmt.Sprintf("%v,%v", m.x, m.y))
}

func (m *MapPosition) ReachedEnd() bool {
	// return m.x == m.dest_x && m.y == m.dest_y
	directions := [][2]int{
		[2]int{m.x, m.y - 1}, // north
		[2]int{m.x, m.y + 1}, // south
		[2]int{m.x + 1, m.y}, // east
		[2]int{m.x - 1, m.y}, // west
	}
	for _, d := range directions {
		if d[0] == m.dest_x && d[1] == m.dest_y {
			return true
		}
	}
	return false
}

func (m *MapPosition) EstimateCost() int {
	// pythagoras : math.sqrt((n.x - n.g.end.x)^2 + (n.y - n.g.end.y)^2)
	xDiff := m.x - m.dest_x
	if xDiff < 0 {
		xDiff = -xDiff
	}
	yDiff := m.y - m.dest_y
	if yDiff < 0 {
		yDiff = -yDiff
	}
	return xDiff + yDiff
	// return int(math.Sqrt(float64(xDiff*xDiff + yDiff*yDiff)))
}

func (m *MapPosition) EdgesOut() []astar.Edge {
	// test north, south, east, west. Look for rock or the final destination door
	// log.Printf("Finding edges from %v, %v", m.x, m.y)
	edges := make([]astar.Edge, 0)
	directions := [][2]int{
		[2]int{m.x, m.y - 1}, // north
		[2]int{m.x, m.y + 1}, // south
		[2]int{m.x + 1, m.y}, // east
		[2]int{m.x - 1, m.y}, // west
	}
	for d, offset := range directions {
		dir := Direction(d)
		if offset[0] > 0 && offset[0] < m.m.Config.Width && offset[1] > 0 && offset[1] < m.m.Config.Height {
			mat := m.m.tiles[offset[0]][offset[1]].Material
			// if mat != MatWall && mat != MatRoom && mat != MatTunnel {
			if mat == MatRock || mat == MatTunnel {
				newPos := &MapPosition{
					m:      m.m,
					x:      offset[0],
					y:      offset[1],
					dest_x: m.dest_x,
					dest_y: m.dest_y,
					dir:    dir,
				}
				if m.m.tiles[offset[0]][offset[1]].Material == MatTunnel {
					newPos.cost = 1
				} else {
					if dir == m.dir {
						newPos.cost = 2
					} else {
						newPos.cost = 3
					}
				}
				edges = append(edges, newPos)

			}
		}
	}
	return edges
}

func (m *MapPosition) Visit() {
	// log.Printf("Tunneling: (%v, %v)", m.x, m.y)
	m.m.tiles[m.x][m.y].Material = MatTunnel
}

const (
	MatSolid Material = iota
	MatRock
	MatRoom
	MatTunnel
	MatWall
	MatDoor
)

type MapTile struct {
	Material Material
}

type Map struct {
	Config MapConfig
	tiles  [][]MapTile
}

type MapConfig struct {
	Width, Height  int
	NumRooms       int
	RoomSize       float64
	RoomSizeStdDev float64
	Damping        float64
	Repulsion      float64
	MaxMoves       int
	MinRoomSize    int
	Connections    int
	DoorRatio      float64 // average doors per 10x10
}

func NewMap(config MapConfig) *Map {
	tiles := make([][]MapTile, config.Width)
	for i := 0; i < config.Width; i++ {
		tiles[i] = make([]MapTile, config.Height)
		for j := 1; j < config.Height-1; j++ {
			if i > 0 && i < config.Width-1 {
				tiles[i][j].Material = MatRock
			}
		}
	}
	return &Map{
		Config: config,
		tiles:  tiles,
	}
}

type Room struct {
	x, y          int
	width, height int
	doors         [][4]int
	numDoors      int
}

func (m *Map) Overlapping(rooms []Room) bool {
	tiles := make([][]bool, m.Config.Width)
	for i := 0; i < m.Config.Width; i++ {
		tiles[i] = make([]bool, m.Config.Height)
	}
	for _, r := range rooms {
		for x := r.x; x < r.x+r.width; x++ {
			for y := r.y; y < r.y+r.height; y++ {
				if x >= 0 && x < m.Config.Width && y >= 0 && y < m.Config.Height {
					if tiles[x][y] {
						return true
					}
					tiles[x][y] = true
				}
			}
		}
	}
	return false
}

func Corners(r Room) [][2]int {
	return [][2]int{
		[2]int{r.x, r.y},                      // top left
		[2]int{r.x + r.width, r.y},            // top right
		[2]int{r.x, r.y + r.height},           // bottom left
		[2]int{r.x + r.width, r.y + r.height}, // bottom right
	}
}

func Contains(r Room, p [2]int) bool {
	return p[0] >= r.x && p[0] < r.x+r.width &&
		p[1] >= r.y && p[1] < r.y+r.height
}

func Overlapping(a, b Room) bool {
	for _, c := range Corners(a) {
		if Contains(b, c) {
			return true
		}
	}
	for _, c := range Corners(b) {
		if Contains(a, c) {
			return true
		}
	}
	return false

	tiled := make(map[[2]int]bool)
	for x := a.x; x < a.x+a.width; x++ {
		for y := a.y; y < a.y+a.height; y++ {
			tiled[[2]int{x, y}] = true
		}
	}
	for x := b.x; x < b.x+b.width; x++ {
		for y := b.y; y < b.y+b.height; y++ {
			if tiled[[2]int{x, y}] {
				return true
			}
		}
	}
	return false
}

func (m *Map) BuildRooms() {
	rooms := make([]Room, m.Config.NumRooms)
	for i := 0; i < m.Config.NumRooms; i++ {
		width := int(math.Abs(rand.NormFloat64()*m.Config.RoomSizeStdDev + m.Config.RoomSize))
		height := int(math.Abs(rand.NormFloat64()*m.Config.RoomSizeStdDev + m.Config.RoomSize))
		x := (m.Config.Width / 2) + int(rand.NormFloat64()*(float64(m.Config.Width)/10.0)-float64(width)/2.0)
		if x < 0 {
			x = 0
		}
		if x+width > m.Config.Width {
			x = m.Config.Width - width
		}
		y := (m.Config.Height / 2) + int(rand.NormFloat64()*(float64(m.Config.Height)/10.0)-float64(height)/2.0)
		if y < 0 {
			y = 0
		}
		if y+height > m.Config.Height {
			y = m.Config.Height - height
		}
		rooms[i] = Room{x, y, width, height, nil, 0}
		log.Printf("Creating room: (%v, %v, %v, %v)", x, y, width, height)
	}
	forces := make([][2]float64, m.Config.NumRooms)
	velocities := make([][2]float64, m.Config.NumRooms)
	masses := make([]float64, m.Config.NumRooms)

	for i, r := range rooms {
		masses[i] = float64(r.width * r.height)
	}
	log.Print("Masses: ", masses)
	tries := 0
	overlapping := true
	for overlapping {
		// m.DebugRooms(fmt.Sprintf("map-%v.png", tries), 10, rooms)
		tries++
		overlapping = false
		overlappingRooms := make([]bool, len(rooms))
		for i := range rooms {
			forces[i][0] = 0.0
			forces[i][1] = 0.0
		}
		for i, a := range rooms {
			for j := i + 1; j < len(rooms); j++ {
				b := rooms[j]
				if Overlapping(a, b) {
					overlapping = true
					overlappingRooms[i] = true
					overlappingRooms[j] = true
					a_centre := [2]int{
						(a.x + a.width/2),
						(a.y + a.height/2),
					}
					b_centre := [2]int{
						(b.x + b.width/2),
						(b.y + b.height/2),
					}
					diff := [2]int{
						a_centre[0] - b_centre[0],
						a_centre[1] - b_centre[1],
					}
					dist := math.Sqrt(float64(diff[0]*diff[0] + diff[1]*diff[1]))
					forces[i][0] += float64(diff[0]) * m.Config.Repulsion / (dist * dist)
					forces[i][1] += float64(diff[1]) * m.Config.Repulsion / (dist * dist)

					forces[j][0] -= float64(diff[0]) * m.Config.Repulsion / (dist * dist)
					forces[j][1] -= float64(diff[1]) * m.Config.Repulsion / (dist * dist)
				}
			}
		}
		for i := range rooms {
			if !overlappingRooms[i] {
				velocities[i][0] = 0.0
				velocities[i][1] = 0.0
				continue
			}
			velocities[i][0] += forces[i][0]
			velocities[i][1] += forces[i][1]

			velocities[i][0] *= (1.0 - m.Config.Damping)
			velocities[i][1] *= (1.0 - m.Config.Damping)

			log.Printf("Velocity for room[%v] -> (%v, %v)", i, velocities[i][0], velocities[i][1])
			log.Printf("Moving room[%v] from (%v, %v)", i, rooms[i].x, rooms[i].y)
			rooms[i].x += int(velocities[i][0])
			rooms[i].y += int(velocities[i][1])

			if rooms[i].x < 0 {
				rooms[i].x = 0
			}
			if rooms[i].x+rooms[i].width > m.Config.Width {
				rooms[i].x = m.Config.Width - rooms[i].width
			}
			if rooms[i].y < 0 {
				rooms[i].y = 0
			}
			if rooms[i].y+rooms[i].height > m.Config.Height {
				rooms[i].y = m.Config.Height - rooms[i].height
			}
			log.Printf("Moving room[%v] to (%v, %v)", i, rooms[i].x, rooms[i].y)
		}
	}

	validRooms := make([]Room, 0, m.Config.NumRooms)
	for _, r := range rooms {
		if r.x >= 0 && r.x+r.width < m.Config.Width && r.y >= 0 && r.y+r.height < m.Config.Height {
			if r.width > m.Config.MinRoomSize && r.height > m.Config.MinRoomSize {
				validRooms = append(validRooms, r)
			}
		}
	}
	rooms = validRooms
	for _, r := range rooms {
		for x := 0; x < r.width; x++ {
			for y := 0; y < r.height; y++ {
				var t MapTile
				if x == 0 || x == r.width-1 || y == 0 || y == r.height-1 {
					t.Material = MatWall
				} else {
					t.Material = MatRoom
				}
				m.tiles[x+r.x][y+r.y] = t
			}
		}
	}
	// generate random pairs of rooms and join them.
	for i := 0; i < len(rooms)-1; i++ {
		from := rooms[i]
		to := rooms[i+1]
		m.JoinRooms(&from, &to)
		for n := 0; n < rand.Intn(m.Config.Connections); n++ {
			j := rand.Intn(len(rooms))
			if i == j {
				continue
			}
			to = rooms[j]
			m.JoinRooms(&from, &to)
		}
	}
}

func DoorRoute(m *Map, from, to [4]int) *MapPosition {
	return &MapPosition{
		m:      m,
		x:      from[2],
		y:      from[3],
		dest_x: to[2],
		dest_y: to[3],
	}
}
func (m *Map) JoinRooms(from, to *Room) {
	// pick a door x, y and a door x,y on room[i+1]
	fromDoor := DoorPosition(from)
	m.tiles[fromDoor[0]][fromDoor[1]].Material = MatDoor
	toDoor := DoorPosition(to)
	m.tiles[toDoor[0]][toDoor[1]].Material = MatDoor
	log.Printf("Tunelling from: (%v, %v) to (%v, %v)", fromDoor[0], fromDoor[1], toDoor[0], toDoor[1])
	count, err := astar.FindRoute(DoorRoute(m, fromDoor, toDoor))
	if err != nil {
		log.Printf("Error finding route: %v", err)
	}
	log.Printf("Dug a tunnel %v long", count)
}

// returns a pair: the door in question, and the space just outside of it.
func DoorPosition(r *Room) [4]int {
	if r.numDoors == 0 {
		// maxDoors := int(float64(r.width*r.height)/100.0) + 1
		maxDoors := 1
		log.Printf("Adding %v doors to room", maxDoors)
		r.numDoors = rand.Intn(maxDoors) + 1
	}
	var door [4]int
	if len(r.doors) < r.numDoors {
		log.Printf("New Door")
		side := rand.Intn(4)
		rx := r.x + rand.Intn(r.width-2) + 1
		ry := r.y + rand.Intn(r.height-2) + 1
		switch side {
		case 0: // top
			door = [4]int{rx, r.y, rx, r.y - 1}
		case 1: // bottom
			door = [4]int{rx, r.y + r.height - 1, rx, r.y + r.height}
		case 2: // left
			door = [4]int{r.x, ry, r.x - 1, ry}
		case 3: // right
			door = [4]int{r.x + r.width - 1, ry, r.x + r.width, ry}
		}
		r.doors = append(r.doors, door)
	} else {
		log.Printf("Existing door")
		door = r.doors[rand.Intn(r.numDoors)]
	}
	return door
}

func (m *Map) DebugRooms(filename string, scale int, rooms []Room) {
	out, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	i := image.NewRGBA(image.Rect(0, 0, m.Config.Width*scale, m.Config.Height*scale))
	for y := 0; y < m.Config.Height; y++ {
		for x := 0; x < m.Config.Width; x++ {
			c := color.White
			for x_i := 0; x_i < scale; x_i++ {
				for y_i := 0; y_i < scale; y_i++ {
					i.Set(x*scale+x_i, y*scale+y_i, c)
				}
			}
		}
	}
	c := color.Black
	for _, r := range rooms {
		top := r.y * scale
		bottom := (r.y + r.height) * scale
		left := r.x * scale
		right := (r.x + r.width) * scale
		for x := left; x < right; x++ {
			i.Set(x, top, c)
			i.Set(x, bottom, c)
		}
		for y := top; y < bottom; y++ {
			i.Set(left, y, c)
			i.Set(right, y, c)
		}
	}
	png.Encode(out, i)
}

func (m *Map) WritePng(filename string, scale int) {
	out, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}
	i := image.NewRGBA(image.Rect(0, 0, m.Config.Width*scale, m.Config.Height*scale))
	for y := 0; y < m.Config.Height; y++ {
		for x := 0; x < m.Config.Width; x++ {
			tile := m.tiles[x][y]
			c := color.Black
			switch tile.Material {
			case MatRoom:
				{
					c = color.White
				}
			case MatRock:
				{
					c = color.Gray16{0xaaff}
				}
			case MatWall:
				{
					c = color.Black
				}
			case MatDoor:
				{
					c = color.Gray16{0xa00f}
				}
			case MatTunnel:
				{
					c = color.Gray16{0xf0ff}
				}
			}
			for x_i := 0; x_i < scale; x_i++ {
				for y_i := 0; y_i < scale; y_i++ {
					i.Set(x*scale+x_i, y*scale+y_i, c)
				}
			}
		}
	}
	png.Encode(out, i)
}

func main() {
	c := MapConfig{
		Width:          200,
		Height:         200,
		NumRooms:       100,
		RoomSize:       10,
		RoomSizeStdDev: 10,
		Damping:        0.1,
		Repulsion:      5.0,
		MaxMoves:       100,
		MinRoomSize:    5,
		Connections:    5,
		DoorRatio:      0.2,
	}
	m := NewMap(c)
	m.BuildRooms()
	m.WritePng("map.png", 10)
}
