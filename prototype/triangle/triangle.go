package main

import (
	"bytes"
	"flag"
	"fmt"
	"image/color"
	"log"
	"math"
	"math/rand"
	"path"
	"runtime"
	"sort"

	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg"
)

type Vertex struct {
	X, Y float64
	E    *HalfEdge // an edge leading outwards from this point
}

func (a *Vertex) Equals(b *Vertex) bool {
	return a.X == b.X && a.Y == b.Y
}

type AsPoints []Vertex

func (a AsPoints) Len() int {
	return len(a)
}

func (a AsPoints) Label(i int) string {
	return fmt.Sprint(i)
}

func (a AsPoints) XYZ(i int) (float64, float64, float64) {
	return a[i].X, a[i].Y, 0.0
}

func (a AsPoints) XY(i int) (float64, float64) {
	return a[i].X, a[i].Y
}

type Graph struct {
	P            []Vertex
	F            []*Face
	debugCounter int // used to increment filenames every time a debugging image is saved

	advancingFront []*HalfEdge
	convexHull     []*HalfEdge
}

func (g *Graph) Pair(a, b *HalfEdge) {
	if a == nil || b == nil {
		debugLog("Attempting to pair non-existant edge: %v and %v", a, b)
		return
	}
	debugLog("Pairing edges %v and %v", a, b)
	a.Pair = b
	b.Pair = a
	// g.InvalidFlip(a)
}

func (g *Graph) InvalidFlip(e *HalfEdge) {
	debugLog("Testing validity: %v", e)
	if !e.Valid() {
		debugLog("%v - invalid", e)
		a := e.Flip()
		debugLog("New edge: %v", a)
		g.DebugSave("invalidflip")
		adjacent := []*HalfEdge{
			a.Next, a.Next.Next,
			a.Pair.Next, a.Pair.Next.Next,
		}
		for _, a := range adjacent {
			g.InvalidFlip(a)
		}
	}
}

func (g *Graph) Tidy() {
	invalid := true
	tries := 0
	for invalid {
		tries++
		debugLog("Tidying for validity, attempt: %v", tries)
		invalid = false
		for _, f := range g.F {
			e := f.E
			for i := 0; i < 3; i++ {
				for v := range g.P {
					if !e.Valid2(v) {
						invalid = true
						debugLog("FALTAL: Face %v Edge %v is invalid for vertex: %v", f, e, v)
						// e.Flip()
						// g.DebugSave(fmt.Sprintf("tidy_%v", tries))
					}
				}
				e = e.Next
			}
		}
	}
}

// Triangulate - take a graph containing points and no faces, and turn it into a convex hull mesh
func (g *Graph) Triangulate() {
	if len(g.P) < 3 {
		log.Fatal("graph has < 3 nodes")
	}
	g.SortByY()

	var leftMost, rightMost int // the indices of the extreme vertices

	p := g.P
	v := 0 // v is the new vertex we are adding
	l := 1 // l is the left of several vertices
	r := 2 // r is the right of several vertices

	if p[r].X < p[l].X {
		l, r = r, l
	}

	pv := p[v]
	pl := p[l]
	pr := p[r]

	switch {
	case pl.X < pv.X && pv.X < pr.X:
		{
			// new triangle 0 - r - l, with r-l on the advancing front, l-v, v-r on the convex hull
			f := g.NewFace(v, r, l) // f.E is v-r
			g.advancingFront = append(g.advancingFront, f.E.Next)
			g.convexHull = append(g.convexHull, f.E.Next.Next, f.E)
			leftMost = l
			rightMost = r
		}
	case pr.X < pv.X:
		{
			// both to the left
			leftMost = l
			rightMost = v
			a := g.topAngle(l, r, v)
			switch {
			case a < math.Pi:
				{
					// l-v is on the advancing Front
					// l-r, r-v is on the convex hull
					f := g.NewFace(v, l, r) // f.E is v-l
					g.advancingFront = append(g.advancingFront, f.E)
					g.convexHull = append(g.convexHull, f.E.Next, f.E.Next.Next)

				}
			case a > math.Pi:
				{
					// l-r, r-v is on the advancing front
					// l-v is on the convex hull
					f := g.NewFace(v, r, l) // f.E is v-r
					g.advancingFront = append(g.advancingFront, f.E.Next, f.E)
					g.convexHull = append(g.convexHull, f.E.Next.Next)
				}
			default:
				{
					log.Fatal("0, 1, 2 are colinear: %v, %v, %v", p[0], p[1], p[2])
				}
			}
		}
	case pl.X > pv.X:
		{
			// both to the right
			leftMost = v
			rightMost = r
			a := g.topAngle(v, l, r)
			switch {
			case a < math.Pi:
				{
					// v-r is on the advancing Front
					// v-l, l-r is on the convex hull
					f := g.NewFace(v, l, r) // f.E is v-l
					g.advancingFront = append(g.advancingFront, f.E.Next.Next)
					g.convexHull = append(g.convexHull, f.E, f.E.Next)
				}
			case a > math.Pi:
				{
					// l-r, r-v is on the advancing front
					// l-v is on the convex hull
					f := g.NewFace(v, r, l) // f.E is v-r
					g.advancingFront = append(g.advancingFront, f.E.Next.Next, f.E.Next)
					g.convexHull = append(g.convexHull, f.E)
				}
			default:
				{
					log.Fatal("0, 1, 2 are colinear: %v, %v, %v", p[0], p[1], p[2])
				}
			}
		}
	default:
		{
			log.Fatal("invalid configuration: %v, %v, %v", pv, pl, pr)
		}
	}
	debugLog("leftMost: %v, rightMost: %v", leftMost, rightMost)
	debugLog("Advancing Front: %v", g.advancingFront)
	debugLog("Convex Hull: %v", g.convexHull)
	g.DebugSave("initialized")
	for v := 3; v < len(g.P); v++ {
		debugLog("Vertex: %v", v)
		pv = p[v]
		switch {
		case pv.X < p[leftMost].X:
			{
				debugLog("Vertex %v left of leftMost: %v", v, leftMost)
				leftMost = v
				e := g.FillConcaveTopLeft(v, 0)
				ePair := g.FillConcaveBottomLeft(v, 0)
				// 3 cases:
				// e is defined and not ePair, so the e.F makes up the new convex hull
				// ePair is defined and not e, so ePair.F makes up the advancing front
				// e and ePair are defined, so the advancing front and convex hull have already been created
				switch {
				case e != nil && ePair != nil:
					{
						debugLog("Expanded both convex hull and advancing front")
						debugLog("convex hull: %v", g.convexHull)
						debugLog("advancing front: %v", g.advancingFront)
						g.Pair(ePair, e)
						g.InvalidFlip(ePair)

					}
				case e != nil && ePair == nil:
					{
						debugLog("Lower left convex hull expanding: %v (%v)", e, g.convexHull)
						g.convexHull = append([]*HalfEdge{e}, g.convexHull...)
						debugLog("Convex hull: %v", g.convexHull)
					}
				case ePair != nil && e == nil:
					{
						debugLog("left advancing front expanding: %v (%v)", e, g.advancingFront)
						g.advancingFront = append([]*HalfEdge{ePair}, g.advancingFront...)
					}
				default:
					{
						debugLog("Error: leftmost vertex resulted in no new upper or lower vertices")
					}
				}
				debugLog("new top concave %v", e)
				debugLog("new bottom concave %v", ePair)
			}
		case pv.X > p[rightMost].X:
			{
				debugLog("Vertex %v right of rightMost: %v", v, rightMost)
				rightMost = v
				e := g.FillConcaveTopRight(v, len(g.advancingFront)-1)
				ePair := g.FillConcaveBottomRight(v, len(g.convexHull)-1)
				debugLog("new top concave %v", e)
				debugLog("new bottom concave %v", ePair)
				switch {
				case e != nil && ePair != nil:
					{
						debugLog("Expanded both convex hull and advancing front")
						debugLog("convex hull: %v", g.convexHull)
						debugLog("advancing front: %v", g.advancingFront)
						g.Pair(ePair, e)
						g.InvalidFlip(ePair)

					}
				case e != nil && ePair == nil:
					{
						debugLog("Lower right convex hull expanding: %v (%v)", e, g.convexHull)
						g.convexHull = append(g.convexHull, e)
						debugLog("Convex hull: %v", g.convexHull)
					}
				case ePair != nil && e == nil:
					{
						debugLog("Lower right advancing front expanding: %v (%v)", ePair, g.advancingFront)
						g.advancingFront = append(g.advancingFront, ePair)
						debugLog("advancing front: %v", g.advancingFront)
					}
				default:
					{
						debugLog("Error: rightmost vertex resulted in no new upper or lower vertices")
					}
				}
			}
		case pv.X >= p[leftMost].X && pv.X <= p[rightMost].X:
			{
				i := g.findEdge(&pv)
				e := g.advancingFront[i]
				debugLog("Vertex %v[%v] found on edge %v", pv, v, e)
				l := g.advancingFront[i].To
				r := g.advancingFront[i].Next.Next.To
				f := g.NewFace(l, r, v)

				// stitch new face to existing triangle
				g.Pair(f.E, g.advancingFront[i])
				// flip deferred to end of the function

				// remove old edge from advancing front
				newFront := make([]*HalfEdge, len(g.advancingFront)+1)
				// leftFront := g.advancingFront[:i]
				copy(newFront, g.advancingFront[:i])
				debugLog("newFront a: %v", newFront)
				newFront[i] = f.E.Next.Next
				debugLog("newFront b: %v", newFront)
				newFront[i+1] = f.E.Next
				debugLog("newFront c: %v", newFront)
				copy(newFront[i+2:], g.advancingFront[i+1:])
				debugLog("newFront d: %v", newFront)

				// rightFront := g.advancingFront[i+1:]
				// debugLog("right Front: %v", rightFront)
				// debugLog("Left new face: %v", f.E.Next.Next)
				// g.advancingFront = append(leftFront, f.E.Next.Next)
				// g.advancingFront = append(g.advancingFront, f.E.Next)
				// debugLog("Right new face: %v", f.E.Next)
				// debugLog("right Front again: %v", rightFront)
				g.advancingFront = newFront // append(g.advancingFront, rightFront...)

				debugLog("New vertex %v %v on edge %v", v, pv, i)
				g.DebugSave("mid-face")
				eLeft := g.FillConcaveRightwards(v, i+1)
				if eLeft != nil {
					// debugLog("joining concave rightwards: %v", eLeft)
					// g.Pair(f.E.Next, eLeft)
					g.InvalidFlip(eLeft)
				} else {
					debugLog("filling concave rightwards did not give an edge to join")
				}
				eRight := g.FillConcaveLeftwards(v, i)
				if eRight != nil {
					// debugLog("joining concave leftwards: %v", eRight)
					// g.Pair(f.E.Next.Next, eRight)
					g.InvalidFlip(eRight)
				} else {
					debugLog("filling concave leftwards did not give an edge to join")
				}
				g.InvalidFlip(e)
			}
		default:
			{
				debugLog("No idea where %v is", v)
				debugLog("%v(%v)", v, pv)
				debugLog("leftMost: %v %v", leftMost, g.P[leftMost])
				debugLog("rightMost: %v %v", rightMost, g.P[rightMost])
			}

		}
		g.DebugSave("iterating")
		debugLog("leftMost: %v, rightMost: %v", leftMost, rightMost)
	}

}

// Fill the convex hull from the left
func (g *Graph) FillConcaveBottomLeft(v, i int) *HalfEdge {
	e := g.convexHull
	debugLog("Filling convex hull from the bottom left, with vertex %v joining to edge %v(%v)", v, i, e[i])
	a := v
	b := e[i].Next.Next.To
	c := e[i].To
	angle := g.bottomAngle(a, b, c)
	debugLog("Bottom angle: (%v - %v - %v) %v", a, b, c, angle)
	if angle < math.Pi {
		f := g.NewFace(a, c, b)
		g.Pair(e[i], f.E.Next) // c-b
		g.DebugSave("fill-concave-bottom-left")
		g.convexHull = append(g.convexHull[:i], append([]*HalfEdge{f.E}, g.convexHull[i+1:]...)...)
		if i < len(g.convexHull)-1 {
			eNext := g.FillConcaveBottomLeft(v, i+1)
			if eNext != nil {
				g.Pair(f.E, eNext)
			}
		}
		ret := f.E.Next.Next
		g.InvalidFlip(e[i])
		return ret
	}
	return nil
}

// Fill the convex hull from the right
func (g *Graph) FillConcaveBottomRight(v, i int) *HalfEdge {
	debugLog("Filling concave bottom right: %v %v", v, i)
	debugLog("convex hull: %v", g.convexHull)

	e := g.convexHull
	a := e[i].Next.Next.To
	b := e[i].To
	c := v // e[i].Next.Next.To
	angle := g.bottomAngle(a, b, c)
	debugLog("Bottom angle: %v %v %v - %v", a, b, c, angle)
	if angle < math.Pi {
		f := g.NewFace(a, c, b)
		debugLog("New face: %v", f)
		g.Pair(e[i], f.E.Next.Next) //b-a
		g.DebugSave("fill-concave-bottom-right")
		g.convexHull = append(g.convexHull[:i], f.E)
		if i > 0 {
			eNext := g.FillConcaveBottomRight(v, i-1)
			if eNext != nil {
				g.Pair(f.E, eNext)
			}
		}
		ret := f.E.Next
		g.InvalidFlip(e[i].Next.Next)
		return ret
	}
	return nil
}

// validate that the vertex v is valid by testing against edge i
// if the angle a,b,c is concave then create a new face. Recursively test that
// new face for concavity.
// if a new face is created, return the edge on the left, otherwise return nil.
func (g *Graph) FillConcaveTopLeft(v, i int) *HalfEdge {

	e := g.advancingFront
	a := v
	b := e[i].To
	c := e[i].Next.Next.To
	angle := g.topAngle(a, b, c)
	debugLog("Filling concave from the top left %v - %v - %v (%v)", a, b, c, angle)
	if angle < math.Pi {
		f := g.NewFace(b, c, a)
		debugLog("new face: %v", f)
		g.Pair(e[i], f.E)
		g.DebugSave("fill-concave-topleft")
		g.advancingFront = append([]*HalfEdge{f.E.Next}, g.advancingFront[i+1:]...)
		if i < len(g.advancingFront)-1 {
			eNext := g.FillConcaveTopLeft(v, i+1)
			if eNext != nil {
				g.Pair(f.E.Next, eNext)
			}
		}
		ret := f.E.Next.Next
		g.InvalidFlip(e[i])
		return ret
	}
	return nil
}

// FillConcaveTopRight - close up concave edges from right to left
func (g *Graph) FillConcaveTopRight(v, i int) *HalfEdge {

	e := g.advancingFront
	a := e[i].To
	b := e[i].Next.Next.To
	c := v
	debugLog("Filling concave top right %v %v %v", a, b, c)
	debugLog("At edge position %v (%v)", i, e)
	angle := g.topAngle(a, b, c)
	debugLog("Angle: %v", angle)
	if angle < math.Pi {
		f := g.NewFace(a, b, c)
		debugLog("New Face: %v", f)
		g.Pair(e[i], f.E)
		g.DebugSave("fill-concave-topright")
		debugLog("original advancing front: %v", g.advancingFront)
		debugLog("i: %v", i)
		debugLog("Modifying advancing front")
		debugLog("left: %v", g.advancingFront[:i])
		debugLog("new: %v", f.E.Next.Next)
		debugLog("right: %v", g.advancingFront[i+1:])
		// g.advancingFront = append(g.advancingFront[:i], append([]*HalfEdge{f.E.Next.Next}, g.advancingFront[i+1:]...)...)
		g.advancingFront = append(g.advancingFront[:i], f.E.Next.Next)
		g.DebugSave("fill-concave-topright-advancingfront")
		if i > 0 {

			eNext := g.FillConcaveTopRight(v, i-1)
			if eNext != nil {
				g.Pair(f.E.Next.Next, eNext)
			}
		}
		ret := f.E.Next
		g.InvalidFlip(e[i])
		return ret
	}
	return nil
}

// FillConcaveLeftwards - after creating a new face on the advancing front, fill concave gaps to the left
// i is the left edge of the new face
// v is the new point of the face
// returns the leftmost edge of a new face, if there is one
func (g *Graph) FillConcaveLeftwards(v, i int) *HalfEdge {
	if i == 0 {
		// no concave gaps to the left
		return nil
	}

	e := g.advancingFront
	a := e[i-1].To
	b := e[i].To
	c := v
	debugLog("Filling concave leftwards %v %v %v", a, b, c)
	debugLog("At edge position %v (%v)", i, e)
	angle := g.topAngle(a, b, c)
	debugLog("Angle: %v", angle)
	if angle < math.Pi {
		f := g.NewFace(a, b, c)
		debugLog("New Face: %v", f)
		g.Pair(e[i], f.E.Next)
		g.Pair(e[i-1], f.E)
		g.DebugSave("fill-concave-leftwards")

		g.advancingFront = append(g.advancingFront[:i-1], append([]*HalfEdge{f.E.Next.Next}, g.advancingFront[i+1:]...)...)
		if i > 0 {

			eNext := g.FillConcaveLeftwards(v, i-1)
			if eNext != nil {
				g.Pair(f.E.Next.Next, eNext)
			}
		}
		ret := f.E.Next
		g.InvalidFlip(e[i])
		return ret
	}
	return nil
}

// FillConcaveRightwards - after creating a new face on the advancing front, fill concave gaps to the right
// i is the right edge of the new face
// v is the new point of the face
func (g *Graph) FillConcaveRightwards(v, i int) *HalfEdge {
	debugLog("filling concave rightwards: (%v) edge: %v (%v)", v, i, g.advancingFront[i])
	if i == len(g.advancingFront)-1 {
		// no concave gaps to the right
		return nil
	}

	e := g.advancingFront
	a := v
	b := e[i+1].To
	c := e[i+1].Next.Next.To
	debugLog("Filling concave rightwards %v %v %v", a, b, c)
	debugLog("At edge position %v (%v)", i, e)
	angle := g.topAngle(a, b, c)
	debugLog("Angle: %v", angle)
	if angle < math.Pi {
		f := g.NewFace(a, b, c)
		debugLog("New Face: %v", f)
		g.Pair(e[i], f.E)
		g.Pair(e[i+1], f.E.Next)
		g.DebugSave("fill-concave-rightwards")

		g.advancingFront = append(g.advancingFront[:i], append([]*HalfEdge{f.E.Next.Next}, g.advancingFront[i+2:]...)...)
		if i < len(g.advancingFront)-1 {

			eNext := g.FillConcaveRightwards(v, i)
			if eNext != nil {
				g.Pair(f.E.Next.Next, eNext)
			}
		}
		ret := f.E
		g.InvalidFlip(e[i])
		return ret
	}
	return nil
}

// findEdge -find the edge in e which cover's v's .X
// on the advancing front, counterclockwise edges go right to left,
// so edge.To is on the left-hand side.
// findEdge assumes that v.X is contained within the advancing front.
func (g *Graph) findEdge(v *Vertex) int {
	e := g.advancingFront
	for i := 0; i < len(e); i++ {
		l := e[i].To
		r := e[i].Next.Next.To
		debugLog("looking for %v on edge %v(%v[%v], %v[%v])", v.X, i, l, g.P[l].X, r, g.P[r].X)
		debugLog("Edge face: %v", e[i].F)
		if g.P[l].X < v.X && v.X <= g.P[r].X {
			return i
		}
	}
	log.Fatalf("Error finding vertex %v on advancing front:\n%v", v, g.advancingFront)

	return -1
}

// topAngle gives the angle between vertices a, b, c which are defined in l-r order along the top of the hull
func (g *Graph) topAngle(a, b, c int) float64 {
	debugLog("angle: %v -> %v -> %v", a, b, c)
	ba := Vertex{
		X: g.P[a].X - g.P[b].X,
		Y: g.P[a].Y - g.P[b].Y,
	}
	bc := Vertex{
		X: g.P[c].X - g.P[b].X,
		Y: g.P[c].Y - g.P[b].Y,
	}
	debugLog("angle: %v . %v", ba, bc)

	baRad := math.Atan2(ba.Y, ba.X)
	// baRad is in the left half of the quadrant, hence if negative, add 2*math.Pi
	if baRad < 0 {
		baRad += 2 * math.Pi
	}
	bcRad := math.Atan2(bc.Y, bc.X)
	debugLog("ba angle: %v", baRad*180.0/math.Pi)
	debugLog("bc angle: %v", bcRad*180.0/math.Pi)
	diff := (baRad - bcRad)
	return diff
}

// bottomAngle gives the angle between vertices a, b, c which are defined in l-r order along the bottom of the hull
func (g *Graph) bottomAngle(a, b, c int) float64 {
	debugLog("angle: %v -> %v -> %v", a, b, c)
	ba := Vertex{
		X: g.P[a].X - g.P[b].X,
		Y: g.P[a].Y - g.P[b].Y,
	}
	bc := Vertex{
		X: g.P[c].X - g.P[b].X,
		Y: g.P[c].Y - g.P[b].Y,
	}
	debugLog("angle: %v . %v", ba, bc)

	baRad := math.Atan2(ba.Y, ba.X)
	// baRad is in the left half of the quadrant, hence if positive, subtract from 2*pi
	baRad = -baRad
	if baRad < 0 {
		baRad += 2 * math.Pi
	}
	bcRad := math.Atan2(bc.Y, bc.X)
	bcRad = -bcRad
	debugLog("ba angle: %v", baRad*180.0/math.Pi)
	debugLog("bc angle: %v", bcRad*180.0/math.Pi)
	diff := (baRad - bcRad)
	return diff
}

func (g *Graph) String() string {
	var b bytes.Buffer
	for i, v := range g.P {
		fmt.Fprintf(&b, "%v: (%v, %v)\n", i, v.X, v.Y)
	}
	fmt.Fprint(&b, "\n")
	for _, f := range g.F {
		fmt.Fprintf(&b, "Face: ")
		e := f.E
		for i := 0; i < 3; i++ {
			fmt.Fprintf(&b, "-> (%v, %v)[%v]", g.P[e.To].X, g.P[e.To].Y, e.To)
			if i < 2 {
				fmt.Fprint(&b, ", ")
			}
			e = e.Next
		}
		fmt.Fprint(&b, "\n")
	}
	return b.String()
}

// Create a new face with vertices a, b, c counterclockwise
// Return the new face, whose edge is ab
func (g *Graph) NewFace(a, b, c int) *Face {
	ab := &HalfEdge{
		To: b,
		g:  g,
	}
	g.P[a].E = ab
	bc := &HalfEdge{
		To: c,
		g:  g,
	}
	g.P[b].E = bc
	ca := &HalfEdge{
		To: a,
		g:  g,
	}
	g.P[c].E = ca

	f := &Face{
		E: ab,
		g: g,
	}
	ab.F = f
	bc.F = f
	ca.F = f
	ab.Next = bc
	bc.Next = ca
	ca.Next = ab
	g.F = append(g.F, f)
	return f
}

func (g *Graph) DebugSave(key string) {
	debugLog("Saving: %v %v", g.debugCounter, key)
	debugLog("Advancing Front: %v", g.advancingFront)
	debugLog("Convex Hull: %v", g.convexHull)
	filename := fmt.Sprintf("debug-%v-%v.svg", g.debugCounter, key)
	g.debugCounter++
	g.Save(filename)
}

type TopEdgeLine []*HalfEdge
type BottomEdgeLine []*HalfEdge

func (e TopEdgeLine) Len() int {
	return len(e) + 1
}

func (e BottomEdgeLine) Len() int {
	return len(e) + 1
}

func (e TopEdgeLine) XY(i int) (float64, float64) {
	var v int
	if i == len(e) {
		v = e[len(e)-1].Next.Next.To
	} else {
		v = e[i].To
	}
	return e[0].g.P[v].X, e[0].g.P[v].Y
}

func (e BottomEdgeLine) XY(i int) (float64, float64) {
	var v int
	if i == 0 {
		v = e[i].Next.Next.To
	} else {
		v = e[i-1].To
	}
	return e[0].g.P[v].X, e[0].g.P[v].Y
}

func (g *Graph) Save(filename string) {

	p, err := plot.New()
	if err != nil {
		log.Fatal(err)
	}
	dots, err := plotter.NewBubbles(AsPoints(g.P), vg.Length(0.1)*vg.Centimeter, vg.Length(0.1)*vg.Centimeter)
	p.Add(dots)

	labels, err := plotter.NewLabels(AsPoints(g.P))
	if err != nil {
		log.Fatal(err)
	}
	p.Add(labels)

	afLine, err := plotter.NewLine(TopEdgeLine(g.advancingFront))
	if err != nil {
		log.Fatal(err)
	}
	afLine.Color = color.RGBA{255, 128, 128, 255}
	afLine.Width = vg.Length(0.1) * vg.Centimeter

	p.Add(afLine)
	chLine, err := plotter.NewLine(BottomEdgeLine(g.convexHull))
	debugLog("Convex Hull Line: %v", chLine)
	if err != nil {
		log.Fatal(err)
	}
	chLine.Color = color.RGBA{128, 255, 128, 255}
	chLine.Width = vg.Length(0.1) * vg.Centimeter
	p.Add(chLine)

	for _, f := range g.F {
		line, err := plotter.NewLine(f)
		if err != nil {
			log.Fatal(err)
		}
		p.Add(line)
	}
	p.Save(vg.Length(10)*vg.Centimeter, vg.Length(10)*vg.Centimeter, filename)

}

func (a *Graph) Equals(b *Graph) bool {
	if len(a.P) != len(b.P) {
		// debugLog("A has a different number of points than B: %v(%v) %v(%v)", len(a.P), a.P, len(b.P), b.P)
		return false
	}
	for i := range a.P {
		if !a.P[i].Equals(&b.P[i]) {
			// fmt.Printf("Points are different: (%v, %v)[%v]", a.P[i], b.P[i], i)
			return false
		}
	}
	if len(a.F) != len(b.F) {
		// fmt.Printf("A has a different number of faces to B: %v %v", len(a.F), len(b.F))
		return false
	}
	edges := make(map[[2]int]bool)
	for i := range a.F {
		e := a.F[i].E
		pointa := e.To
		pointb := e.To
		for j := 0; j < 4; j++ {
			// fmt.Printf("Face %v, Edge %v\n", i, j)
			// fmt.Printf("Point: %v[%v]\n", a.P[pointa], pointa)
			e = e.Next
			pointb = e.To
			edges[[2]int{pointa, pointb}] = true
			pointa = pointb
		}
	}
	for i := range b.F {
		e := b.F[i].E
		pointa := e.To
		pointb := e.To
		for j := 0; j < 4; j++ {
			e = e.Next
			pointb = e.To
			if !edges[[2]int{pointa, pointb}] {
				// fmt.Printf("b does not contain (%v - %v)\na: %q\nb: %q", pointa, pointb, a, b)
				return false
			}
			pointa = pointb
		}
	}

	return true
}

func NewRandomGraph(size int, r *rand.Rand) *Graph {
	g := Graph{
		P: make([]Vertex, size),
	}
	for i := 0; i < size; i++ {
		g.P[i].X = r.NormFloat64()
		g.P[i].Y = r.NormFloat64()
		// g.P[i].X = 100 * r.Float64()
		// g.P[i].Y = 100 * r.Float64()
	}
	return &g
}

type ByY []Vertex

func (v ByY) Len() int {
	return len(v)
}

func (v ByY) Less(a, b int) bool {
	return v[a].Y < v[b].Y
}

func (v ByY) Swap(a, b int) {
	v[a], v[b] = v[b], v[a]
}

func (g *Graph) SortByY() {
	sort.Sort(ByY(g.P))
}

type HalfEdge struct {
	To   int       // the vertex at the end of this half-edge
	Pair *HalfEdge // the opposite half-edge
	Next *HalfEdge // the next half-edge around the triangle
	F    *Face
	g    *Graph
}

func (e *HalfEdge) String() string {
	var b bytes.Buffer
	fmt.Fprint(&b, "(")
	if e.Next != nil && e.Next.Next != nil {
		fmt.Fprintf(&b, "From: %v, ", e.Next.Next.To)
	}
	fmt.Fprintf(&b, "To: %v", e.To)
	fmt.Fprint(&b, ")")
	return b.String()
}

// calculate's the determinant of a 3x3 matrix
func determinant(m [][]float64) float64 {
	a := m[0][0]
	b := m[0][1]
	c := m[0][2]

	d := m[1][0]
	e := m[1][1]
	f := m[1][2]

	g := m[2][0]
	h := m[2][1]
	i := m[2][2]

	// fmt.Printf("%v*(%v*%v-%v*%v) - %v*(%v*%v-%v*%v) + %v*(%v*%v-%v*%v)", a, e, i, f, h, b, d, i, f, g, c, d, h, e, g)
	det := a*(e*i-f*h) - b*(d*i-f*g) + c*(d*h-e*g)
	return det
}

// Valid2 - tests if the face defined by an edge is valid according to some other vertex v
func (e *HalfEdge) Valid2(v int) bool {
	if v == e.To || v == e.Next.To || v == e.Next.Next.To {
		return true
	}
	a := e.g.P[e.To]
	b := e.g.P[e.Next.To]
	c := e.g.P[e.Next.Next.To]

	d := e.g.P[v]
	debugLog("Testing validity: (%v %v %v) - %v", a, b, c, d)

	m := [][]float64{
		[]float64{a.X - d.X, a.Y - d.Y, (a.X*a.X - d.X*d.X) + (a.Y*a.Y - d.Y*d.Y)},
		[]float64{b.X - d.X, b.Y - d.Y, (b.X*b.X - d.X*d.X) + (b.Y*b.Y - d.Y*d.Y)},
		[]float64{c.X - d.X, c.Y - d.Y, (c.X*c.X - d.X*d.X) + (c.Y*c.Y - d.Y*d.Y)},
	}
	return determinant(m) <= 0
}

// Valid - tests if the point opposite the edge's pair is outside of the circle formed by the three points of e's triangle
func (e *HalfEdge) Valid() bool {
	if e.Pair == nil {
		return true
	}
	a := e.g.P[e.To]
	b := e.g.P[e.Next.To]
	c := e.g.P[e.Next.Next.To]

	d := e.g.P[e.Pair.Next.To]
	debugLog("Testing validity: (%v %v %v) - %v", a, b, c, d)

	m := [][]float64{
		[]float64{a.X - d.X, a.Y - d.Y, (a.X*a.X - d.X*d.X) + (a.Y*a.Y - d.Y*d.Y)},
		[]float64{b.X - d.X, b.Y - d.Y, (b.X*b.X - d.X*d.X) + (b.Y*b.Y - d.Y*d.Y)},
		[]float64{c.X - d.X, c.Y - d.Y, (c.X*c.X - d.X*d.X) + (c.Y*c.Y - d.Y*d.Y)},
	}
	return determinant(m) <= 0
}

// Flip - flip the quadrilateral with e
// return one of the newly created edges
func (e *HalfEdge) Flip() *HalfEdge {
	//     l
	// 2-------- 3
	// |\        |
	// |  \  e'  |
	//i|  e \    |k
	// |      \  |
	// 0 ------- 1
	//      j
	//
	//     l
	// 2-------- 3
	// |       / |
	// |   b /   |
	//i|   / a   |k
	// | /       |
	// 0 ------- 1
	//      j
	//
	// triangle A = 0 - 1 - 2
	// e = 1-2
	// triangle B = 1 - 3 - 2
	// ePair = 2-1
	// i = 2-0
	// j = 0-1
	// k = 1-3
	// l = 3-2
	// fmt.Printf("e: %+v\n", e)
	ePair := e.Pair
	if ePair == nil {
		debugLog("Attempting to flip edge with no pair: %v", e)
		return nil
	}
	// fmt.Printf("ePair: %+v\n", ePair)
	i := e.Next
	// fmt.Printf("i: %+v\n", i)
	j := e.Next.Next
	// fmt.Printf("j: %+v\n", j)
	k := ePair.Next
	// fmt.Printf("k: %+v\n", k)
	l := ePair.Next.Next
	// fmt.Printf("l: %+v\n", l)
	debugLog("Flipping (%v %v %v)(%v %v %v)", e, i, j, ePair, k, l)
	debugLog("original: %v, %v", e.F, ePair.F)

	// create a new HalfEdge A from: e.Next.To -> e.Pair.Next.To

	a := &HalfEdge{
		To: i.To,
		F:  j.F,
		g:  e.g,
	}
	e.To = i.To
	e.F = j.F
	k.F = j.F

	b := &HalfEdge{
		To:   k.To,
		F:    l.F,
		Pair: a,
		g:    e.g,
	}
	i.F = l.F
	ePair.To = k.To
	ePair.F = l.F

	// a.Pair = b
	// b.Pair = a

	j.F.E = j
	l.F.E = l
	// fmt.Printf("a: %+v", a)
	// fmt.Printf("b: %+v", b)

	// a.Next = j
	e.Next = j
	j.Next = k
	// k.Next = a
	k.Next = e

	// b.Next = l
	ePair.Next = l
	l.Next = i
	// i.Next = b
	i.Next = ePair

	// e.g.P[i.To].E = b
	// e.g.P[k.To].E = a
	e.g.P[i.To].E = ePair
	e.g.P[k.To].E = e

	debugLog("Flipping (%v %v %v)(%v %v %v)", a, j, k, b, l, i)
	debugLog("flipped: %v, %v", e.F, ePair.F)
	return e
}

type Face struct {
	E *HalfEdge // one of the half-edges of this face.
	g *Graph
}

func (f *Face) IsValid() (bool, error) {
	i := 0
	origin := f.E
	e := f.E
	for {
		if e.To != e.Next.Next.Next.To {
			return false, fmt.Errorf("Face %v edge %v (%v != %v)", f, i, e.To, e.Next.Next.Next.To)
		}
		i++
		e = e.Next
		if e == origin {
			break
		}
		if i > 3 {
			return false, fmt.Errorf("Face %v has too many edges: %v", f, e.Next.Next.Next.Next)
		}
	}
	return true, nil
}

func (f *Face) String() string {
	e := f.E
	var b bytes.Buffer
	fmt.Fprint(&b, "Face: ")
	for i := 0; i < 3; i++ {
		fmt.Fprint(&b, "Edge[ ")
		fmt.Fprintf(&b, "To:%v %v", e.To, f.g.P[e.To])
		fmt.Fprint(&b, "] ")
		e = e.Next
		if i < 2 {
			fmt.Fprintf(&b, " -> ")
		}
	}
	return b.String()
}

func (f *Face) Len() int {
	return 4 // because this is a closed loop: 0 -> 1 -> 2 -> 0
}

func (f *Face) XY(i int) (float64, float64) {
	e := f.E
	for j := 0; j < i; j++ {
		e = e.Next
	}

	v := e.g.P[e.To]
	return v.X, v.Y
}

var (
	size = flag.Int("size", 10, "number of points in graph")
	seed = flag.Int("seed", 1, "random seed")
	beta = flag.Float64("beta", 1.0, "beta-skeleton parameter")
)

func debugLog(f string, args ...interface{}) {
	// log.Printf(f, args...)
	_, file, line, ok := runtime.Caller(1)
	_, _, line2, ok2 := runtime.Caller(2)
	if !ok || !ok2 {
		log.Printf(fmt.Sprintf("unknown caller: %v", f), args...)
	} else {
		log.Printf(fmt.Sprintf("%v: %v . %v - %v", path.Base(file), line2, line, f), args...)
	}
}

type SkeletonEdge struct {
	P [2]int
	s *Skeleton
}

func (s SkeletonEdge) Len() int {
	return 2
}

func (s SkeletonEdge) XY(i int) (float64, float64) {
	point := s.s.P[s.P[i]]
	return point.X, point.Y
}

type Skeleton struct {
	P []Vertex
	E []SkeletonEdge
}

type ByLength []SkeletonEdge

func (e ByLength) Len() int {
	return len(e)
}

func (e ByLength) Less(a, b int) bool {
	aStartIndex := e[a].P[0]
	aEndIndex := e[a].P[1]

	aStartVertex := e[a].s.P[aStartIndex]
	aEndVertex := e[a].s.P[aEndIndex]

	aVector := Vertex{
		X: aEndVertex.X - aStartVertex.X,
		Y: aEndVertex.Y - aStartVertex.Y,
	}

	aLen := math.Sqrt(aVector.X*aVector.X + aVector.Y*aVector.Y)

	bStartIndex := e[b].P[0]
	bEndIndex := e[b].P[1]

	bStartVertex := e[b].s.P[bStartIndex]
	bEndVertex := e[b].s.P[bEndIndex]

	bVector := Vertex{
		X: bEndVertex.X - bStartVertex.X,
		Y: bEndVertex.Y - bStartVertex.Y,
	}

	bLen := math.Sqrt(bVector.X*bVector.X + bVector.Y*bVector.Y)

	return aLen < bLen
}

func (e ByLength) Swap(a, b int) {
	e[a], e[b] = e[b], e[a]
}

type vertexMap map[int]int

func (s *Skeleton) EuclidianMinimumSpanningTree() *Skeleton {
	edges := make([]SkeletonEdge, len(s.E))
	m := make(vertexMap)
	n := Skeleton{
		P: make([]Vertex, len(s.P)),
		E: make([]SkeletonEdge, 0),
	}
	copy(edges, s.E)
	copy(n.P, s.P)
	sort.Sort(ByLength(edges))
	for i := range n.P {
		m[i] = i
	}
	for i := range edges {
		p, q := edges[i].P[0], edges[i].P[1]
		pSet := m[p]
		qSet := m[q]
		if pSet != qSet {
			n.E = append(n.E, SkeletonEdge{
				P: edges[i].P,
				s: &n,
			})
			// merge pset and qset
			for vertex, set := range m {
				if set == qSet {
					m[vertex] = pSet
				}
			}
		}
	}
	return &n
}

func (s *Skeleton) Save(filename string) {

	p, err := plot.New()
	if err != nil {
		log.Fatal(err)
	}
	dots, err := plotter.NewBubbles(AsPoints(s.P), vg.Length(0.1)*vg.Centimeter, vg.Length(0.1)*vg.Centimeter)
	if err != nil {
		log.Fatal(err)
	}
	dots.Color = color.RGBA{50, 50, 50, 255}

	p.Add(dots)

	labels, err := plotter.NewLabels(AsPoints(s.P))
	if err != nil {
		log.Fatal(err)
	}
	labels.Color = color.RGBA{200, 200, 200, 255}
	p.Add(labels)

	for _, e := range s.E {
		line, err := plotter.NewLine(e)
		if err != nil {
			log.Fatal(err)
		}
		p.Add(line)
	}
	p.Save(vg.Length(10)*vg.Centimeter, vg.Length(10)*vg.Centimeter, filename)
}

func (s *Skeleton) AddEdge(a, b int) {
	switch {
	case a == b:
		{
			return
		}
	case a < b:
		{
			s.E = append(s.E, SkeletonEdge{
				s: s,
				P: [2]int{a, b},
			})
		}
	case a > b:
		{
			s.E = append(s.E, SkeletonEdge{
				s: s,
				P: [2]int{b, a},
			})
		}
	}
}

func (s *Skeleton) Angle(a, b, c int) float64 {
	debugLog("Skeleton angle: %v -> %v -> %v", a, b, c)
	ba := Vertex{
		X: s.P[a].X - s.P[b].X,
		Y: s.P[a].Y - s.P[b].Y,
	}
	baMag := math.Sqrt(ba.X*ba.X + ba.Y*ba.Y)
	ba.X /= baMag
	ba.Y /= baMag

	bc := Vertex{
		X: s.P[c].X - s.P[b].X,
		Y: s.P[c].Y - s.P[b].Y,
	}
	bcMag := math.Sqrt(bc.X*bc.X + bc.Y*bc.Y)
	bc.X /= bcMag
	bc.Y /= bcMag

	debugLog("angle: %v . %v", ba, bc)

	dotProduct := ba.X*bc.X + ba.Y*bc.Y
	debugLog("dot product: %v", dotProduct)
	angle := math.Acos(dotProduct)

	debugLog("Skeleton angle: %v -> %v -> %v = %v", a, b, c, angle)
	return angle
}

func (g *Graph) BetaSkeleton(beta float64) *Skeleton {
	if beta < 1 {
		log.Fatal("only works for beta >= 1")
	}
	threshold := math.Asin(1 / beta)
	debugLog("threshold angle: %v", threshold)
	s := Skeleton{
		P: make([]Vertex, len(g.P)),
	}
	copy(s.P, g.P)
	debugLog("Skeleton: %v", s)
	for i, f := range g.F {
		e := f.E
		debugLog("Face: %v, starting edge: %v", i, e)
		for j := 0; j < 3; j++ {
			a := e.To
			c := e.Next.Next.To
			debugLog("testing edge: %v-%v", a, c)
			bs := []int{e.Next.To}
			if e.Pair != nil {
				bs = append(bs, e.Pair.Next.To)
			}
			valid := true
			for _, b := range bs {
				angle := s.Angle(a, b, c)
				if angle > threshold {
					valid = false
					break
				}
			}
			if valid {
				s.AddEdge(a, c)
			}
			e = e.Next
		}
	}
	return &s
}

func main() {
	flag.Parse()
	debugLog("Seed: %v", *seed)
	r := rand.New(rand.NewSource(int64(*seed)))
	g := NewRandomGraph(*size, r)
	g.Triangulate()
	// g.Tidy()
	g.DebugSave("main")
	fmt.Printf("%v", g.String())
	for _, f := range g.F {
		if ok, err := f.IsValid(); !ok {
			fmt.Println(err)
		}
	}
	s := g.BetaSkeleton(*beta)
	fmt.Println("skeleton:")
	fmt.Println(s)
	s.Save("skeleton.svg")
	m := s.EuclidianMinimumSpanningTree()
	m.Save("spanningtree.svg")

	debugLog("Seed: %v", *seed)
}
