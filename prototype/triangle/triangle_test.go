package main

import (
	"math"
	"testing"
)

func TestEquals(t *testing.T) {

	// b
	a := &Graph{
		P: []Vertex{
			Vertex{X: 0, Y: 0},
			Vertex{X: 1, Y: 0},
			Vertex{X: 0, Y: 1},
			Vertex{X: 1, Y: 1},
		},
	}
	aEdges := []*HalfEdge{
		&HalfEdge{ // 0
			To: 1, // 0-1
			g:  a,
		},
		&HalfEdge{ // 1
			To: 2, // 1-2
			g:  a,
		},
		&HalfEdge{ // 2
			To: 0, // 2-0
			g:  a,
		},
	}
	a.P[0].E = aEdges[0]
	a.P[1].E = aEdges[1]
	a.P[2].E = aEdges[2]

	aEdges[0].Next = aEdges[1]
	aEdges[1].Next = aEdges[2]
	aEdges[2].Next = aEdges[0]
	a.F = []*Face{&Face{
		E: aEdges[0],
		g: a,
	}}

	// b
	b := &Graph{
		P: []Vertex{
			Vertex{X: 0, Y: 0},
			Vertex{X: 1, Y: 0},
			Vertex{X: 0, Y: 1},
			Vertex{X: 1, Y: 1},
		},
	}
	bEdges := []*HalfEdge{
		&HalfEdge{ // 0
			To: 1, // 0-1
			g:  b,
		},
		&HalfEdge{ // 1
			To: 3, // 1-3
			g:  b,
		},
		&HalfEdge{ // 2
			To: 0, // 3-0
			g:  b,
		},
	}
	b.P[0].E = bEdges[1]
	b.P[1].E = bEdges[1]
	b.P[3].E = bEdges[1]
	bEdges[0].Next = bEdges[1]
	bEdges[1].Next = bEdges[2]
	bEdges[2].Next = bEdges[0]
	b.F = []*Face{&Face{
		E: bEdges[0],
		g: b,
	}}
	t.Logf("a:\n%v", a)
	t.Logf("b:\n%v", b)
	if testing.Verbose() {
		a.Save("test-equals-a.png")
		b.Save("test-equals-b.png")
	}
	if a.Equals(b) {
		t.Errorf("a != b\na:\n%v\nb:\n%v", a, b)
	}
}

func TestSortByY(t *testing.T) {
	got := Graph{
		P: []Vertex{
			Vertex{X: 3, Y: 2},
			Vertex{X: 2, Y: 3},
			Vertex{X: 1, Y: 1},
		},
	}
	want := Graph{
		P: []Vertex{
			Vertex{X: 1, Y: 1},
			Vertex{X: 3, Y: 2},
			Vertex{X: 2, Y: 3},
		},
	}
	got.SortByY()
	if !got.Equals(&want) {
		t.Errorf("g.TestSortByY:\n%q\nWant:\n%q", got, want)
	}
}

func TestNewFace(t *testing.T) {
	tests := make([]*Graph, 3)

	for i := 0; i < 3; i++ {
		tests[i] = &Graph{
			P: []Vertex{
				Vertex{X: 0.0, Y: 0.0},
				Vertex{X: 1.0, Y: 0.0},
				Vertex{X: 0.0, Y: 0.0},
			},
		}
		a := i
		b := (i + 1) % 3
		c := (i + 2) % 3
		tests[i].NewFace(a, b, c)
	}

	want := &Graph{
		P: []Vertex{
			Vertex{X: 0.0, Y: 0.0},
			Vertex{X: 1.0, Y: 0.0},
			Vertex{X: 0.0, Y: 0.0},
		},
	}

	edges := []*HalfEdge{
		&HalfEdge{
			To: 1,
			g:  want,
		},
		&HalfEdge{
			To: 2,
			g:  want,
		},
		&HalfEdge{
			To: 0,
			g:  want,
		},
	}
	face := &Face{
		E: edges[0],
		g: want,
	}
	edges[0].Next = edges[1]
	edges[0].F = face
	edges[1].Next = edges[2]
	edges[1].F = face
	edges[2].Next = edges[0]
	edges[2].F = face

	want.F = []*Face{
		face,
	}

	for _, got := range tests {
		if !got.Equals(want) {
			t.Errorf("NewFace(0, 1, 2) ->\n%v\nwant:\n%v", *got, *want)
		}
	}
}

func getReferenceGraphB() *Graph {
	//     l
	// 2-------- 3
	// |       / |
	// |   b /   |
	//i|   / a   |k
	// | /       |
	// 0 ------- 1
	//      j
	//
	referenceGraph := &Graph{
		P: []Vertex{
			Vertex{X: 0.0, Y: 0.0},
			Vertex{X: 1.0, Y: 0.0},
			Vertex{X: 0.0, Y: 1.0},
			Vertex{X: 1.0, Y: 1.0},
		},
	}
	referenceGraphEdges := []*HalfEdge{
		&HalfEdge{
			To: 1, // 0-1
			g:  referenceGraph,
		},
		&HalfEdge{
			To: 3, // 1-3
			g:  referenceGraph,
		},
		&HalfEdge{
			To: 0, // 3-0
			g:  referenceGraph,
		},
		&HalfEdge{
			To: 3, // 0-3
			g:  referenceGraph,
		},
		&HalfEdge{
			To: 2, // 3-2
			g:  referenceGraph,
		},
		&HalfEdge{
			To: 0, // 2-0
			g:  referenceGraph,
		},
	}
	referenceGraphEdges[0].Next = referenceGraphEdges[1]
	referenceGraphEdges[1].Next = referenceGraphEdges[2]
	referenceGraphEdges[2].Next = referenceGraphEdges[0]
	referenceGraphEdges[3].Next = referenceGraphEdges[4]
	referenceGraphEdges[4].Next = referenceGraphEdges[5]
	referenceGraphEdges[5].Next = referenceGraphEdges[0]

	referenceGraph.P[0].E = referenceGraphEdges[0] // 0-1
	referenceGraph.P[1].E = referenceGraphEdges[1] // 1-3
	referenceGraph.P[2].E = referenceGraphEdges[5] // 2-0
	referenceGraph.P[3].E = referenceGraphEdges[3] // 3-0

	referenceGraphFaces := []*Face{
		&Face{
			E: referenceGraphEdges[0],
			g: referenceGraph,
		},
		&Face{
			E: referenceGraphEdges[3],
			g: referenceGraph,
		},
	}
	referenceGraphEdges[0].F = referenceGraphFaces[0]
	referenceGraphEdges[1].F = referenceGraphFaces[0]
	referenceGraphEdges[2].F = referenceGraphFaces[0]
	referenceGraphEdges[3].F = referenceGraphFaces[1]
	referenceGraphEdges[4].F = referenceGraphFaces[1]
	referenceGraphEdges[5].F = referenceGraphFaces[1]

	referenceGraph.F = referenceGraphFaces
	return referenceGraph
}

func getReferenceGraphA() *Graph {
	//     l
	// 2-------- 3
	// |\        |
	// |  \  e'  |
	//i|  e \    |k
	// |      \  |
	// 0 ------- 1
	//      j
	//
	referenceGraph := &Graph{
		P: []Vertex{
			Vertex{X: 0.0, Y: 0.0},
			Vertex{X: 1.0, Y: 0.0},
			Vertex{X: 0.0, Y: 1.0},
			Vertex{X: 1.0, Y: 1.0},
		},
	}

	referenceGraphEdges := []*HalfEdge{
		&HalfEdge{ // 0
			To: 1, // 0-1
			g:  referenceGraph,
		},
		&HalfEdge{ // 1
			To: 2, // 1-2
			g:  referenceGraph,
		},
		&HalfEdge{ // 2
			To: 0, // 2-0
			g:  referenceGraph,
		},
		&HalfEdge{ // 3
			To: 3, // 1-3
			g:  referenceGraph,
		},
		&HalfEdge{ // 4
			To: 2, // 3-2
			g:  referenceGraph,
		},
		&HalfEdge{ // 5
			To: 1, // 2-1
			g:  referenceGraph,
		},
	}
	referenceGraphEdges[0].Next = referenceGraphEdges[1]
	referenceGraphEdges[1].Next = referenceGraphEdges[2]
	referenceGraphEdges[2].Next = referenceGraphEdges[0]

	referenceGraphEdges[3].Next = referenceGraphEdges[4]
	referenceGraphEdges[4].Next = referenceGraphEdges[5]
	referenceGraphEdges[5].Next = referenceGraphEdges[3]

	referenceGraphEdges[1].Pair = referenceGraphEdges[5]
	referenceGraphEdges[5].Pair = referenceGraphEdges[1]

	referenceGraph.P[0].E = referenceGraphEdges[0] // 0-1
	referenceGraph.P[1].E = referenceGraphEdges[1] // 1-2
	referenceGraph.P[2].E = referenceGraphEdges[2] // 2-0
	referenceGraph.P[3].E = referenceGraphEdges[3] // 1-3

	referenceGraphFaces := []*Face{
		&Face{
			E: referenceGraphEdges[0],
			g: referenceGraph,
		},
		&Face{
			E: referenceGraphEdges[3],
			g: referenceGraph,
		},
	}

	referenceGraphEdges[0].F = referenceGraphFaces[0]
	referenceGraphEdges[1].F = referenceGraphFaces[0]
	referenceGraphEdges[2].F = referenceGraphFaces[0]
	referenceGraphEdges[3].F = referenceGraphFaces[1]
	referenceGraphEdges[4].F = referenceGraphFaces[1]
	referenceGraphEdges[5].F = referenceGraphFaces[1]

	referenceGraph.F = referenceGraphFaces
	return referenceGraph
}

func getReferenceTriangle() *Graph {
	t := &Graph{
		P: []Vertex{
			Vertex{X: 0.0, Y: 0.0},
			Vertex{X: 1.0, Y: 0.0},
			Vertex{X: 0.0, Y: 1.0},
		},
	}

	edges := []*HalfEdge{
		&HalfEdge{
			To: 1,
			g:  t,
		},
		&HalfEdge{
			To: 2,
			g:  t,
		},
		&HalfEdge{
			To: 0,
			g:  t,
		},
	}
	face := &Face{
		E: edges[0],
		g: t,
	}
	edges[0].Next = edges[1]
	edges[0].F = face
	edges[1].Next = edges[2]
	edges[1].F = face
	edges[2].Next = edges[0]
	edges[2].F = face

	t.F = []*Face{
		face,
	}
	return t
}

func TestEdgeNewFace(t *testing.T) {
	got := getReferenceTriangle()
	want := getReferenceTriangle()
	want.P = append(want.P, Vertex{X: 1, Y: 1})
	got.P = append(got.P, Vertex{X: 1, Y: 1})

	pairEdge := got.F[0].E.Next

	edges := []*HalfEdge{
		&HalfEdge{
			To: 1,
			g:  want,
		},
		&HalfEdge{
			To: 3,
			g:  want,
		},
		&HalfEdge{
			To: 2,
			g:  want,
		},
	}
	face := &Face{
		E: edges[0],
		g: want,
	}
	edges[0].Next = edges[1]
	edges[0].F = face
	edges[1].Next = edges[2]
	edges[1].F = face
	edges[2].Next = edges[0]
	edges[2].F = face

	want.F = append(want.F, face)
	pairEdge.NewFace(3)
	if !got.Equals(want) {
		t.Errorf("(Edge).NewFace() ->\n%v\nwant:\n%v", got, want)
	}
	t.Logf("got:\n%v\nwant:\n%v", got, want)
}

func TestFlip(t *testing.T) {
	want := &Graph{
		P: []Vertex{
			Vertex{X: 0.0, Y: 0.0},
			Vertex{X: 1.0, Y: 0.0},
			Vertex{X: 0.0, Y: 1.0},
			Vertex{X: 1.0, Y: 1.0},
		},
	}
	wantEdges := []*HalfEdge{
		&HalfEdge{
			To: 1, // 0-1
			g:  want,
		},
		&HalfEdge{
			To: 3, // 1-3
			g:  want,
		},
		&HalfEdge{
			To: 0, // 3-0
			g:  want,
		},
		&HalfEdge{
			To: 3, // 0-3
			g:  want,
		},
		&HalfEdge{
			To: 2, // 3-2
			g:  want,
		},
		&HalfEdge{
			To: 0, // 2-0
			g:  want,
		},
	}
	wantEdges[0].Next = wantEdges[1]
	wantEdges[1].Next = wantEdges[2]
	wantEdges[2].Next = wantEdges[0]
	wantEdges[3].Next = wantEdges[4]
	wantEdges[4].Next = wantEdges[5]
	wantEdges[5].Next = wantEdges[0]

	want.P[0].E = wantEdges[0] // 0-1
	want.P[1].E = wantEdges[1] // 1-3
	want.P[2].E = wantEdges[5] // 2-0
	want.P[3].E = wantEdges[3] // 3-0

	wantFaces := []*Face{
		&Face{
			E: wantEdges[0],
			g: want,
		},
		&Face{
			E: wantEdges[3],
			g: want,
		},
	}
	wantEdges[0].F = wantFaces[0]
	wantEdges[1].F = wantFaces[0]
	wantEdges[2].F = wantFaces[0]
	wantEdges[3].F = wantFaces[1]
	wantEdges[4].F = wantFaces[1]
	wantEdges[5].F = wantFaces[1]

	want.F = wantFaces

	got := &Graph{
		P: []Vertex{
			Vertex{X: 0.0, Y: 0.0},
			Vertex{X: 1.0, Y: 0.0},
			Vertex{X: 0.0, Y: 1.0},
			Vertex{X: 1.0, Y: 1.0},
		},
	}

	gotEdges := []*HalfEdge{
		&HalfEdge{ // 0
			To: 1, // 0-1
			g:  got,
		},
		&HalfEdge{ // 1
			To: 2, // 1-2
			g:  got,
		},
		&HalfEdge{ // 2
			To: 0, // 2-0
			g:  got,
		},
		&HalfEdge{ // 3
			To: 3, // 1-3
			g:  got,
		},
		&HalfEdge{ // 4
			To: 2, // 3-2
			g:  got,
		},
		&HalfEdge{ // 5
			To: 1, // 2-1
			g:  got,
		},
	}
	gotEdges[0].Next = gotEdges[1]
	gotEdges[1].Next = gotEdges[2]
	gotEdges[2].Next = gotEdges[0]

	gotEdges[3].Next = gotEdges[4]
	gotEdges[4].Next = gotEdges[5]
	gotEdges[5].Next = gotEdges[3]

	gotEdges[1].Pair = gotEdges[5]
	gotEdges[5].Pair = gotEdges[1]

	got.P[0].E = gotEdges[0] // 0-1
	got.P[1].E = gotEdges[1] // 1-2
	got.P[2].E = gotEdges[2] // 2-0
	got.P[3].E = gotEdges[3] // 1-3

	gotFaces := []*Face{
		&Face{
			E: gotEdges[0],
			g: got,
		},
		&Face{
			E: gotEdges[3],
			g: got,
		},
	}

	gotEdges[0].F = gotFaces[0]
	gotEdges[1].F = gotFaces[0]
	gotEdges[2].F = gotFaces[0]
	gotEdges[3].F = gotFaces[1]
	gotEdges[4].F = gotFaces[1]
	gotEdges[5].F = gotFaces[1]

	got.F = gotFaces

	pairEdge := got.F[0].E.Next
	t.Log(pairEdge)
	t.Log(pairEdge.Pair)
	if testing.Verbose() {
		got.Save("test-flipedge-original.png")
	}
	pairEdge.Flip()

	t.Logf("got:\n%v", got)
	t.Logf("want:\n%v", want)
	if !got.Equals(want) {
		t.Errorf("(Edge).Flip() ->\n%v\nwant:\n%v", got, want)
	}
	if testing.Verbose() {
		got.Save("test-flipedge-got.png")
		want.Save("test-flipedge-want.png")
	}

}

func TestGetAllEdges(t *testing.T) {
	// g := Graph{}
	// e := g.AllEdgesFrom(0)
}

func TestGetEdge(t *testing.T) {
	// g := Graph{}
	// e := g.GetEdge(0, 1)
}

func TestValidate(t *testing.T) {
	// reference triangle A is a right angled triangle:
	// (0,0) - (0, 1) - (1, 0)
	// circumcircle is centered at the center of the hypoteneuse: (0.5, 0.5), with r= sqrt(0.5)
	valid := getReferenceGraphA()
	valid.P[3].X = 2.0
	valid.P[3].Y = 2.0

	validPairEdge := valid.P[0].E.Next

	invalid := getReferenceGraphA()
	invalid.P[3].X = 0.6
	invalid.P[3].Y = 0.6
	invalidPairEdge := invalid.P[0].E.Next

	if !validPairEdge.Valid() {
		t.Errorf("(%v).Valid -> invalid\n%v", validPairEdge, valid)
	}
	if invalidPairEdge.Valid() {
		t.Errorf("(%v).Valid -> valid\n%v", invalidPairEdge, invalid)
	}

}

func TestDeterminant(t *testing.T) {
	test := [][]float64{
		[]float64{1, 5, 3},
		[]float64{2, 4, 7},
		[]float64{4, 6, 2},
	}

	want := 74.0
	got := determinant(test)

	if got != want {
		t.Errorf("determinant(%v) -> %v. Want: %v", test, got, want)
	}
}

func TestTopAngle(t *testing.T) {
	tests := []struct {
		g    *Graph
		want float64
	}{

		{g: &Graph{
			P: []Vertex{
				Vertex{X: 0, Y: 0},
				Vertex{X: 1, Y: 1},
				Vertex{X: 2, Y: 0},
			},
		},
			want: 0.75 * 2 * math.Pi,
		},
		{g: &Graph{
			P: []Vertex{
				Vertex{X: 0, Y: 1},
				Vertex{X: 1, Y: 0},
				Vertex{X: 2, Y: 1},
			},
		},
			want: 0.5 * math.Pi,
		},
	}

	for _, test := range tests {
		got := test.g.topAngle(0, 1, 2)
		if got != test.want {
			t.Errorf("topAngle() -> %v. Want: %v\n%v", got, test.want, test.g)
		}

	}
}

func TestBottomAngle(t *testing.T) {
	tests := []struct {
		g    *Graph
		want float64
	}{

		{g: &Graph{
			P: []Vertex{
				Vertex{X: 0, Y: 0},
				Vertex{X: 1, Y: 1},

				Vertex{X: 2, Y: 0},
			},
		},
			want: 0.5 * math.Pi,
		},
		{g: &Graph{
			P: []Vertex{
				Vertex{X: 0, Y: 1},
				Vertex{X: 1, Y: 0},
				Vertex{X: 2, Y: 1},
			},
		},
			want: 0.75 * 2 * math.Pi,
		},
	}

	for _, test := range tests {
		got := test.g.bottomAngle(0, 1, 2)
		if got != test.want {
			t.Errorf("bottomAngle() -> %v. Want: %v\n%v", got, test.want, test.g)
		}

	}
}
