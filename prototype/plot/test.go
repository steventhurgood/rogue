package main

import (
	"log"

	"github.com/gonum/plot"
	"github.com/gonum/plot/plotter"
	"github.com/gonum/plot/vg"
)

type Points [][2]float64

func (p Points) Len() int {
	return len(p)
}

func (p Points) XYZ(i int) (float64, float64, float64) {
	return p[i][0], p[i][1], 0.0
}

func (p Points) XY(i int) (float64, float64) {
	return p[i][0], p[i][1]
}

var (
	data Points = [][2]float64{
		[2]float64{1.0, 3.7},
		[2]float64{1.0, 4.0},
		[2]float64{3.0, 1.0},
	}
)

func main() {
	p, err := plot.New()
	if err != nil {
		log.Fatal(err)
	}
	bubbles, err := plotter.NewBubbles(data, vg.Length(0.05)*vg.Inch, vg.Length(0.05)*vg.Inch)
	if err != nil {
		log.Fatal(err)
	}
	line, err := plotter.NewLine(data)
	if err != nil {
		log.Fatal(err)
	}
	p.Add(bubbles)
	p.Add(line)
	err = p.Save(vg.Length(10.0)*vg.Centimeter, vg.Length(10.0)*vg.Centimeter, "test.png")
	if err != nil {
		log.Fatal(err)
	}
}
