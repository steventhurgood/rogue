package rogue

import (
	"math"
	"math/rand"
)

type DistParams struct {
	Mean, Sd float64
}

func RandomLogNormal(p *DistParams, r *rand.Rand) int {
	rNorm := r.NormFloat64()

	scale := math.Log(1 + (p.Sd*p.Sd)/(p.Mean*p.Mean))
	location := math.Log(p.Mean) - 0.5*scale

	return int(math.Exp(location + scale*rNorm))
}
