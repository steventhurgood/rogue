package rogue

import (
	"fmt"
	"math"
)

// box handles the attributes and methods for arranging box-like shapes

type BoxLike interface {
	X() int
	SetX(int)
	Y() int
	SetY(int)

	Width() int
	SetWidth(int)
	Height() int
	SetHeight(int)
}

type Box struct {
	x, y          int // the left and top edges
	width, height int
}

func BoxToBoxLike(b []*Box) []BoxLike {
	r := make([]BoxLike, len(b))
	for i := range b {
		r[i] = b[i]
	}
	return r
}

func BoxCentre(b BoxLike) [2]int {
	return [2]int{
		b.X() + b.Width()/2,
		b.Y() + b.Height()/2,
	}
}

func (b *Box) X() int {
	return b.x
}

func (b *Box) SetX(x int) {
	b.x = x
}

func (b *Box) Y() int {
	return b.y
}

func (b *Box) SetY(y int) {
	b.y = y
}

func (b *Box) Width() int {
	return b.width
}

func (b *Box) SetWidth(w int) {
	b.width = w
}

func (b *Box) Height() int {
	return b.height
}

func (b *Box) SetHeight(h int) {
	b.height = h
}

// NormalizeBox moves all boxes so that they start from origin 0, 0, and returns a box with x,y coordinates to reset them to their original positions.
// That is, a list containing a single box from (-1, -1) - (1, 1) would have that box moved to (0,0) - (2,2), and the returned box would have x,y be (-1, -1)
// and width/height = 2, 2
func NormalizeBox(boxes []BoxLike) (*Box, error) {
	extents, err := GetExtents(boxes)
	if err != nil {
		return nil, err
	}
	for _, b := range boxes {
		b.SetX(b.X() - extents.x)
		b.SetY(b.Y() - extents.y)
	}
	return extents, nil
}

func BoxDistance(a, b BoxLike) float64 {
	aMid := [2]float64{
		float64(a.X()) + 0.5*float64(a.Width()),
		float64(a.Y()) + 0.5*float64(a.Height()),
	}
	bMid := [2]float64{
		float64(b.X()) + 0.5*float64(b.Width()),
		float64(b.Y()) + 0.5*float64(b.Height()),
	}
	dist := math.Sqrt((aMid[0]-bMid[0])*(aMid[0]-bMid[0]) + (aMid[1]-bMid[1])*(aMid[1]-bMid[1]))
	return dist
}

// GetExtents returns a box that contains all of a slice of BoxLike objects.
func GetExtents(boxes []BoxLike) (*Box, error) {
	if len(boxes) == 0 {
		return nil, fmt.Errorf("Extents of 0 boxes")
	}
	n := &Box{
		x:      boxes[0].X(),
		y:      boxes[0].Y(),
		width:  boxes[0].X() + boxes[0].Width(),  // temporarily use width as max(x)
		height: boxes[0].Y() + boxes[0].Height(), // temporarily use height as max(y)
	}
	for _, b := range boxes {
		if b.X() < n.x {
			n.x = b.X()
		}
		if b.Y() < n.y {
			n.y = b.Y()
		}
		if b.X()+b.Width() > n.width {
			n.width = b.X() + b.Width()
		}
		if b.Y()+b.Height() > n.height {
			n.height = b.Y() + b.Height()
		}
	}
	n.width = n.width - n.x
	n.height = n.height - n.y
	return n, nil
}
